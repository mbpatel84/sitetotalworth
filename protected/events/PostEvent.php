<?php
class PostEvent {
	public static function sendEmailOnMessageSent($event) {
		unset($_GET['owner']);
		$sender=$event->sender;
		$params=$event->params;
		$mailer = Yii::app()->mailer;
		$mailer->setFrom(Yii::app()->params["notificationEmail"], Yii::app()->params["notificationName"]);
		$mailer->Subject = Yii::t("mail", "You got new message!", array(), null, $params['receiver']->lang_id);
		$mailer->AddAddress($params['receiver']->email);
		$mailer->setPathViews("application.views.mail.post");
		$mailer->setPathLayouts("application.views.mail.layouts");
		$mailer->getView("new_message", array(
			"name"=>Helper::mb_ucfirst($params['receiver']->username),
			"fromName"=>Helper::mb_ucfirst($sender->owner->username),
			"user"=>$params['receiver'],
			"messageLink"=>Yii::app()->controller->createAbsoluteUrl("post/chain", array("id" => $params['receiver_header_id'])),
		), "main");
        $mailer->AltBody=Yii::t('notification', 'Please, use mail client which support HTML markup');
		$mailer->Send();
	}

	public static function bindWebsite($event) {
		$chain_id=$event->params['chain_id'];
		$bind=new BindWebsite;
		$bind->chain_id=$chain_id;
		$bind->website_id=Yii::app()->controller->website_id;
		$bind->save();
	}

}