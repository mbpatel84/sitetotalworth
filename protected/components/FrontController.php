<?php
class FrontController extends BaseController {
	protected $_end = 'front';

	public $newMessages=0;
	public $upcomingHeaders=array();
	public $senders;

	public function init() {
		parent::init();
		if(!Yii::app()->user->isGuest AND !Yii::app()->request->isPostRequest) {
			$this->setUpcomingData();
		}
	}

	protected function setUpcomingData() {
		$box=Yii::app()->innerMail->box(Yii::app()->user->loadModel());
		$this->newMessages=$box->getNewMessageCount();
		if($this->newMessages) {
			$cnt=Yii::app()->params['upcomingHeaders'];
			$this->upcomingHeaders=$box->getUpcomingHeaders($cnt, $senders);
			$criteria=new CDbCriteria;
			$criteria->index='id';
			$criteria->addInCondition('id', $senders);
			$this->senders=User::model()->findAll($criteria);
		}
	}

	protected function setUserLoginUrl() {
		Yii::app()->user->loginUrl = $this->createUrl('user/sign-in');
	}

	protected function setErrorHandlerAction() {
		Yii::app()->errorHandler->errorAction='site/error';
	}
}