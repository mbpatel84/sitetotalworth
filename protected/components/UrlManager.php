<?php
class UrlManager extends CUrlManager {
	public function createUrl($route, $params=array(), $ampersand='&') {
		if(!isset($params['language'])) {
			$params['language'] = Yii::app() -> language;
		}
		if(isset($_GET['owner'])) {
			$params['owner']=$_GET['owner'];
		}
		return parent::createUrl($route, $params, $ampersand);
	}
}