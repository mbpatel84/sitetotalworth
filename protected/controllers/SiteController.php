<?php
class SiteController extends FrontController {
	public function actionIndex() {
		$cs=Yii::app()->clientScript;
		$this -> title = Yii::t("site", "Index page title");
		$params=array(
			"{Portal}"=>Helper::getInstalledUrl(),
		);
		$cs -> registerMetaTag(Yii::t("site", "Index page keywords", $params), 'keywords');
		$cs -> registerMetaTag(Yii::t("site", "Index page description", $params), 'description');

		$widget = $this->widget('application.widgets.WebsiteList', array(
			"config"=>array(
                "totalItemCount"=>Yii::app()->params['websitesOnIndexPage'],
                "pagination"=>array(
                    "pageSize"=>Yii::app()->params['websitesOnIndexPage']
                )
            ),
		), true);

		$requestForm = $this->widget('application.widgets.RequestFormWidget', array(
		), true);

		$this->render('index', array(
			"widget"=>$widget,
			"requestForm"=>$requestForm,
		));
	}

	public function actionContact() {
		$form = new ContactForm;
		$this -> title = Yii::t("contact", "Contact us");
		$params=array(
			"{Portal}"=>Helper::getInstalledUrl(),
		);
		$cs=Yii::app()->clientScript;
		$cs->registerMetaTag(Yii::t("contact", "Contact page keywords", $params), 'keywords');
		$cs->registerMetaTag(Yii::t("contact", "Contact page description", $params), 'description');

		if(Yii::app() -> request -> isPostRequest && !empty($_POST['ContactForm'])) {
			$form->attributes=$_POST['ContactForm'];
			if($form->validate()) {
				try {
					$mail = Yii::app() -> mailer;
					$mail -> SetFrom($form -> email, $form -> name);
					$mail -> Subject = $form -> subject;
					$mail -> AddAddress(Yii::app() -> params["adminEmail"]);
					$mail -> MsgHTML($form -> body);
					$mail -> AltBody = $form -> body;
					$mail -> Send();
					Yii::app() -> user -> setFlash('success', Yii::t("notification", "Email has been sent"));
				} catch (Exception $e) {
					Yii::log($e->getMessage(), 'error', 'application.site.contact');
					Yii::app() -> user -> setFlash('danger', Yii::t("notification", "An error occurred while sending email"));
				}
				$this -> refresh();
			}
		}

		$this -> render("contact", array(
			'form' => $form,
		));
	}
}