<?php
class CronController extends FrontController {
	public function init() {
		parent::init();
		$app_key = Yii::app() -> params['cronKey'];
		$get_key = Yii::app() -> request -> getQuery('key');
		if($app_key===false) {
			throw new CHttpException(400, Yii::t("error", "Bad request"));
		}
		if($app_key != $get_key) {
			throw new CHttpException(400, Yii::t("error", "Bad request"));
		}
	}

	public function actionSitemap() {
		$this->runCommand(array(
			'yiic', 'sitemap', 'index'
		));
	}

	public function actionGarbageCollector() {
		$this->runCommand(array(
			'yiic', 'garbagecollector', 'index'
		));
	}

	public function actionOnSaleChecker() {
		$this->runCommand(array(
			'yiic', 'checkonsale', 'index'
		));
	}

	protected function runCommand($args) {
		// Get command path
		$commandPath = Yii::app() -> getBasePath() . DIRECTORY_SEPARATOR . 'commands';

		// Create new console command runner
		$runner = new CConsoleCommandRunner();

		// Adding commands
		$runner -> addCommands($commandPath);

		// If something goes wrong return error
		$runner -> run ($args);
	}
}