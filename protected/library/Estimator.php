<?php
class Estimator {
	private $alexa = 0;
	
	const AVERAGE_DAYS_IN_MONTH = 29.53;
	const POWER = -0.732;
	const MAX_UNIQUE_VISITORS_PER_DAY = 7000000;
	const ESTIMATE_PAGEVIEWS = 4;
	const PERCENT_OF_PAGEVIEWS = 0.1; // 10 %
	const AD_UNIT_CPM = 0.03;
	const AVERAGE_DAYS_IN_YEAR = 365.2425;
	
	public function __construct($alexaRank) {
		$this->alexa = (int) $alexaRank;
	}
	
	protected function _getEstimatedUniqueDailyVisitors() {
		static $uv = null;
		if(null === $uv) {
			$uv = self::MAX_UNIQUE_VISITORS_PER_DAY * pow($this->alexa, self::POWER); 
		}
		return $uv;
	}
	
	protected function _getEstimatedUniqueMonthlyVisitors() {
		return $this->getEstimatedUniqueDailyVisitors() * self::AVERAGE_DAYS_IN_MONTH;
	}
	
	protected function _getEstimatedUniqueYearlyVisitors() {
		return $this->getEstimatedUniqueDailyVisitors() * self::AVERAGE_DAYS_IN_YEAR;
	}
	
	protected function _getEstimatedDailyAdsRevenue() {
		static $dr = null;
		if(null===$dr) {
			$dr = $this->getPercentOfPageViews() * self::AD_UNIT_CPM;
		}
		return $dr;
	}
	
	protected function _getEstimatedMonthlyAdsRevenue() {
		return $this->getEstimatedDailyAdsRevenue() * self::AVERAGE_DAYS_IN_MONTH;
	}

	protected function _getEstimatedYearlyAdsRevenue() {
		return $this->getEstimatedDailyAdsRevenue() * self::AVERAGE_DAYS_IN_YEAR;
	}

	protected function _getEstimatedDailyPageViews() {
		static $pw = null;
		if(null===$pw) {
			$pw = round($this->getEstimatedUniqueDailyVisitors() * self::ESTIMATE_PAGEVIEWS);
		}
		return $pw;
	}

	protected function _getEstimatedMonthlyPageViews() {
		return $this->getEstimatedDailyPageViews() * self::AVERAGE_DAYS_IN_MONTH;
	}
	
	protected function _getEstimatedYearlyPageViews() {
		return $this->getEstimatedDailyPageViews() * self::AVERAGE_DAYS_IN_YEAR;
	}

	private function getPercentOfPageViews() {
		return $this->getEstimatedDailyPageViews() * self::PERCENT_OF_PAGEVIEWS;
	}
	
	public function __call($method, $args) {
		$method = "_".$method;
		if(method_exists($this, $method)) {
			if($this->alexa <= 0) {
				return 0;
			}
			return call_user_func_array(array($this, $method), $args);
		}
		throw new Exception ("Unable to find {$method} in Estimator");
	}
}