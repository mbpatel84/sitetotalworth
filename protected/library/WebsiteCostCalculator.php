<?php
class WebsiteCostCalculator {
	private $pageRates = array(
		'google' => 0.15,
		'bing' => 0.10,
		'yahoo' => 0.08
	);
	private $socialRates = array(
		'google' => 0.15,
		'twitter' => 0.08,
		'facebook' => 0.12,
	);
	private $prRates = array(
		0 => 0.5,
		1 => 0.7,
		2 => 1,
		3 => 1.05,
		4 => 1.15,
		5 => 1.3,
		6 => 3.0,
		7 => 9,
		8 => 10,
		9 => 10.5,
		10 => 11,
	);

	private $bonusBasedOnAlexa = array(
		10 => array(
			'alexa'=>1.30,
			'backlinks'=>1,
		),
		50 => array(
			'alexa'=>1.26,
			'backlinks'=>0.95,
		),
		100 => array(
			'alexa' => 1.25,
			'backlinks'=>0.90,
		),
		500 => array(
			'alexa' => 1.23,
			'backlinks'=>0.85,
		),
		1000 => array(
			'alexa' => 1.21,
			'backlinks'=>0.80,
		),
		10000 => array(
			'alexa' => 1.20,
			'backlinks'=>0.75,
		),
		50000 => array(
			'alexa' => 1.19,
			'backlinks'=>0.70,
		),
		100000 => array(
			'alexa' => 1.18,
			'backlinks'=>0.65,
		),
		200000 => array(
			'alexa' => 1.17,
			'backlinks'=>0.60,
		),
		350000 => array(
			'alexa' => 1.16,
			'backlinks'=>0.55,
		),
		400000 => array(
			'alexa' => 1.15,
			'backlinks'=>0.50,
		),
		600000 => array(
			'alexa' => 1.14,
			'backlinks'=>0.45,
		),
		900000 => array(
			'alexa' => 1.13,
			'backlinks'=>0.40,
		),
		1200000 => array(
			'alexa' => 1.12,
			'backlinks'=>0.30,
		),
		3000000 => array(
			'alexa' => 1.11,
			'backlinks'=>0.25,
		),
		5000000 => array(
			'alexa' => 1.09,
			'backlinks'=>0.15,
		),
		10000000 => array(
			'alexa' => 1.07,
			'backlinks'=>0.05,
		),
		15000000 => array(
			'alexa' => 1.05,
			'backlinks'=>0.03,
		),
	);

	private $alexaTotal = 35000001;
	private $fbLike = 0, $gPlus = 0, $tweet = 0, $googlePage = 0, $bingPage = 0, $yahooPage = 0, $pageRank = 0, $alexa = 0,
	$backLinks = 0;

	public function setFbLike($fbLike) { $this->fbLike = $this->abs($fbLike); }
	public function setGPlus($gPlus) { $this->gPlus = $this->abs($gPlus); }
	public function setTweet($tweet) { $this->tweet = $this->abs($tweet); }

	public function setGooglePage($n) { $this->googlePage = $this->abs($n); }
	public function setBingPage($n) { $this->bingPage = $this->abs($n); }
	public function setYahooPage($n) { $this->yahooPage = $this->abs($n); }

	public function setPageRank($pr) { $this->pageRank = $this->abs($pr); }

	public function setBackLinks($n) { $this->backLinks = $this->abs($n); }

	public function setAlexa($n) { $this->alexa = $this->abs($n); }

	private function getPrRate() {
		return isset($this->prRates[$this->pageRank]) ? $this->prRates[$this->pageRank] : 0.1;
	}

	private function getAlexaBonus() {
		if($this->alexa <= 0) {
			return 0;
		}
		$bonus = $this->getBonusBasedOnAlexa('alexa');
		$diff = $this->alexaTotal - $this->alexa;
		if($diff < 0) {
			$diff = 0;
		}
		return (1 / $this->alexa) * pow($diff, $bonus['const']);
	}

	private function getBonusBasedOnAlexa($type) {
		if($this->alexa <= 0) {
			$last = array_slice($this->bonusBasedOnAlexa, -1, 1, true);
			foreach($last as $id=>$const) {
				return array(
					'id'=>$id, 'const'=>$const[$type]
				);
			}
		}
		foreach($this->bonusBasedOnAlexa as $id => $const) {
			if($this->alexa < $id) {
				break;
			}
		}
		return array('id'=>$id, 'const'=>$const[$type]);
	}


	private function getSearchEngineBonus() {
		$prRate = $this->getPrRate();
		$bonus = 	($this->pageRates['google'] * $prRate * $this->googlePage) +
							($this->pageRates['bing'] * $prRate * $this->bingPage) +
							($this->pageRates['yahoo'] * $prRate * $this->yahooPage);
		return $bonus;
	}

	private function getSocialBonus() {
		$bonus = 	($this->socialRates['facebook'] * $this->fbLike) +
							($this->socialRates['twitter'] * $this->tweet) +
							($this->socialRates['google'] * $this->gPlus);
		return $bonus;
	}

	private function getBackLinksBonus() {
		$bonus = $this->getBonusBasedOnAlexa('backlinks');
		return $this->backLinks * $bonus['const'];
	}

	public function getPrice() {
		$socialBonus = $this->getSocialBonus();
		$searchBonus = $this->getSearchEngineBonus();
		$alexaBonus = $this->getAlexaBonus();
		$backLinksBonus = $this->getBackLinksBonus();
		$price = $socialBonus + $searchBonus + $alexaBonus + $backLinksBonus;
		/*var_dump("Alexa bonus: ". number_format($alexaBonus, 2, ",", "."));
		var_dump("Search engine: ". number_format($searchBonus, 2, ",", "."));
		var_dump("Social activity: ". number_format($socialBonus, 2, ",", "."));
		var_dump("Backlinks: ". number_format($backLinksBonus, 2, ",", "."));
 		var_dump("Total: ". number_format($price, 2, ",", "."));*/
		return $price;
	}

	public function flush() {
		$this->fbLike=$this->gPlus=$this->tweet=$this->googlePage=
		$this->bingPage=$this->yahooPage=$this->pageRank=$this->alexa=$this->backLinks=0;
	}

	private function abs($n, $d=0) { return (int) $n < 0 ? $d : (int) $n; }
}

/*
$gIndex = 351000000;
$bIndex = 60700000;
$yIndex = 212000000;

$fbLike = 1368662;
$twLike = 7686;
$gpLike = 5202658;

$backLinks = 78600000;

$alexa = 1;
$pr = 9;
//3.030.928.321,45
*/

/*
$gIndex = 27000;
$bIndex = 0;
$yIndex = 5060;

$fbLike = 1;
$twLike = 0;
$gpLike = 0;

$backLinks = 1200;
$alexa = 80000;
$pr = 2;

$calc = new WebsiteCostCalculator;

$calc->setFbLike($fbLike);
$calc->setGPlus($gpLike);
$calc->setTweet($twLike);

$calc->setGooglePage($gIndex);
$calc->setBingPage($bIndex);
$calc->setYahooPage($yIndex);

$calc->setPageRank($pr);
$calc->setAlexa($alexa);

$calc->setBackLinks($backLinks);

$price = $calc->getPrice();
*/