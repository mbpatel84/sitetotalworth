<?php
return array(
	'ru' => 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя',
	'en' => 'abcdefghijklmnopqrstuvwxyz',
    'de' => 'aäbcdefghijklmnoöpqrsßtuüvwxyz',
);