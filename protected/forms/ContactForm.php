<?php
class ContactForm extends CFormModel {
	public $name;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;

	public function rules() {
		return array(
			array('name, email, subject, body', 'required'),
			array('email', 'email'),
			array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
		);
	}

	public function attributeLabels() {
		return array(
			'name' => Yii::t("contact", "Name"),
			'email' => Yii::t("contact", "Email"),
			'subject' => Yii::t("contact", "Subject"),
			'body' => Yii::t("contact", "Body"),
			"verifyCode"=>Yii::t("misc", "Verification code"),
		);
	}
}