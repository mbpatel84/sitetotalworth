<?php
class ForgotPasswordForm extends CFormModel {
	public $email;

	public function rules() {
		return array(
			array('email', 'required'),
			array('email', 'email'),
			array('email', 'exist', 'className' => 'User', 'attributeName' => 'email', 'message' => Yii::t("user", "We didn't find such email address"), 'criteria'=>array(
				'condition'=>'role=:role',
				'params'=>array(':role'=>User::ROLE_USER),
			)),
		);
	}
	public function attributeLabels() {
		return array(
			'email' => Yii::t("user", "Email"),
		);
	}
}