<?php
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Website Worth Calculator',
	'language' => 'en',
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.forms.*',
		'application.events.*',
	),

	'components'=>array(

		'mailer' => array(
			'class' => 'ext.mailer.EMailer',
			// Mail server protocol
			'Mailer' => 'smtp',
			'SMTPAuth' => true,
			// Mail server host
			'Host' => 'mail.example.com',
			// Mail server port
			'Port' => 25,
			// Username
			'Username' => 'no-reply@example.com',
			// Password
			'Password' => '',
			// Encoding
			'CharSet' => 'UTF-8',
		),

		'innerMail' => array(
			'class'=>'InnerMail',
			'connectionID' => 'db',
			'headerTable' => '{{post_header_%d}}',
			'messageTable' => '{{post_message_%d}}',
			'folderTable' => '{{post_folder_%d}}',
			'blockTable' => '{{block_sender}}',
			'scamTable' => '{{scam_report}}',
			'serverCount'=>3,
			'headersPageSize'=>20, // Number of headers per page
			'messagesPageSize'=>30, // Number of messages per page
			'blockedUsersPageSize'=>15, // Number of blocked users per page
		),

		'user'=>array(
			// enable cookie-based authentication
			'class' => 'application.components.WebUser',
			'allowAutoLogin'=>true,
		),

		'authManager' => array(
			'class' => 'application.components.PhpAuthManager',
			'defaultRoles' => array('guest'),
		),

		'urlManager'=>array(
			'urlFormat'=>'path',
			'class'=>'UrlManager',
            'cacheID'=>'cache',
			'showScriptName' => false,
			'rules'=>array(
				'proxy'=>'PagePeekerProxy/index',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>' => 'site/index',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/feed/<_a:(rss|atom)>.xml' => 'feed/<_a>',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/cost/<domain:[\\pL\w\d\-\.]+>' => 'website/show',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/contact' => 'site/contact',

				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/buy/<slug:[\\pL\d\-]+>/order-by/<order:(added_at|price)>/<sort:(asc|desc)>' => 'category/index',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/buy/<slug:[\\pL\d\-]+>/order-by/<order:(added_at|price)>' => 'category/index',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/buy/order-by/<order:(added_at|price)>/<sort:(asc|desc)>' => 'category/index',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/buy/order-by/<order:(added_at|price)>' => 'category/index',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/buy/<slug:[\\pL\d\-]+>' => 'category/index',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/buy' => 'category/index',

				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/<_a:(top|upcoming)>'=> 'website/<_a>list',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/top/page-rank'=> 'website/PageRanklist',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/top/page-rank/<id:[\d\w\-\_]+>'=> 'website/PageRank',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/top/<_a:(page-rank|country)>/<id:[\d\w\-\_]+>'=> 'website/<_a>',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/top/<_a:(page-rank|country)>'=> 'website/<_a>list',

				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
				'<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/<controller:[\w\-]+>' => '<controller>/index',
				'admin/<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>' => 'admin/site/index',
				'admin' => 'admin/site/index',
				'<module:\w+>/<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+|\w+>' => '<module>/<controller>/<action>',
				'<module:\w+>/<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<module>/<controller>/<action>',
				'<module:\w+>/<language:[a-z]{2}|[a-z]{2}_[a-z]{2}>/<controller:[\w\-]+>' => '<module>/<controller>/index',
			),
		),
		'messages' => array(
			'class' => 'DbMessageSource',
			'sourceMessageTable' => 'sc_trans_source_message',
			'translatedMessageTable' => 'sc_trans_message',
			'missingTranslationTable' => 'sc_trans_missing',
			'cachingDuration'=>60 * 60 * 24 * 30,
			'onMissingTranslation'=>array('DbMessageSource', 'onMissingTranslation'),
		),

		'cache' => array(
			'class' => 'CFileCache',
		),

		'db'=>array(
			// Mysql host: localhost and databse name catalog
			'connectionString' => 'mysql:host=localhost;dbname=sitetotalworth',
			// whether to turn on prepare emulation
			'emulatePrepare' => true,
			// db username
			'username' => 'root',
			// db password
			'password' => '',
			// default cahrset
			'charset' => 'utf8',
			// table prefix
			'tablePrefix' => 'sc_',
			// cache time to reduce SHOW CREATE TABLE * queries
			'schemaCachingDuration' => 60 * 60 * 24 * 30,
			'enableProfiling'=>true,
			'enableParamLogging' => true,
		),

		'securityManager' => array(
			'cryptAlgorithm'=>array('rijndael-256', '', 'ofb', ''), // More http://php.net/manual/en/function.mcrypt-module-open.php
			'encryptionKey' => 'pnkRVLZC6Oj87H2G8qmsNN',
		),

		'clientScript'=>array(
			'packages'=>array(
				'jquery'=>array(
					'baseUrl'=>'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/',
						'js'=>array('jquery.min.js'),
				),
			),
		),


		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
					'except'=>'exception.CHttpException.*',
				),
                /*array(
                    'class'=>'CWebLogRoute',
                ),
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'trace',
                    'categories'=>'system.db.*',
                    'logFile'=>'sql.log',
                ),*/
			),
		),
	),


	'params'=>array(
		'adminEmail'=>'webmaster@example.com', // This email will be receiving message sent by contact form
		'notificationEmail' => 'no-reply@example.com',  // Notifier email
		'notificationName' => 'Website Worth Calculator Team',  // Notifier name
		'webdataCache' => 60 * 60 * 24, // Cache estimate price for X seconds
		'checkForBadwords' => true, // Whether to check website for bad words before calculate estimate price
		'default_timezone'=>'Europe/Vilnius', // Application timezone http://www.php.net/manual/en/timezones.php
		'shareHTML'=>"", // HTML for share buttons
		'shareJS'=>'', // JS for share buttons
		'googleMapsAPIKey'=>'', // Google maps API
		'websitesPerPage' => 12, // Number of website in widget
		'onSaleCheckLimit' => 3, // If after 3 attempts BOT will not find widget on website which is on sale, it will be automatically removed.
		'pricePattern'=>'{currency} {price}', // $ 12.34
		'currency'=>'&#36;', // USD $ http://webdesign.about.com/od/localization/l/blhtmlcodes-cur.htm
		'dollarRate'=>1, // The dollar's ratio against the set currency. For example if you set 2.45 and website worth is 10$ then the result will be 24.50 (Currency)
		'websitesOnIndexPage' => 6,
		'adminWebsitesPerPage'=>20,
		'onSalePerPage' => 15,
		'countriesPerPage' => 30,
		'decPoint' => ',',
		'thousandsSep' => '.',
		'decimals'=>2, // number of decimal points in each price
		'useProxyImage' => true,
		'upcomingHeaders'=>3,
		'captcha'=>true, // Use or not captcha to get estimate price
		'autoCalculation'=>false, // Whether to automatically calucalte estimate price if user directly request URL /lang/cost/domain.com. Dont work if captcha set true
		'cronKey'=>false, // Secret key. Used to run cron job commands via browser
		'badLoginCount'=>3, // Captcha will appear on if user enter X times invalid login or password
		'logMissingTranslations'=>true, // Whether to log phrases which are do not has translations
		'inCategoryPerPage'=>15,
		'bannerTop'=>'', // Top banner source
		'bannerBottom'=>'', // Bottom banner source
		'thumbs'=>array( // Thumbnail providers
				0 => array(
						'{{Protocol}}://free.pagepeeker.com/v2/thumbs.php?size={{Size}}&url={{Url}}',
						'{{Size}}'=>'m',
						'{{Protocol}}'=>'http',
				),
				1 => array(
						'{{Protocol}}://free2.pagepeeker.com/v2/thumbs.php?size={{Size}}&url={{Url}}',
						'{{Size}}'=>'m',
						'{{Protocol}}'=>'http',
				),
				2 => array(
						'{{Protocol}}://free3.pagepeeker.com/v2/thumbs.php?size={{Size}}&url={{Url}}',
						'{{Size}}'=>'m',
						'{{Protocol}}'=>'http',
				),
				3 => array(
						'{{Protocol}}://free4.pagepeeker.com/v2/thumbs.php?size={{Size}}&url={{Url}}',
						'{{Size}}'=>'m',
						'{{Protocol}}'=>'http',
				),
				/*
				0=>array(
						'http://immediatenet.com/t/l?Size=1024x768&URL={{Url}}',
				),
				*/
		),
	),
);