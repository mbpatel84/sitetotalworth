<?php
return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		// console application components
		'components'=>array(
			'request' => array(
				// your host name (With protocol): for example: http://your-domain.com
				'hostInfo' => 'http://localhost',
				// if you installed script not in root directory, then you need to set baseUrl. For example : /sitecost
				'baseUrl' => '',
			),
		),
	)
);