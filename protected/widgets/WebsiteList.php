<?php
class WebsiteList extends Widget {
	public $config = array();
	public $template="website_list";

	public function init() {
		$config = array(
			"criteria"=>array(
				"select"=>"t.id, t.domain, t.idn, t.price",
				"with"=>array(
					"search_engine" => array(
						"select"=>"page_rank",
					),
					"alexa" => array(
						"select"=>"rank",
					),
					"social" => array(
						"select"=>"facebook_like_count",
					)
				),
				"order"=>"t.added_at DESC",
			),
			"countCriteria"=>array(),
			"pagination" => array(
				"pageVar"=>"page",
				"pageSize"=>Yii::app()->params['websitesPerPage'],
			)
		);
		$this->config = CMap::mergeArray($config, $this->config);
	}

	public function run() {
		$dataProvider=new CActiveDataProvider('Website', $this->config);
        $data=$dataProvider->getData();
        if(empty($data)) {
            return null;
        }
		$thumbnailStack=WebsiteThumbnail::thumbnailStack($data);
		$this->render($this->template, array(
			"dataProvider" => $dataProvider,
			"thumbnailStack"=>$thumbnailStack,
            "data"=>$data,
		));
	}
}