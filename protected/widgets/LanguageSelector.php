<?php
class LanguageSelector extends Widget {
	public function run() {
		$languages = Language::model()->getList();
		if(!$languages OR count($languages) < 2) {
			return null;
		}
		$this->render("language_selector", array(
			"languages"=>$languages,
		));
	}
}