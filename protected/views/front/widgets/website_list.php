<script type="text/javascript">
$(document).ready(function(){
    var urls = {
        <?php foreach($thumbnailStack as $id=>$url): ?>
        <?php echo $id ?>:'<?php echo $url ?>',
        <?php endforeach; ?>
    };
    dynamicThumbnail(urls);

	$('#list').click(function(){
		$('.item').removeClass('col-sm-6 col-md-4').addClass('col-xs-12');
        $('.item').find('.image-container').removeClass('col-md-6 col-sm-7 col-xs-4').addClass('col-xs-2');
        $('.item').find('h3').removeClass("text-center").addClass("text-left");

		$(this).removeClass('btn-default').addClass('btn-primary');
		$('#grid').removeClass('btn-primary').addClass('btn-default');
	});
	$('#grid').click(function(){
		$('.item').removeClass('col-xs-12').addClass('col-sm-6 col-md-4');
        $('.item').find('.image-container').removeClass("col-xs-2").addClass('col-md-6 col-sm-7 col-xs-4');
        $('.item').find('h3').removeClass("text-left").addClass("text-center");

		$(this).removeClass('btn-default').addClass('btn-primary');
		$('#list').removeClass('btn-primary').addClass('btn-default');
	});
});
</script>

<div class="btn-group visible-sm visible-lg visible-md">
	<a href="#" id="grid" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-th"></span>&nbsp;<?php echo Yii::t("website", "Grid") ?></a>
	<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>&nbsp;<?php echo Yii::t("website", "List") ?></a>
</div>
<div class="clearfix"></div>
<br/>

<div class="row">
<?php
	foreach ($data as $website):
		$url = Yii::app()->controller->createUrl("website/show", array("domain"=>$website->domain));
?>
	<div class="item col-sm-6 col-md-4">
		<div class="panel panel-default website-container">
            <h3 class="container-header text-center"><?php echo Helper::cropDomain($website->idn) ?></h3>

            <div class="row">
                <div class="image-container col-md-6 col-sm-7 col-xs-4">
                    <a href="<?php echo $url ?>">
                        <img class="img-thumbnail website_ico" id="thumb_<?php echo $website->id ?>" src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/loader.gif" alt="<?php echo $website->idn ?>" />
                    </a>
                </div>
                <div class="text-container col-md-6 col-sm-5 col-xs-8">
                    <small><?php echo Yii::t("website", "Alexa Rank") ?>: <strong><?php echo Helper::f($website->alexa->rank) ?></strong></small>
                    <br/>
                    <small><?php echo Yii::t("website", "Page Rank") ?>: <strong><?php echo $website->search_engine->page_rank ?></strong></small>
                    <br/>
                    <small><?php echo Yii::t("website", "Likes") ?>: <strong><?php echo Helper::f($website->social->facebook_like_count) ?></strong></small>
                    <br/>
                    <small><?php echo Yii::t("website", "Estimate Price") ?>: <strong><?php echo Helper::p($website->price) ?></strong></small>
                </div>
            </div>
            <a class="btn btn-primary btn-sm pull-right" style="margin-right: 25px" href="<?php echo $url ?>">
                <?php echo Yii::t("website", "Explore more") ?>
            </a>
		</div>
	</div>
<?php endforeach; ?>
</div>

<div class="pull-right">
<?php $this -> widget('CLinkPager', array(
	'pages' => $dataProvider->getPagination(),
	'htmlOptions' => array(
		'class' => 'pagination',
	),
	'cssFile' => false,
	'header' => '',
	'hiddenPageCssClass' => 'disabled',
	'selectedPageCssClass' => 'active',
)); ?>
</div>
<div class="clearfix"></div>