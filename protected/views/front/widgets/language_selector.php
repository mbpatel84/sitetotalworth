<li class="dropdown">
<a href="#language-selector" class="dropdown-toggle" data-toggle="dropdown"><?php echo Yii::t("misc", "Language") ?> <b class="caret"></b></a>
<ul class="dropdown-menu">
<?php foreach($languages as $language):?>
<?php if($language->id == Yii::app() -> language) continue;
$url=Yii::app()->controller->createAbsoluteUrl('', array_merge($_GET, array("language"=>$language->id)));
?>
<?php Yii::app() -> clientScript -> registerLinkTag(
'alternate', null, $url, null, array(
	'hreflang' => $language->id,
)); ?>
<li>
<?php echo CHtml::link(Language::formatLanguage($language), $url) ?>
</li>
<?php endforeach; ?>
<li class="divider"></li>
<li><a href="<?php echo Yii::app()->request->url ?>" class="disabled"><?php echo Language::formatLanguage($languages[Yii::app() -> language]) ?></a></li>
</ul>
</li>