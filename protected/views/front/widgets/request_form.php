<script type="text/javascript">
function papulateErrors (obj, errors) {
	for(var e in errors) {
		if(typeof(errors[e]) == 'object')
			papulateErrors(obj, errors[e])
		else
			obj.append(errors[e] + '<br/>');
	}
}

function request() {
	var data = $("#request-form").serializeArray(),
			button = $("#submit"),
			progress = $("#progress-bar"),
			errObj = $("#errors");

	data.push({
		"name":"redirect",
		"value":"<?php echo $this->redirect ?>"
	}, {
		"name":"instant",
		"value":<?php echo (int)$this->instant ?>
	});
	
	errObj.hide();
	progress.show();
	errObj.html('');
	button.attr("disabled", true);
	$.getJSON('<?php echo $this -> requestUrl ?>', data, function(response) {
		button.attr("disabled", false);
		// If response's type is string then all is ok, redirect to statistics
		if(typeof(response) == 'string') {
			document.location.href = response;
			return true;
		}
		// If it's object, then display errors
		papulateErrors(errObj, response);
		progress.hide();
		errObj.show();
	}).error(function(xhr, ajaxOptions, thrownError) {
		papulateErrors(errObj, {0:xhr.status + ': ' + xhr.responseText});
		errObj.show();
		progress.hide();
		button.attr("disabled", false);
	});
}

$(document).ready(function() {
	$("#submit").click(function() {
		request();
		return false;
	});

	$("#website-form input").keypress(function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			request();
			return false;
		}
	});
});
</script>


<div class="jumbotron">
    <h<?php echo $this->hSize ?>><?php echo Yii::app()->name ?></h<?php echo $this->hSize ?>>
    <p class="lead"><?php echo Yii::t("website", "Estimated website cost of any domain") ?></p>
		<div class="row"><!--Widget wrapper-->
				<div class="col-md-12 col-lg-6">
						<form role="form" id="request-form" style="margin-bottom: 21px">
								<div class="form-group">
										<div class="input-group">
												<?php echo CHtml::activeTextField($form, 'domain', array(
														'class' => 'form-control input-lg',
														'placeholder'=>'Enter website to estimate the worth.',
														'id'=>'domain_name',
												)); ?>
												<span class="input-group-btn">
														<button type="submit" id="submit" class="btn btn-primary" style="padding: 14px 28px 15px 28px !important">
																<?php echo Yii::t("website", "Calculate") ?>
														</button>
												</span>
										</div>
								</div>
								<?php if(Helper::isAllowedCaptcha()): ?>
									<div class="form-group">
									<div class="input-group">
										<?php $this->owner->widget('CCaptcha', array(
											'buttonType' => 'button',
											'buttonOptions' => array(
												'class' => 'btn btn-sm btn-default',
											),
											'imageOptions' => array(
												'class' => '',
												'style' => 'float:left;',
											),
										)); ?>
										<div class="col-xs-4">
										<?php echo CHtml::activeTextField($form, 'verifyCode', array('class' => 'form-control')); ?>
										</div>
									</div>
									</div>
								<?php endif; ?>
						</form>

						<div class="alert alert-danger" id="errors" style="margin-top: 21px; display: none"></div>

						<div class="clearfix"></div>

						<div id="progress-bar" class="progress progress-striped active" style="display: none">
								<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
				</div>
		</div><!--End of Widget wrapper-->


		<div class="clearfix"></div>
    <p>
        <?php echo Yii::t("website", "{NumOfWebsites} total website price calculated", array(
            "{NumOfWebsites}"=>'<span class="label label-success">'.Helper::f($total).'</span>',
        )) ?>
    </p>
</div>


