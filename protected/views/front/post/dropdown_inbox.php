<li>
<a href="<?php echo $this->createUrl("post/mark-as-read") ?>" class="inbox-operation">
	<i class="fa fa-pencil"></i>&nbsp;<?php echo Yii::t("post", "Mark as Read") ?>
</a>
</li>
<li>
<a href="<?php echo $this->createUrl("post/mark-as-star") ?>" class="inbox-operation">
	<i class="fa fa-star"></i>&nbsp;<?php echo Yii::t("post", "Mark as Starred") ?>
</a>
</li>
<li>
<a href="<?php echo $this->createUrl("post/mark-as-important") ?>" class="inbox-operation">
	<i class="fa fa-bookmark"></i>&nbsp;<?php echo Yii::t("post", "Mark as Important") ?>
</a>
</li>
<li>
<a href="<?php echo $this->createUrl("post/move-to-trash") ?>" class="inbox-operation">
	<i class="fa fa-trash-o"></i>&nbsp;<?php echo Yii::t("post", "Move to Trash") ?>
</a>
</li>