<script type="text/javascript">
$(document).ready(function() {
	<?php if($website): ?>
	dynamicThumbnail({<?php echo $website->id ?>:'<?php echo WebsiteThumbnail::getUrl(array("{{Url}}"=>$website->domain)) ?>'});
	<?php endif; ?>
});
</script>

<div class="row">
    <div class="col-lg-8">
        <?php echo $widget ?>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-<?php echo $website ? "primary" : "warning" ?>">
            <div class="panel-heading">
                <h3 class="panel-title">
                <span class="fa-stack">
                <i class="fa fa-globe fa-stack-1x"></i>
                <?php if(!$website): ?>
                <i class="fa fa-ban fa-stack-2x text-danger"></i>
                <?php endif; ?>
                </span>
                &nbsp;<?php echo $website ? $website->idn : Yii::t("post", "Removed from sale"); ?></h3>
            </div>
            <div class="panel-body">
                <?php if(!$website): ?>
                <p><?php echo Yii::t("post", "The website has been sold or removed from sales.") ?></p>
                <?php else: ?>
                <img id="thumb_<?php echo $website->id ?>" class="img-thumbnail img-responsive" src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/loader.gif" alt="<?php echo $website->idn ?>">
                <br/>
                <table class="table custom-border">
                        <tr>
                                <td width="30%">
                                        <?php echo Yii::t("website", "Estimate Price"); ?>
                                </td>
                                <td>
                                        <strong><?php echo Helper::p($website->price) ?></strong>
                                </td>
                        </tr>
                        <tr>
                                <td>
                                        <?php echo Yii::t("website", "Selling price") ?>
                                </td>
                                <td>
                                        <strong><?php echo Helper::p($sale->price) ?></strong>
                                </td>
                        </tr>
                        <tr>
                                <td>
                                        <?php echo Yii::t("website", "Unique monthly visitors") ?>
                                </td>
                                <td>
                                        <strong><?php echo Helper::f($sale->monthly_visitors) ?></strong>
                                </td>
                        </tr>
                        <tr>
                                <td>
                                        <?php echo Yii::t("website", "Monthly page view") ?>
                                </td>
                                <td>
                                        <strong><?php echo Helper::f($sale->monthly_views) ?></strong>
                                </td>
                        </tr>
                        <tr>
                                <td>
                                        <?php echo Yii::t("website", "Monthly revenue") ?>
                                </td>
                                <td>
                                        <strong><?php echo Helper::p($sale->monthly_revenue) ?></strong>
                                </td>
                        </tr>
                </table>
                    <h4><?php echo Yii::t("website", "Notes") ?></h4>
                    <i class="fa fa-quote-left fa-3x pull-left fa-border"></i>
                    <p style="word-wrap: break-word;"><?php echo Helper::mb_ucfirst(CHtml::encode($website->sale->description)) ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
