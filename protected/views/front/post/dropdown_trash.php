<li>
<a href="<?php echo $this->createUrl("post/restore-from-trash") ?>" class="inbox-operation">
	<i class="fa fa-mail-reply-all"></i>&nbsp;<?php echo Yii::t("post", "Restore from Trash folder") ?>
</a>
</li>
<li>
<a href="<?php echo $this->createUrl("post/completely-remove") ?>" class="inbox-operation">
	<i class="fa fa-trash-o"></i>&nbsp;<?php echo Yii::t("post", "Completely remove") ?>
</a>
</li>