<script type="text/javascript">
    $(document).ready(function(){
        $("#checkAll").on("click", function(){
            if ($(this).is(':checked')) {
                $(".headers").prop("checked", true);
            } else {
                $(".headers").prop("checked", false);
            }
        });
        $(".inbox-operation").on("click", function() {
					var $href=$(this).attr("href");
					var form=document.getElementById("header_form");
					form.action=$href;
					form.submit();
					return false;
        });
    })
</script>
<h1><i class="fa fa-inbox"></i>&nbsp;<?php echo Yii::t("post", "My messages") ?></h1>
<hr>

<div class="row email">
    <div id="list-toggle" class="col-md-2">
        <ul class="emailNav nav nav-pills nav-stacked margin-bottom-10">
            <li class="inbox<?php echo $folder==UserInnerMailBox::FOLDER_INBOX ? " active" : null ?>">
                <a href="<?php echo $this->createUrl("post/index", array("f"=>UserInnerMailBox::FOLDER_INBOX)) ?>">
                <i class="fa fa-inbox fa-fw"></i> <?php echo Yii::t("post", "folder_inbox") ?>
                <?php echo $this->newMessages > 0 ? "(". $this->newMessages .")" : null; ?>
                </a>
            </li>
            <li class="sent<?php echo $folder==UserInnerMailBox::FOLDER_SENT ? ' active' : null ?>">
                <a href="<?php echo $this->createUrl("post/index", array("f"=>UserInnerMailBox::FOLDER_SENT)) ?>">
                <i class="fa fa-mail-forward fa-fw"></i> <?php echo Yii::t("post", "folder_sent") ?>
                </a>
            </li>
            <li class="starred<?php echo $folder==UserInnerMailBox::FOLDER_STARRED ? ' active' : null ?>">
            <a href="<?php echo $this->createUrl("post/index", array("f"=>UserInnerMailBox::FOLDER_STARRED)) ?>">
                <i class="fa fa-star fa-fw"></i> <?php echo Yii::t("post", "folder_starred") ?>
            </a>
            </li>
            <li class="important<?php echo $folder==UserInnerMailBox::FOLDER_IMPORTANT ? ' active' : null ?>">
            <a href="<?php echo $this->createUrl("post/index", array("f"=>UserInnerMailBox::FOLDER_IMPORTANT)) ?>">
                <i class="fa fa-bookmark fa-fw"></i> <?php echo Yii::t("post", "folder_important") ?>
            </a>
            </li>
            <li class="trash<?php echo $folder==UserInnerMailBox::FOLDER_TRASH ? ' active' : null ?>">
                <a href="<?php echo $this->createUrl("post/index", array("f"=>UserInnerMailBox::FOLDER_TRASH)) ?>">
                    <i class="fa fa-trash-o fa-fw"></i> <?php echo Yii::t("post", "folder_trash") ?>
                </a>
            </li>
            <li class="spam">
            <a href="<?php echo $this->createUrl("post/blocked-users") ?>">
                <span class="fa-stack">
                  <i class="fa fa-user fa-stack-1x"></i>
                  <i class="fa fa-ban fa-stack-2x text-danger"></i>
                </span><?php echo Yii::t("post", "Blocked users") ?>
            </a>
            </li>
        </ul>
    </div>
    <div class="col-md-10">
          <table class="table table-hover">
             <thead>
                <tr>
                    <th>
                        <span><input type="checkbox" id="checkAll">
                    </th>
                   <th colspan="3">
                         <div class="btn-group">
                             <button class="btn btn-light-grey dropdown-toggle" data-toggle="dropdown"> <?php echo Yii::t("post", "Action") ?>
                                <i class="fa fa-caret-down"></i>
                             </button>
                             <ul class="dropdown-menu context" role="menu">
															<?php echo $dropDownItems ?>
                             </ul>
                         </div>
                   </th>
                    <th colspan="3">
                        <a href="<?php echo $this->createUrl("post/index") ?>" class="btn btn-default pull-right">
                            <i class="fa fa-refresh"></i>&nbsp;<?php echo Yii::t("post", "Refresh") ?>
                        </a>
                    </th>
                </tr>
             </thead>
             <tbody>
							<form method="POST" id="header_form">
                <?php foreach($headers as $header): ?>
                    <?php
                        $new=$box->isNew($header['status']);
                        $username=CHtml::encode(Helper::mb_ucfirst($senders[$header['companion_id']]->username));
                        $subject=CHtml::encode(Helper::mb_ucfirst($header['subject']));
                        $url=$this->createUrl("post/chain", array("id"=>$header['id']));
                    ?>
                    <tr<?php echo $box->isNew($header['status']) ? ' class="active"' : null ?>>
                        <td width="15px">
                            <input type="checkbox" class="headers" name="header[]" value="<?php echo $header['id'] ?>">
                        </td>
                        <td width="15px"><i class="fa fa-star<?php echo !$box->isInStarredFolder($header['id'], $state) ? "-o" : null ?>"></i></td>
                        <td width="15px"><i class="fa fa-bookmark<?php echo !$box->isInImportantFolder($header['id'], $state) ? "-o" : null ?>"></i></td>
                        <td>
													<a class="mail-link" href="<?php echo $url; ?>">
													<?php if($new): ?>
															<strong><?php echo $username ?></strong>
													<?php else: ?>
															<?php echo $username ?>
													<?php endif; ?>
													</a>
                        </td>
                        <td>
													<a class="mail-link" href="<?php echo $url; ?>">
                            <?php if($new): ?>
                                <span class="label label-success"><?php echo Yii::t("post", "New") ?></span>
                                <strong><?php echo $subject ?></strong>
                            <?php else: ?>
                                <?php echo $subject ?>
                            <?php endif; ?>
													</a>
                        </td>
                        <td class="text-right">
													<a class="mail-link" href="<?php echo $url; ?>">
														<?php echo Helper::time_elapsed_string($header['appeared_date']) ?>
													</a>
												</td>
                    </tr>
                <?php endforeach; ?>
							</form>
             </tbody>

             <thead>
                <tr>
                   <th colspan="6" class="text-center">
                        <a href="<?php echo $this->createUrl("post/index", array("f"=>$folder, "page"=>$pgNr-1)) ?>" class="btn btn-sm btn-primary<?php echo ($pgNr<=1) ? " disabled" : null?>">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a href="<?php echo $this->createUrl("post/index", array("f"=>$folder, "page"=>$pgNr+1)) ?>" class="btn btn-sm btn-primary<?php echo ($pgNr>=$pgCnt) ? " disabled" : null?>">
                            <i class="fa fa-angle-right"></i>
                        </a>
                       <span class="pull-right"><?php echo $summaryText ?></span>
                   </th>
                </tr>
             </thead>
          </table>
    </div>
</div>