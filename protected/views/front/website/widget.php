<div style="max-width:200px; font-size: 12px; word-wrap: break-word; text-align:center; background-color: #ffffff; border: 1px solid #dddddd; -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.05); box-shadow: 0 1px 1px rgba(0,0,0,0.05);">
  <div style="color: #333333; background-color: #f5f5f5; padding:5px; border-bottom: 1px solid #dddddd;">
    <a style="color: #008cba; text-decoration: underline;" href="<?php echo $url ?>" target="_blank">
      <?php echo $domain ?>
    </a>
  </div>
  <div style="padding:5px; color: #222222; line-height: 1.5;">
    <?php echo Yii::t("website", "Estimated worth") ?><br/>
    &#8226;&nbsp;<span style="font-size: 14px"><strong><?php echo $price ?></strong></span>&nbsp;&#8226;
  </div>
</div>