<script type="text/javascript">
$(document).ready(function(){
	var pie_data = [];

	<?php $i=0; foreach($pageRankIndex as $pr => $total) : ?>
	pie_data[<?php echo $i ?>] = {
		label: '&nbsp;&nbsp;<?php echo Yii::t("website", "Page Rank") ." ". $pr . ' ('. Helper::proportion($sum, $total). '%)' ?>',
		data: <?php echo $total ?>,
	}
	<?php $i++; endforeach; ?>

	drawFlot();
	window.onresize = function(event) {
		drawFlot();
	}
	function drawFlot() {
		$.plot($("#pr-pie"), pie_data, {
			series: {
				pie: {
					show: true
				}
			}
		});
	}

});
</script>

<h1 align="center"><?php echo CHtml::encode($this->title) ?></h1>

<div class="row">
	<div id="pr-pie" style="max-width:400px;height:200px; margin:0 auto;"></div>
</div>
<br/>
<?php $this->renderPartial("top_breadcrumbs") ?>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th><?php echo Yii::t("website", "Page Rank") ?></th>
			<th><?php echo Yii::t("website", "Number of websites") ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($prRange as $pr): $url = $this->createUrl("website/page-rank", array("id"=>$pr)) ?>
		<tr>
			<td class="cell-link" width="80%">
				<a href="<?php echo $url ?>">
					<?php echo $pr ?>
				</a>
			</td>
			<td class="cell-link">
				<a href="<?php echo $url ?>">
					<?php echo isset($pageRankIndex[$pr]) ? $pageRankIndex[$pr] : 0 ?>
				</a>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

