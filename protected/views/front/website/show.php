<script type="text/javascript">
    $(document).ready(function(){
        dynamicThumbnail({<?php echo $website->id ?>:'<?php echo $thumbnail ?>'});
    });
</script>

<h1 style="word-wrap: break-word;">
<?php echo Yii::t("website", "How much {Website} is worth?", array(
	"{Website}"=>$website->idn,
))?>
</h1>
<hr>
<div class="jumbotron">
    <div class="row">
        <div class="col-md-4 col-lg-6 col-sm-12">
            <img class="img-responsive img-thumbnail" id="thumb_<?php echo $website->id ?>" src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/loader.gif" alt="<?php echo $website->idn ?>" />
        </div>
        <div class="col-md-8 col-lg-6 col-sm-12 text-left">
            <h2 style="word-wrap: break-word;"><?php echo $website->idn ?></h2>
            <p><?php echo Yii::t("website", "Has Estimated Worth of") ?></p>
            <h2>
                <span class="label label-success"><?php echo Helper::p($website->price) ?></span>
                <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/coins.png" alt="<?php echo Yii::t("website", "Coins") ?>">
            </h2>
            <br/>
            <small>
                <?php echo Yii::t("website", "Site Price calculated at: {Time}", array(
									"{Time}"=>$time,
                )); ?>
                <?php if($update): ?>
                &nbsp;&nbsp;
                <?php if(!Helper::isAllowedCaptcha()) : ?>
                <a href="<?php echo $updateLink ?>"><span class="glyphicon glyphicon glyphicon-refresh refresh-icon"></span></a>
                <?php else: ?>
                <a href="#domain_name" id="recalculate"><span class="glyphicon glyphicon glyphicon-refresh refresh-icon"></span></a>
                <script type="text/javascript">
                $("#recalculate").on("click", function() {
                    $("#domain_name").val("<?php echo $website->idn ?>");
                });
                </script>
                <?php endif; ?>
                <?php endif; ?>
            </small>
            <p>
                <br/>
                <?php echo Yii::app()->params["shareJS"] ."\n". Yii::app()->params["shareHTML"] ?>
            </p>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="panel panel-default">
	<div class="panel-heading">
			<h3 class="panel-title">
					<i class="fa fa-gears"></i>
					&nbsp;
					<?php echo Yii::t("website", "More actions") ?>
			</h3>
	</div>
	<div class="panel-body">
			<div class="col-xs-12 btn-group">
					<a class="btn btn-sm btn-success" href="http://website-review.php5developer.com" title="" target="_blank">
							<i class="fa fa-list-alt"></i>&nbsp;
							<?php echo Yii::t("website", "Get website review") ?>
					</a>
					<a class="btn btn-sm btn-info" href="http://webmaster-tools.php5developer.com" title="" target="_blank">
							<i class="fa fa-gear"></i>&nbsp;
							<?php echo Yii::t("website", "Webmaster info") ?>
					</a>
					<a class="btn btn-sm btn-warning" href="http://catalog.php5developer.com" title="" target="_blank">
							<i class="fa fa-folder-open"></i>&nbsp;
							<?php echo Yii::t("website", "Add to catalog. Free") ?>
					</a>
					<a class="btn btn-sm btn-primary" href="<?php echo $this->createUrl("website/show", array("domain"=>$website->domain, "#"=>"widget")) ?>">
							<i class="fa fa-share-alt"></i>&nbsp;
							<?php echo Yii::t("website", "Get widget / Sale website") ?>
					</a>
					<a class="btn btn-sm btn-default" href="<?php echo $this->createUrl("website/show", array("domain"=>$website->domain, "#"=>"estimate")) ?>">
							<i class="fa fa-fax"></i>&nbsp;
							<?php echo Yii::t("website", "Estimate other website") ?>
					</a>
			</div>
	</div>
</div>

<?php if($website->sale): ?>
<div class="row">
    <div class="col-md-12">
        <h2>
            <strong style="color:#fff; background-color: #b3232e; border: 1px solid #8b0025">&nbsp;<?php echo Yii::t("website", "This website is on sale") ?> </strong><img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/sale.png" alt="<?php echo Yii::t("website", "This website is on sale") ?>">
        </h2>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/bank.png" alt="<?php echo Yii::t("website", "Sales Information") ?>">
            &nbsp;
            <?php echo Yii::t("website", "Sales Information") ?>
        </h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table custom-border">
                    <tr>
                        <td class="width30pers">
                            <?php echo Yii::t("website", "Selling price") ?>
                        </td>
                        <td>
                            <strong><?php echo Helper::p($website->sale->price) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "Unique monthly visitors") ?>
                        </td>
                        <td>
                            <strong><?php echo CHtml::encode(Helper::f($website->sale->monthly_visitors)) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "Monthly page view") ?>
                        </td>
                        <td>
                            <strong><?php echo CHtml::encode(Helper::f($website->sale->monthly_views)) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "Monthly revenue") ?>
                        </td>
                        <td>
                            <strong><?php echo Helper::p($website->sale->monthly_revenue) ?></strong>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12">
                <h4><i class="fa fa-quote-left pull-left fa-border fa-lg"></i> <?php echo Yii::t("website", "Notes") ?></h4>

                <p style="word-wrap: break-word;">
                    <?php echo Helper::mb_ucfirst(CHtml::encode($website->sale->description)) ?>
                </p>
            </div>
        </div>

        <?php if(Yii::app()->user->isGuest): ?>
        <br/>
				<div class="alert alert-success">
					<?php echo Yii::t("website", "You should login to contact seller") ?>
				</div>
        <?php elseif($block AND $block['external']): ?>
					<div class="alert alert-danger">
						<?php echo Yii::t("website", "Seller has blocked you") ?>
					</div>
        <?php elseif($block AND $block['internal']): ?>
					<div class="alert alert-danger">
					<?php echo Yii::t("website", "You have blocked this user. {Click here} to unblock", array(
						"{Click here}"=>CHtml::link(Yii::t("misc", "Click here"), $this->createUrl("post/unblock-sender", array("id"=>$website->sale->user->id))),
					)) ?>
					</div>
        <?php elseif(Yii::app()->user->id != $website->sale->user_id AND Yii::app()->user->loadModel()->canSendMessage()): ?>
            <a class="btn btn-primary" href="<?php echo $this->createUrl("post/send", array("id"=>$website->id)) ?>">
							<?php echo Yii::t("website", "Contact seller") ?>
						</a>
        <?php endif; ?>
    </div>
</div>
<?php else: ?>
<br/>
<?php endif; ?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/analytics.png" alt="<?php echo Yii::t("website", "Estimated Data Analytics") ?>">
            &nbsp;
            <?php echo Yii::t("website", "Estimated Data Analytics") ?>
        </h3>
    </div>
    <div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<table class="table custom-border">
						<thead>
							<tr>
								<th colspan="2">
									<?php echo Yii::t("website", "Estimated Daily Stats") ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="vertical-align: middle">
									<img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/visitors.png" alt="<?php echo Yii::t("website", "Daily Unique Visitors") ?>">
									&nbsp;
									<?php echo Yii::t("website", "Daily Unique Visitors") ?>
								</td>
								<td style="vertical-align: middle"><?php echo $dailyVisitors ? Helper::f($dailyVisitors) : "n/a" ?></td>
							</tr>
							<tr>
								<td style="vertical-align: middle">
									<img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/pageviews.png" alt="<?php echo Yii::t("website", "Daily Pageviews") ?>">
									&nbsp;
									<?php echo Yii::t("website", "Daily Pageviews") ?>
								</td>
								<td style="vertical-align: middle"><?php echo $dailyPageviews ? Helper::f($dailyPageviews) : "n/a" ?></td>
							</tr>
							<tr>
								<td style="vertical-align: middle">
									<img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/revenue.png" alt="<?php echo Yii::t("website", "Daily Ads Revenue") ?>">
									&nbsp;
									<?php echo Yii::t("website", "Daily Ads Revenue") ?>
								</td>
								<td style="vertical-align: middle"><?php echo $dailyAdsRevenue ? Helper::formatCurrencyPrice($dailyAdsRevenue) : "n/a" ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-4">
					<table class="table custom-border">
						<thead>
							<tr>
								<th colspan="2">
									<?php echo Yii::t("website", "Estimated Monthly Stats") ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="vertical-align: middle">
									<img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/visitors.png" alt="<?php echo Yii::t("website", "Estimated Monthly Stats") ?>">
									&nbsp;
									<?php echo Yii::t("website", "Monthly Unique Visitors") ?>
								</td>
								<td style="vertical-align: middle"><?php echo $monthlyVisitors ? Helper::f($monthlyVisitors) : "n/a" ?></td>
							</tr>
							<tr>
								<td style="vertical-align: middle">
									<img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/pageviews.png" alt="<?php echo Yii::t("website", "Monthly Pageviews") ?>">
									&nbsp;
									<?php echo Yii::t("website", "Monthly Pageviews") ?>
								</td>
								<td style="vertical-align: middle"><?php echo $monthlyPageviews ? Helper::f($monthlyPageviews) : "n/a" ?></td>
							</tr>
							<tr>
								<td style="vertical-align: middle">
									<img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/revenue.png" alt="<?php echo Yii::t("website", "Monthly Ads Revenue") ?>">
									&nbsp;
									<?php echo Yii::t("website", "Monthly Ads Revenue") ?>
								</td>
								<td style="vertical-align: middle"><?php echo $monthlyAdsRevenue ? Helper::formatCurrencyPrice($monthlyAdsRevenue) : "n/a" ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-4">
					<table class="table custom-border">
						<thead>
							<tr>
								<th colspan="2">
									<?php echo Yii::t("website", "Estimated Yearly Stats") ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="vertical-align: middle">
									<img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/visitors.png" alt="<?php echo Yii::t("website", "Yearly Unique Visitors") ?>">
									&nbsp;
									<?php echo Yii::t("website", "Yearly Unique Visitors") ?>
								</td>
								<td style="vertical-align: middle"><?php echo $yearlyVisitors ? Helper::f($yearlyVisitors) : "n/a" ?></td>
							</tr>
							<tr>
								<td style="vertical-align: middle">
									<img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/pageviews.png" alt="<?php echo Yii::t("website", "Yearly Pageviews") ?>">
									&nbsp;
									<?php echo Yii::t("website", "Yearly Pageviews") ?>
								</td>
								<td style="vertical-align: middle"><?php echo $yearlyPageviews ? Helper::f($yearlyPageviews) : "n/a" ?></td>
							</tr>
							<tr>
								<td style="vertical-align: middle">
									<img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/revenue.png" alt="<?php echo Yii::t("website", "Yearly Ads Revenue") ?>">
									&nbsp;
									<?php echo Yii::t("website", "Yearly Ads Revenue") ?>
								</td>
								<td style="vertical-align: middle"><?php echo $yearlyAdsRevenue ? Helper::formatCurrencyPrice($yearlyAdsRevenue) : "n/a" ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/info.png" alt="<?php echo Yii::t("website", "Basic information") ?>">
            &nbsp;
            <?php echo Yii::t("website", "Basic information") ?>
        </h3>
    </div>
    <div class="panel-body">
        <table class="table custom-border">
            <tr>
                <td class="width30pers">
                    <?php echo Yii::t("website", "Domain name") ?>
                </td>
                <td>
                    <strong><?php echo $website->idn ?></strong>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo Yii::t("website", "Title") ?>
                </td>
                <td>
                    <?php echo CHtml::encode(html_entity_decode($website->meta_tags->title)) ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo Yii::t("website", "Keywords") ?>
                </td>
                <td>
                    <?php echo CHtml::encode(html_entity_decode($website->meta_tags->keywords)) ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo Yii::t("website", "Description") ?>
                </td>
                <td>
                    <?php echo CHtml::encode(html_entity_decode($website->meta_tags->description)) ?>
                </td>
            </tr>
        </table>
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/search_engine.png" alt="<?php echo Yii::t("website", "Search Engine Stats") ?>">
            &nbsp;
            <?php echo Yii::t("website", "Search Engine Stats") ?>
        </h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <table class="table custom-border">
                    <tr>
                        <td class="vmiddle width30pers">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/google.png" alt="<?php echo Yii::t("website", "Google Index") ?>">
                            &nbsp;
                            <?php echo Yii::t("website", "Google Index") ?>
                        </td>
                        <td class="vmiddle">
                            <strong><?php echo Helper::f($website->search_engine->google_index) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/yahoo.png" alt="<?php echo Yii::t("website", "Yahoo Index") ?>">
                            &nbsp;
                            <?php echo Yii::t("website", "Yahoo Index") ?>
                        </td>
                        <td class="vmiddle">
                            <strong><?php echo Helper::f($website->search_engine->yahoo_index) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/bing.png" alt="<?php echo Yii::t("website", "Bing Index") ?>">
                            &nbsp;
                            <?php echo Yii::t("website", "Bing Index") ?>
                        </td>
                        <td class="vmiddle">
                            <strong><?php echo Helper::f($website->search_engine->bing_index) ?></strong>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <table class="table custom-border">
                    <tr>
                        <td class="width40pers" style="vertical-align: middle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/page_rank_icon.png" width="32" height="32" alt="<?php echo Yii::t("website", "Page Rank") ?>">
                            &nbsp;
                            <?php echo Yii::t("website", "Page Rank") ?>
                        </td>
                        <td>
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/pr/<?php echo $website->search_engine->page_rank ?>.png" alt="<?php echo Yii::t("website", "Page Rank"). " ". $website->search_engine->page_rank ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/backlink.png" alt="<?php echo Yii::t("website", "Google Backlinks") ?>">
                            &nbsp;
                            <?php echo Yii::t("website", "Google Backlinks") ?>
                        </td>
                        <td class="vmiddle">
                            <strong><?php echo Helper::f($website->search_engine->google_backlinks) ?></strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">

        <h3 class="panel-title">
            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/alexa.png" alt="<?php echo Yii::t("website", "Alexa Stats") ?>">
            <?php echo Yii::t("website", "Alexa Stats") ?>
            &nbsp;
        </h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <table class="table custom-border">
                    <tr>
                        <td class="width30pers">
                            <?php echo Yii::t("website", "Global Rank") ?>
                        </td>
                        <td>
                            <strong><?php echo Helper::f($website->alexa->rank) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <?php echo Yii::t("website", "Links in") ?>
                        </td>
                        <td>
                            <strong><?php echo Helper::f($website->alexa->linksin) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "Review count") ?>
                        </td>
                        <td>
                            <strong><?php echo Helper::f($website->alexa->review_count) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "Review average") ?>
                        </td>
                        <td>
                            <strong><?php echo $website->alexa->review_avg ?></strong>
                        </td>
                    </tr>
                </table>
            </div>

            <?php if($website->alexa->country_code OR $website->alexa->country_rank OR $website->alexa->speed_time OR $website->alexa->pct): ?>
            <div class="col-md-6">
                <table class="table custom-border">
                    <?php if($website->alexa->country_rank) :?>
                    <tr>
                        <td class="vmiddle width30pers">
                            <?php echo Yii::t("website", "Local Rank") ?>
                        </td>
                        <td>
                            <strong><?php echo $website->alexa->country_rank ?></strong>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php if($website->alexa->country_code) :?>
                    <tr>
                        <td class="vmiddle">
                            <?php echo Yii::t("website", "Country") ?>
                        </td>
                        <td class="vmiddle">
                            <?php echo $country->getCountryName($website->alexa->country_code, '--') ?>
                            <?php echo Helper::getFlagUrl($website->alexa->country_code); ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php if($website->alexa->speed_time AND $website->alexa->pct) :?>
                        <tr>
                            <td>
                                <?php echo Yii::t("website", "Load speed") ?>
                            </td>
                            <td>
                                <?php echo Yii::t("website", "{Seconds} Seconds", array(
                                    "{Seconds}"=>Helper::f($website->alexa->speed_time/1000, 3)
                                )) ?>
                                &nbsp;
                                (<?php echo Helper::alexaSpeed($website->alexa->pct); ?>)
                            </td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
            <?php endif; ?>
            <div class="clearfix"></div>
            <img class="img-thumbnail" style="margin: 15px 0 0 15px" src="http://traffic.alexa.com/graph?&amp;w=320&amp;h=230&amp;o=f&amp;c=1&amp;y=t&amp;b=ffffff&amp;r=1m&amp;u=<?php echo $website->domain ?>" alt="<?php echo Yii::t("website", "Daily Global Rank Trend") ?>">
            <img class="img-thumbnail" style="margin: 15px 15px 0 15px" src="http://traffic.alexa.com/graph?&amp;w=320&amp;h=230&amp;o=f&amp;c=1&amp;y=r&amp;b=ffffff&amp;r=1m&amp;u=<?php echo $website->domain ?>" alt="<?php echo Yii::t("website", "Daily Reach (Percent)") ?>">
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/facebook.png" alt="<?php echo Yii::t("website", "Facebook Stats") ?>">
                    &nbsp;
                    <?php echo Yii::t("website", "Facebook Stats") ?>
                </h3>
            </div>
            <div class="panel-body">
                <table class="table custom-border">
                    <tr>
                        <td class="width30pers">
                            <?php echo Yii::t("website", "Like count") ?>
                        </td>
                        <td>
                            <strong><?php echo Helper::f($website->social->facebook_like_count) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "Share count") ?>
                        </td>
                        <td>
                            <strong><?php echo Helper::f($website->social->facebook_share_count) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "Comment count") ?>
                        </td>
                        <td>
                            <strong><?php echo Helper::f($website->social->facebook_comment_count) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "Click count") ?>
                        </td>
                        <td>
                            <strong><?php echo Helper::f($website->social->facebook_click_count) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "Total count") ?>
                        </td>
                        <td>
                            <strong><?php echo Helper::f($website->social->facebook_total_count) ?></strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/social.png" alt="<?php echo Yii::t("website", "Social Stats") ?>">
                    &nbsp;
                    <?php echo Yii::t("website", "Social Stats") ?>
                </h3>
            </div>
            <div class="panel-body">
                <table class="table custom-border">
                    <tr>
                        <td class="width30pers vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/twitter.png" alt="<?php echo Yii::t("website", "Twitter") ?>">
                            &nbsp;
                            <?php echo Yii::t("website", "Twitter") ?>
                        </td>
                        <td class="vmiddle">
                            <strong><?php echo Helper::f($website->social->twitter) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/gplus.png" alt="<?php echo Yii::t("website", "Gplus+") ?>">
                            &nbsp;
                             <?php echo Yii::t("website", "Gplus+") ?>
                        </td>
                        <td class="vmiddle">
                            <strong><?php echo Helper::f($website->social->gplus) ?></strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/antivirus.png" alt="<?php echo Yii::t("website", "Antivirus Stats") ?>">
                    &nbsp;
                    <?php echo Yii::t("website", "Antivirus Stats") ?>
                </h3>
            </div>
            <div class="panel-body">
                <table class="table custom-border">
                    <tr>
                        <td class="width30pers vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/google.png" alt="<?php echo Yii::t("website", "Google") ?>">
                            &nbsp;
                            <?php echo Yii::t("website", "Google") ?>
                        </td>
                        <td class="vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/<?php echo $website->antivirus->google ?>.png" alt="<?php echo $website->antivirus->google ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/avg.png" alt="<?php echo Yii::t("website", "AVG") ?>">
                            &nbsp;
                            <?php echo Yii::t("website", "AVG") ?>
                        </td>
                        <td class="vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/<?php echo $website->antivirus->avg ?>.png" alt="<?php echo $website->antivirus->avg ?>">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/catalog.png" alt="<?php echo Yii::t("website", "Catalog Stats") ?>">
                    &nbsp;
                    <?php echo Yii::t("website", "Catalog Stats") ?>
                </h3>
            </div>
            <div class="panel-body">
                <table class="table custom-border">
                    <tr>
                        <td class="width30pers vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/yahoo.png" alt="<?php echo Yii::t("website", "Yahoo") ?>">
                            &nbsp;
                            <?php echo Yii::t("website", "Yahoo") ?>
                        </td>
                        <td class="vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/<?php echo $website->catalog->yahoo ? "success" : "failed" ?>.png" alt="<?php echo $website->catalog->yahoo ? Yii::t("website", "Success") : Yii::t("website", "Failed") ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/dmoz.png" alt="<?php echo Yii::t("website", "Dmoz") ?>">
                            &nbsp;
                            <?php echo Yii::t("website", "Dmoz") ?>
                        </td>
                        <td class="vmiddle">
                            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/<?php echo $website->catalog->dmoz ? "success" : "failed" ?>.png" alt="<?php echo $website->catalog->dmoz ? Yii::t("website", "Success") : Yii::t("website", "Failed") ?>">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/location.png" alt="<?php echo Yii::t("website", "Location Stats") ?>">
            &nbsp;
            <?php echo Yii::t("website", "Location Stats") ?>
        </h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <table class="table custom-border">
                    <tr>
                        <td class="width30pers">
                            <?php echo Yii::t("website", "IP Address") ?>
                        </td>
                        <td>
                            <strong><?php echo CHtml::encode($website->location->ip) ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="vmiddle">
                           <?php echo Yii::t("website", "Country") ?>
                        </td>
                        <td class="vmiddle">
                            <strong><?php echo $country->getCountryName($website->location->country_code, strtoupper($website->location->country_code)) ?></strong>
                            <?php echo Helper::getFlagUrl($website->location->country_code); ?>
                        </td>
                    </tr>
                    <?php if($website->location->region_name): ?>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "Region") ?>
                        </td>
                        <td>
                            <strong><?php echo CHtml::encode($website->location->region_name) ?></strong>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php if($website->location->city): ?>
                    <tr>
                        <td>
                            <?php echo Yii::t("website", "City") ?>
                        </td>
                        <td>
                            <strong><?php echo CHtml::encode($website->location->city)?></strong>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php if($website->location->longitude OR $website->location->latitude): ?>
                        <tr>
                            <td>
                                <?php echo Yii::t("website", "Longitude") ?>
                            </td>
                            <td>
                                <strong><?php echo CHtml::encode($website->location->longitude) ?></strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo Yii::t("website", "Latitude") ?>
                            </td>
                            <td>
                                <strong><?php echo CHtml::encode($website->location->latitude)?></strong>
                            </td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
            <?php if(Yii::app()->params['googleMapsAPIKey']): ?>
            <div class="col-md-6">
                <img class="img-thumbnail img-responsive" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $website->location->latitude ?>,<?php echo $website->location->longitude ?>&amp;sensor=false&amp;zoom=5&amp;size=640x250&amp;markers=<?php echo $website->location->latitude ?>,<?php echo $website->location->longitude ?>&amp;key=<?php echo Yii::app()->params['googleMapsAPIKey'] ?>" alt="<?php echo $website->idn ?>">
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <img src="<?php echo Yii::app() -> getBaseUrl(true) ?>/images/whois.png" alt="<?php echo Yii::t("website", "WHOIS") ?>">
            &nbsp;
            <?php echo Yii::t("website", "WHOIS") ?>
            <div class="pull-right">
                <a class="btn btn-default btn-xs dropdown-toggle" id="dropdown-toggle" data-toggle="collapse" href="#collapseWhois">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
            </div>
        </h3>
    </div>
    <div class="panel-collapse collapse" id="collapseWhois">
        <div class="panel-body">
            <?php echo str_replace("\n", "<br/>", CHtml::encode($website->whois->text)) ?>
        </div>
    </div>
</div>

<a id="widget"></a>
<div class="jumbotron">
	<h3 class="no-top-margin"><?php echo Yii::t("website", "Show Your Visitors Your Website Value") ?></h3>
	<div class="row">
		<div class="col-sm-6 col col-md-3  col-lg-3" style="margin-bottom:21px">
			<?php echo $widget ?>
		</div>

		<div class="col-sm-6 col-md-9 col-lg-9">
			<div class="panel panel-default">
					<div class="panel-heading">
							<h3 class="panel-title">
									<i class="fa fa-code fa-lg"></i>&nbsp;<?php echo Yii::t("website", "Get code") ?>
									<div class="pull-right">
											<a class="btn btn-default btn-xs dropdown-toggle" data-toggle="collapse" href="#collapseWidget">
													<span class="glyphicon glyphicon-chevron-down"></span>
											</a>
									</div>
							</h3>
					</div>
					<div class="panel-collapse collapse" id="collapseWidget">
							<div class="panel-body">
									<textarea rows="3" class="form-control" onclick="this.focus();this.select()" readonly="readonly">
									<?php echo trim(CHtml::encode($widget)) ?>
									</textarea>
							</div>
					</div>
			</div>
		</div>
	</div>
	<?php if(!$website->sale): ?>
	<div class="row">
		<div class="col-lg-12">
			<p>
				<?php echo Yii::t("website", "Website owner? {Sale Website}!", array(
					"{Sale Website}"=>CHtml::link(Yii::t("website", "Sale Website"), Yii::app()->user->isGuest ? $this->createUrl("website/sell") : $this->createUrl("sale/add", array("id"=>$website->id))),
				)) ?>
			</p>
		</div>
	</div>
	<?php endif; ?>
</div>

<a id="estimate"></a>
<?php $this->widget('application.widgets.RequestFormWidget', array(
	'hSize'=>3
)) ?>
