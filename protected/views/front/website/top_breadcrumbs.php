<ul class="nav nav-pills nav-justified">
	<li<?php echo stripos($this->action->id, 'top') !== false ? ' class="active"' : null; ?>>
		<a href="<?php echo $this->createUrl("website/toplist") ?>">
			<?php echo Yii::t("website", "TOP by Cost") ?>
		</a>
	</li>
	<li<?php echo stripos($this->action->id, 'pagerank') !== false ? ' class="active"' : null; ?>>
		<a href="<?php echo $this->createUrl("website/page-ranklist") ?>">
			<?php echo Yii::t("website", "TOP by PageRank") ?>
		</a>
	</li>
	<li<?php echo stripos($this->action->id, 'country') !== false ? ' class="active"' : null; ?>>
		<a href="<?php echo $this->createUrl("website/countrylist") ?>">
			<?php echo Yii::t("website", "TOP by Countries") ?>
		</a>
	</li>
</ul>
<br/>