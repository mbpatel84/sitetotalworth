<form class="form-horizontal" method="POST">
	<fieldset>
		<legend><p><?php echo Yii::t("contact", "Contact page description") ?></p></legend>

		<?php echo CHtml::errorSummary($form, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>

		<div class="form-group">
		<?php echo CHtml::activeLabel($form, 'name', array('class' => 'col-lg-2 control-label')); ?>
		<div class="col-lg-10">
		<?php echo CHtml::activeTextField($form, 'name', array('class' => 'form-control')); ?>
		</div>
		</div>


		<div class="form-group">
		<?php echo CHtml::activeLabel($form, 'email', array('class' => 'col-lg-2 control-label')); ?>
		<div class="col-lg-10">
		<?php echo CHtml::activeTextField($form, 'email', array('class' => 'form-control')); ?>
		</div>
		</div>

		<div class="form-group">
		<?php echo CHtml::activeLabel($form, 'subject', array('class' => 'col-lg-2 control-label')); ?>
		<div class="col-lg-10">
		<?php echo CHtml::activeTextField($form, 'subject', array('class' => 'form-control')); ?>
		</div>
		</div>

		<div class="form-group">
		<?php echo CHtml::activeLabel($form, 'body', array('class' => 'col-lg-2 control-label')); ?>
		<div class="col-lg-10">
		<?php echo CHtml::activeTextArea($form, 'body', array('class' => 'form-control', 'rows'=>6)); ?>
		</div>
		</div>

		<?php if(CCaptcha::checkRequirements()): ?>
		<div class="form-group">
		<?php echo CHtml::activeLabel($form, 'verifyCode', array('class'=> 'col-lg-2 control-label')); ?>

		<div class="col-lg-10">
		<?php $this->widget('CCaptcha', array(
			'buttonType' => 'button',
			'buttonOptions' => array(
				'class' => 'btn btn-sm btn-default',
			),
			'imageOptions' => array(
				'class' => '',
				'style' => 'float:left;',
			),
		)); ?>
		<div class="col-xs-4">
		<?php echo CHtml::activeTextField($form, 'verifyCode', array('class' => 'form-control col-lg-2')); ?>
		</div>
		</div>

		</div>
		<?php endif; ?>

		<div class="form-group">
		<div class="col-lg-10 col-lg-offset-2">
		<?php echo CHtml::submitButton(Yii::t("misc", "Submit"), array(
			'class' => 'btn btn-large btn-primary',
		)); ?>
		</div>
		</div>

	</fieldset>
</form>