<div class="row">
    <div class="col-lg-12">
        <form class="form-horizontal" method="POST">
            <fieldset>
                <legend><p><?php echo Yii::t("user", "Register Form") ?></p></legend>
                <br/>
                <p class="col-lg-offset-2">
									<?php echo Yii::t("user", "{Click here} if you already have an account and just need to login.", array(
										"{Click here}"=>CHtml::link(Yii::t("misc", "Click here"), $this->createUrl("user/sign-in")),
									)) ?>
                </p>
                <br/>

                <?php echo CHtml::errorSummary($user, null, null, array(
                    'class' => 'alert alert-danger col-lg-offset-2',
                )); ?>

                <div class="form-group">
                    <?php echo CHtml::activeLabel($user, 'email', array('class' => 'col-lg-2 control-label')); ?>
                    <div class="col-lg-10">
                        <?php echo CHtml::activeTextField($user, 'email', array(
                            'class' => 'form-control login',
                            'required'=>true,
                            'type'=>'email',
                            'placeholder'=>Yii::t("user", "Email"),
                            'autofocus'=>true,
                        )); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo CHtml::activeLabel($user, 'password', array('class' => 'col-lg-2 control-label')); ?>
                    <div class="col-lg-10">
                        <?php echo CHtml::activePasswordField($user, 'password', array(
                            'class' => 'form-control password',
                            'required'=>true,
                            'placeholder'=>Yii::t("user", "Password"),
                        )); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo CHtml::activeLabel($user, 'password2', array('class' => 'col-lg-2 control-label')); ?>
                    <div class="col-lg-10">
                        <?php echo CHtml::activePasswordField($user, 'password2', array(
                            'class' => 'form-control password',
                            'required'=>true,
                            'placeholder'=>Yii::t("user", "Re-Password"),
                        )); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo CHtml::activeLabel($user, 'username', array('class' => 'col-lg-2 control-label')); ?>
                    <div class="col-lg-10">
                        <?php echo CHtml::activeTextField($user, 'username', array(
                            'class' => 'form-control login',
                            'required'=>true,
                            'placeholder'=>Yii::t("user", "Username"),
                        )); ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-lg btn-primary" type="submit"><?php echo Yii::t("user", "Sign up") ?></button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>




