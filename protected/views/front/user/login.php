<div class="row">
	<div class="col-md-6 col-lg-6">
		<form class="form-horizontal" method="POST">
			<fieldset>
				<legend><p><?php echo Yii::t("user", "Login Form") ?></p></legend>

				<?php echo CHtml::errorSummary($login_form, null, null, array(
						'class' => 'alert alert-danger col-lg-offset-2',
				)); ?>

				<div class="form-group">
				<?php echo CHtml::activeLabel($login_form, 'email', array('class' => 'col-lg-2 control-label')); ?>
				<div class="col-lg-10">
				<?php echo CHtml::activeTextField($login_form, 'email', array('class' => 'form-control login')); ?>
				</div>
				</div>

				<div class="form-group">
				<?php echo CHtml::activeLabel($login_form, 'password', array('class' => 'col-lg-2 control-label')); ?>
				<div class="col-lg-10">
				<?php echo CHtml::activePasswordField($login_form, 'password', array('class' => 'form-control password')); ?>
				</div>
				</div>

				<?php if(CCaptcha::checkRequirements() AND $scenario): ?>
				<div class="form-group ">
				<?php echo CHtml::activeLabel($login_form, 'verifyCode', array('class'=> 'col-lg-2 control-label')); ?>

				<div class="col-lg-10">
				<?php $this->widget('CCaptcha', array(
					'buttonType' => 'button',
					'buttonOptions' => array(
						'class' => 'btn btn-sm btn-default',
					),
					'imageOptions' => array(
						'class' => '',
						'style' => 'float:left;',
					),
				)); ?>
				<div class="col-xs-4">
				<?php echo CHtml::activeTextField($login_form, 'verifyCode', array('class' => 'form-control col-lg-2')); ?>
				</div>
				</div>

				</div>
				<?php endif; ?>

				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<div class="checkbox">
								<label for="LoginForm_remember">
									<?php echo CHtml::activeCheckBox($login_form, 'remember'); ?><?php echo Yii::t("user", "Remember me"); ?>
								</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button class="btn btn-sm btn-primary" type="submit"><?php echo Yii::t("user", "Sign in") ?></button>
					</div>
				</div>

			</fieldset>
		</form>
        <div class="col-lg-offset-2">
            <ul class="list-group">
                <li class="list-group-item">
                    <span class="glyphicon glyphicon-lock"></span>
                    &nbsp;
                    <a href="<?php echo $this->createUrl("user/forgot-password") ?>">
                        <?php echo Yii::t("user", "Forgot your password?") ?>
                    </a>
                </li>
                <li class="list-group-item">
                    <span class="glyphicon glyphicon-pencil"></span>
                    &nbsp;
                    <?php echo Yii::t("user", "Don't have an account yet? {Sign up now}!", array(
											"{Sign up now}"=>CHtml::link(Yii::t("user", "Sign up now"), $this->createUrl("user/sign-up")),
                    )) ?>
                </li>
            </ul>
        </div>
	</div>
	<div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
		<h3><?php echo Yii::t("user", "Need an account?") ?></h3>
        <br/>
        <p>
					<?php echo Yii::t("user", "Creating a {InstalledUrl} account is fast, easy, and free", array(
						"{InstalledUrl}"=>"<strong>".Helper::getBrandUrl()."</strong>",
					)) ?>
        </p>
        <br/>
        <a href="<?php echo $this->createUrl("user/sign-up") ?>" class="btn btn-primary btn-lg"><?php echo Yii::t("user", "Create an account") ?></a>
	</div>
</div>
<br/>



