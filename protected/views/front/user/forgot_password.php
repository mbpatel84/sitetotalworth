<form class="form-horizontal" method="POST">
	<fieldset>
		<legend>
			<?php echo Yii::t("user", "Forgot your password?") ?>
		</legend>
			<br/>
        <p>
					<?php echo Yii::t("user", "Forgot password instruction") ?>
        </p>

		<?php echo CHtml::errorSummary($form, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>

		<div class="form-group">
			<?php echo CHtml::activeLabel($form, 'email', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($form, 'email', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<?php echo CHtml::submitButton(Yii::t("misc", "Submit"), array(
					'class' => 'btn btn-large btn-primary',
				)); ?>
				&nbsp; <a href="<?php echo $this->createUrl("user/sign-in") ?>"><?php echo Yii::t("user", "or wait, I remember!") ?></a>
			</div>
		</div>
	<fieldset>
</form>