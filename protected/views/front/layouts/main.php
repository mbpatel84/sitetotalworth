<!DOCTYPE html>
<html lang="<?php echo Yii::app() -> language ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="author" content="php5developer.com">
<meta name="dcterms.rightsHolder" content="php5developer.com">
<link rel="shortcut icon" href="<?php echo Yii::app()->getBaseUrl(true) ?>/favicon.ico" />
<link href="<?php echo Yii::app()->baseUrl ?>/css/yeti.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo Yii::app()->baseUrl ?>/css/app.css" rel="stylesheet">
<link href="<?php echo Yii::app()->baseUrl ?>/css/font-awesome.min.css" rel="stylesheet">

<link rel="apple-touch-icon" href="<?php echo Yii::app()->getBaseUrl(true) ?>/images/touch-114.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo Yii::app()->getBaseUrl(true) ?>/images/touch-72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo Yii::app()->getBaseUrl(true)  ?>/images/touch-114.png">

<?php Yii::app()->clientScript->registerCoreScript('jquery') ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap.min.js') ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/base.js') ?>

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<title><?php echo CHtml::encode($this->title) ?></title>
</head>

<body>
<div id="wrap">

<div class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
								<span class="sr-only"><?php echo Yii::t("misc", "Toggle navigation") ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="<?php echo $this -> createUrl('site/index', array('language' => Yii::app() -> language)) ?>">
                <?php echo Yii::app()->name ?>
            </a>
        </div>
        <div class="navbar-collapse collapse navbar-inverse-collapse">
            <ul class="nav navbar-nav">
                <li<?php echo ($this->id == "site" AND $this->action->id != "contact") ? ' class="active"' : null?>>
                    <a href="<?php echo $this -> createUrl('site/index', array('language' => Yii::app() -> language)) ?>">
                        <?php echo Yii::t("misc", "Home") ?>
                    </a>
                </li>
                <li<?php echo ($this->id == "website" AND in_array(strtolower($this->action->id), array("toplist", "countrylist", "country", "pageranklist", "pagerank"))) ? ' class="active"' : null?>>
                    <a href="<?php echo $this->createUrl("website/toplist") ?>">
                        <?php echo Yii::t("misc", "Top") ?>
                    </a>
                </li>
                <li<?php echo ($this->id == "website" AND $this->action->id == "upcominglist") ? ' class="active"' : null?>>
                    <a href="<?php echo $this->createUrl("website/upcominglist") ?>">
                        <?php echo Yii::t("misc", "Upcoming") ?>
                    </a>
                </li>
                <!-- <li<?php echo ($this->id == "website" AND $this->action->id == "sell") ? ' class="active"' : null?>>
									<a href="<?php echo $this->createUrl("website/sell") ?>">
										<?php echo Yii::t("misc", "Sell Websites") ?>
									</a>
								</li>
                <li<?php echo ($this->id == "category") ? ' class="active"' : null?>>
                    <a href="<?php echo $this->createUrl("category/index") ?>">
                        <?php echo Yii::t("misc", "Buy Websites") ?>
                    </a> -->
                </li>
                <li<?php echo ($this->id == "site" AND $this->action->id == "contact") ? ' class="active"' : null?>>
                    <a href="<?php echo $this->createUrl("site/contact") ?>">
                        <?php echo Yii::t("contact", "Contact us") ?>
                    </a>
                </li>
                <?php $this -> widget('application.widgets.LanguageSelector'); ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-rss fa-lg"></i><b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo $this->createUrl("feed/rss") ?>"
                               target="_blank" type="application/rss+xml"
                               rel="alternate"
                               title="<?php echo Yii::t("misc", "{BrandName} | RSS feed", array("{BrandName}"=>Yii::app()->name)) ?>">
                                  <?php echo Yii::t("misc", "RSS") ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->createUrl("feed/atom") ?>"
                               target="_blank"
                               type="application/atom+xml"
                               rel="alternate"
                               title="<?php echo Yii::t("misc", "{BrandName} | Atom feed", array("{BrandName}"=>Yii::app()->name)) ?>">
                                <?php echo Yii::t("misc", "Atom") ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if(Yii::app()->user->isGuest): ?>
                    <li><a href="<?php echo $this->createUrl("user/sign-in") ?>"><?php echo Yii::t("misc", "Sign in") ?></a></li>
                <?php else: ?>
                    <?php if($this->newMessages > 0): ?>
                        <li class="dropdown<?php echo ($this->id == "post") ? ' active' : null?>" id="header-message">
                            <a href="<?php echo $this->createUrl('post/index') ?>" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-inbox"></i>
                                <span class="badge"><?php echo $this->newMessages ?></span>
                                <span class="hidden-lg">&nbsp;<?php echo Yii::t("misc", "Inbox") ?></span>
                            </a>
                            <ul class="dropdown-menu inbox">
                                <li class="dropdown-title">
                                    <span><i class="fa fa-envelope-o"></i> <?php echo Yii::t("misc", "{NumberOfMsg} Messages", array(
																			"{NumberOfMsg}"=>$this->newMessages
                                    )) ?></span>
                                </li>
                                <?php foreach($this->upcomingHeaders as $header): ?>
                                <li>
                                    <a href="<?php echo $this->createUrl("post/chain", array("id"=>$header['id'])) ?>">
                                        <span class="body">
                                            <span class="from">
                                                <?php echo CHtml::encode($this->senders[$header['companion_id']]->username) ?>
                                            </span>
                                            <span class="message">
                                            <?php echo CHtml::encode(Helper::mb_ucfirst(Helper::cropText($header['subject'], 150))) ?>
                                            </span>
                                            <span class="time">
                                                <i class="fa fa-clock-o"></i>
                                                <span><?php echo Helper::time_elapsed_string($header['sent_date'])?></span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <?php endforeach; ?>
                                <li class="footer">
                                    <a href="<?php echo $this->createUrl('post/index') ?>"><?php echo Yii::t("misc", "See all messages") ?> <i class="fa fa-arrow-circle-right"></i></a>
                                </li>
                            </ul>
                        </li>
                    <?php else: ?>
                        <li class="dropdown<?php echo ($this->id == "post") ? ' active' : null?>" id="header-message">
                        <a href="<?php echo $this->createUrl('post/index') ?>">
                            <i class="fa fa-inbox"></i>
                            <span class="hidden-lg">&nbsp;<?php echo Yii::t("misc", "Inbox") ?></span>
                        </a>
                        </li>
                    <?php endif; ?>

                    <li <?php echo ($this->id == "sale") ? ' class="active"' : null?>>
                        <a href="<?php echo $this->createUrl('sale/index') ?>"><i class="fa fa-usd"></i>
                        <span class="hidden-lg">&nbsp;<?php echo Yii::t("misc", "On sale") ?></span>
                        </a>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?php echo Yii::t("misc", "Hello, {Username}", array(
															"{Username}"=>"<strong>". CHtml::encode(Yii::app()->user->name) ."</strong>",
                            )) ?> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="<?php echo $this->createUrl('post/index') ?>"><i class="fa fa-inbox fa-fw"></i> <?php echo Yii::t("misc", "Inbox") ?></a>
                            </li>
                            <li><a href="<?php echo $this->createUrl('sale/index') ?>"><i class="fa fa-usd fa-fw"></i> <?php echo Yii::t("misc", "On sale") ?></a>
                            </li>
                            <li><a href="<?php echo $this->createUrl('profile/settings') ?>"><i class="fa fa-gear fa-fw"></i> <?php echo Yii::t("misc", "Settings") ?></a>
                            </li>
                            <li><a href="<?php echo $this->createUrl('user/logout') ?>"><i class="fa fa-sign-out fa-fw"></i> <?php echo Yii::t("misc", "Logout") ?></a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <br/>
    <br/>
	<br/>
    <?php if(!empty(Yii::app()->params['bannerTop'])): ?>
	<div class="row" style="margin-bottom:21px">
			<div class="col-xs-12 col-sm-12">
				<?php echo Yii::app()->params['bannerTop'] ?>
			</div>
	</div>
	<?php endif; ?>

	<?php if($flashes=Yii::app()->user->getFlashes()): ?>
	<div class="row" style="margin-bottom:21px">
		<div class="col-xs-12 col-sm-12">
			<?php echo $this -> renderPartial("//". $this->_end. "/site/flash", array("flashes"=>$flashes)) ?>
		</div>
	</div>
	<?php endif; ?>

	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<?php echo $content ?>
		</div>
	</div>

	<?php if(!empty(Yii::app()->params['bannerBottom'])): ?>
	<div class="row" style="margin-bottom:21px">
		<div class="col-xs-12 col-sm-12">
			<?php echo Yii::app()->params['bannerBottom'] ?>
		</div>
	</div>
	<?php endif; ?>

</div>

</div><!-- End of wrap -->

<div id="footer">
	<div class="container">
			<!-- <p class="text-muted">
				<?php echo Yii::t("misc", "Developed by {Developer}", array(
					"{Developer}"=>'<strong><a href="http://php5developer.com" target="_blank">PHP5 Developer</a></strong>',
				)) ?>
			</p> -->
	</div>
</div>

</body>
</html>