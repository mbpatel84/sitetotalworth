<form method="post" class="form-horizontal">
	<fieldset>
		<legend><?php echo CHtml::encode($title) ?></legend>
		<br/>

		<?php echo CHtml::errorSummary($onSale, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>

		<div class="form-group">
			<?php echo CHtml::activeLabel($onSale, 'category_id', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeDropDownList($onSale, 'category_id', array(""=>Yii::t("misc", "Please choose category"), "-"=>'--------------') + $catList, array(
					'class' => 'form-control',
					'options' => array(
						'-' => array(
							'disabled' => 'disabled',
						),
						'' => array(
							'readonly' => 'readonly',
						),
				))); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($onSale, 'price', array('class' => 'col-lg-2 control-label')); ?>
			<div class="input-group col-lg-10">
				<span class="input-group-addon"><?php echo Yii::app()->params['currency'] ?></span>
				<?php echo CHtml::activeTextField($onSale, 'price', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>


		<div class="form-group">
			<?php echo CHtml::activeLabel($onSale, 'monthly_visitors', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($onSale, 'monthly_visitors', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>


		<div class="form-group">
			<?php echo CHtml::activeLabel($onSale, 'monthly_revenue', array('class' => 'col-lg-2 control-label')); ?>
			<div class="input-group col-lg-10">
				<span class="input-group-addon"><?php echo Yii::app()->params['currency'] ?></span>
				<?php echo CHtml::activeTextField($onSale, 'monthly_revenue', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($onSale, 'monthly_views', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($onSale, 'monthly_views', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($onSale, 'description', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextArea($onSale, 'description', array(
					'class' => 'form-control',
                    'rows' => '5',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-offset-2 col-sm-10">
				<button type="submit" class="btn btn-lg btn-primary"><?php echo $onSale->isNewRecord ? Yii::t("sale", "Put up for sale") : Yii::t("misc", "Update"); ?></button>
			</div>
		</div>

	</fieldset>
</form>
<br/><br/>
