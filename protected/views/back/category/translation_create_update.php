<form class="form-signin" method="post">
	<fieldset>
		<?php echo CHtml::errorSummary($trans, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>

		<div class="form-group">
			<div class="col-sm-10 col-lg-offset-2">
				<a href="<?php echo $this->createUrl("admin/category/managetranslations", array("id"=>$trans->category->id)) ?>" class="btn btn-success">
					<i class="fa fa-language fa-fw"></i> <?php echo Yii::t("language", "Manage Translations") ?>
				</a>
			</div>
		</div>
		<br/>
		<div class="clearfix"></div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($trans, 'translation', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($trans, 'translation', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($trans, 'lang_id', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeDropDownList($trans, 'lang_id', CHtml::listData(Language::model()->getList(false), 'id', array('Language', 'formatLanguage')), array(
					'class' => 'form-control',
					'prompt' => Yii::t("language", "Choose any language"),
				)); ?>
			</div>
		</div>

 		<div class="form-group">
			<div class="col-lg-offset-2 col-sm-10">
				<button class="btn btn-lg btn-primary" type="submit">
					<?php echo $trans->isNewRecord ?  Yii::t("misc", "Create") : Yii::t("misc", "Update"); ?>
				</button>
			</div>
		</div>
	</fieldset>
</form>