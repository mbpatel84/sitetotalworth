<form method="post" class="form-horizontal">
	<fieldset>
		<?php echo CHtml::errorSummary($category, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>
		<div class="form-group">
			<div class="col-sm-10 col-lg-offset-2">
				<div class="btn-group">
							<a href="<?php echo $this->createUrl("admin/category/index", array("id"=>$category->id)) ?>" class="btn btn-primary">
								<i class="glyphicon glyphicon-arrow-left"></i> <?php echo Yii::t("category", "Manage Categories") ?>
							</a>
					<?php if(!$category->isNewRecord): ?>
							<a href="<?php echo $this->createUrl("admin/category/managetranslations", array("id"=>$category->id)) ?>" class="btn btn-success">
								<i class="fa fa-language fa-fw"></i> <?php echo Yii::t("language", "Manage Translations") ?>
							</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<br/>

		<div class="form-group">
			<?php echo CHtml::activeLabel($category, 'name', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($category, 'name', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($category, 'slug', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($category, 'slug', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>
 		<div class="form-group">
			<div class="col-lg-offset-2 col-sm-10">
				<button class="btn btn-lg btn-primary" type="submit">
					<?php echo $category->isNewRecord ?  Yii::t("misc", "Create") : Yii::t("misc", "Update"); ?>
				</button>
			</div>
		</div>
	</fieldset>
</form>