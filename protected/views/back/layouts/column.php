<?php $this->beginContent('/'.$this->_end.'/layouts/main'); ?>
<div id="wrapper">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
								<span class="sr-only"><?php echo Yii::t("misc", "Toggle navigation") ?></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo $this->createUrl("admin/site/index") ?>">
							<?php echo Yii::app()->name ?> <?php echo Yii::t("admin", "Admin panel") ?>
						</a>
				</div>
				<!-- /.navbar-header -->
				<ul class="nav navbar-top-links navbar-right">
						<?php $this -> widget('application.widgets.LanguageSelector'); ?>
						<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
										<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
								</a>
								<ul class="dropdown-menu dropdown-user">
										<?php if($this->user->isSuperUser()) : ?>
										<li>
											<a href="<?php echo $this->createUrl("admin/user/update", array("id"=>$this->user->id)) ?>">
												<i class="fa fa-user fa-fw"></i>&nbsp;<?php echo Yii::t("user", "Profile Settings") ?>
											</a>
										</li>
										<li>
											<a href="<?php echo $this->createUrl("admin/user/reset-password", array("id"=>$this->user->id)) ?>">
												<i class="fa fa-refresh fa-fw"></i>&nbsp;<?php echo Yii::t("user", "Reset password") ?>
											</a>
										</li>
										<li class="divider"></li>
										<?php endif; ?>
										<li><a href="<?php echo $this->createUrl("admin/user/logout") ?>"><i class="fa fa-sign-out fa-fw"></i> <?php echo Yii::t("misc", "Logout") ?></a>
										</li>
								</ul>
								<!-- /.dropdown-user -->
						</li>
						<!-- /.dropdown -->
				</ul>
				<!-- /.navbar-top-links -->

				<div class="navbar-default navbar-static-side" role="navigation">
						<div class="sidebar-collapse">
								<ul class="nav" id="side-menu">
										<li>
												<a href="<?php echo $this->createUrl("admin/site/index") ?>"><i class="fa fa-dashboard fa-fw"></i> <?php echo Yii::t("admin", "Dashboard") ?></a>
										</li>
										<li>
												<a href="<?php echo $this->createUrl("admin/user/index") ?>"><i class="fa fa-users fa-fw"></i> <?php echo Yii::t("user", "Manage Users") ?></a>
										</li>
										<li>
												<a href="<?php echo $this->createUrl("admin/website/index") ?>"><i class="fa fa-link fa-fw"></i> <?php echo Yii::t("website", "Manage Websites") ?></a>
										</li>
										<li>
												<a href="<?php echo $this->createUrl("admin/category/index") ?>"><i class="fa fa-sitemap fa-fw"></i> <?php echo Yii::t("category", "Manage Categories") ?></a>
										</li>
										<li>
												<a href="#"><i class="fa fa-language fa-fw"></i> <?php echo Yii::t("admin", "Localization") ?><span class="fa arrow"></span></a>
												<ul class="nav nav-second-level">
														<li>
																<a href="<?php echo $this->createUrl("admin/language/index") ?>"><?php echo Yii::t("language", "Manage existing languages") ?></a>
														</li>
														<li>
																<a href="<?php echo $this->createUrl("admin/language/category") ?>"><?php echo Yii::t("language", "Manage Translations") ?></a>
														</li>
												</ul>
										</li>
										<li>
												<a href="#"><i class="fa fa-gears fa-fw"></i> <?php echo Yii::t("admin", "Tools") ?><span class="fa arrow"></span></a>
												<ul class="nav nav-second-level">
														<li>
																<a href="<?php echo $this->createUrl("admin/tools/sitemap") ?>"><?php echo Yii::t("admin", "Sitemap generation") ?></a>
														</li>
														<li>
																<a href="<?php echo $this->createUrl("admin/tools/clearcache") ?>"><?php echo Yii::t("admin", "Flush cache") ?></a>
														</li>
														<li>
																<a href="<?php echo $this->createUrl("admin/tools/checkonsale") ?>"><?php echo Yii::t("admin", "Run widget checker") ?></a>
														</li>
														<li>
																<a href="<?php echo $this->createUrl("admin/tools/garbagecollector") ?>"><?php echo Yii::t("admin", "Run garbage collector") ?></a>
														</li>
												</ul>
										</li>
								</ul>
								<!-- /#side-menu -->
						</div>
						<!-- /.sidebar-collapse -->
				</div>
				<!-- /.navbar-static-side -->
		</nav>

		<div id="page-wrapper">
				<div class="row">
						<div class="col-xs-12 col-sm-12">
								<?php echo $this -> renderPartial("//". $this->_end. "/site/flash", array(
									"messages"=>Yii::app() -> user -> getFlashes(),
								)) ?>
						</div>
				</div>
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"><?php echo CHtml::encode($this->title) ?></h1>
						</div>
						<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
						<?php echo $content ?>
                     </div>
				</div>
				<!-- /.row -->
		</div> <!-- #page-wrapper -->
</div> <!-- /#wrapper -->

<?php $this->endContent('/'.$this->_end.'/layouts/main'); ?>