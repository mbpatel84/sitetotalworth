<div class="btn-group">
	<a href="<?php echo $this->createUrl("admin/language/category") ?>" class="btn btn-primary">
		<i class="glyphicon glyphicon-arrow-left"></i> <?php echo Yii::t("language", "Manage Translations") ?>
	</a>
</div>
<br/><br/>

<form class="form-horizontal" method="post">
	<fieldset>
		<?php echo CHtml::errorSummary($model, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'newName', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($model, 'newName', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-primary">
					<?php echo Yii::t("misc", "Update"); ?>
				</button>
			</div>
		</div>
	</fieldset>
</form>