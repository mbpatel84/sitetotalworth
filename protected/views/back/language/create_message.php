<div class="btn-group">
    <?php if($existCat): ?>
    <a href="<?php echo $this->createUrl("admin/language/messages", array("id"=>$model->category)) ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-arrow-left"></i> <?php echo Yii::t("language", "Manage translations in {Category} category", array(
					"{Category}"=>Helper::mb_ucfirst($model->category),
        )) ?>
    </a>
    <?php else: ?>
    <a href="<?php echo $this->createUrl("admin/language/category") ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-arrow-left"></i> <?php echo Yii::t("language", "Manage Translations") ?>
    </a>
    <?php endif; ?>
</div>
<br/><br/>

<form class="form-horizontal" method="post">
	<fieldset>
		<?php echo CHtml::errorSummary($model, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'message', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($model, 'message', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'category', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeDropDownList($model, 'category', $categories, array(
					'class' => 'form-control',
					'options' => array(
						'-' => array(
							'disabled' => 'disabled',
						),
						'' => array(
							'readonly' => 'readonly',
						),
				))); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<?php echo Yii::t("misc", "OR") ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'new_category', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($model, 'new_category', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-primary">
					<?php echo Yii::t("misc", "Create"); ?>
				</button>
			</div>
		</div>
	</fieldset>
</form>