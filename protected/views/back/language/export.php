<div class="btn-group">
	<a href="<?php echo $this->createUrl("admin/language/index") ?>" class="btn btn-primary">
		<i class="fa fa-list"></i> <?php echo Yii::t('language', 'Manage existing languages') ?>
	</a>
	<a href="<?php echo $this->createUrl("admin/language/category") ?>" class="btn btn-success">
		<i class="fa fa-language"></i> <?php echo Yii::t("language", "Manage Translations") ?>
	</a>
	<a href="<?php echo $this->createUrl("admin/language/import") ?>" class="btn btn-warning">
		<i class="fa fa-upload"></i> <?php echo Yii::t("language", "Import Translation") ?>
	</a>
</div>
<br/><br/>

<form class="form-horizontal" method="post">
	<fieldset>
		<legend><?php echo Yii::t("language", "Please choose language you want to export") ?></legend>

		<?php echo CHtml::errorSummary($model, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'language', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeDropDownList($model, 'language', $languages, array(
					'class' => 'form-control',
					'options' => array(
						'-' => array(
							'disabled' => 'disabled',
						),
						'' => array(
							'readonly' => 'readonly',
						),
					),
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'trans_language', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeDropDownList($model, 'trans_language', $languages, array(
					'class' => 'form-control',
					'options' => array(
						'-' => array(
							'disabled' => 'disabled',
						),
						'' => array(
							'readonly' => 'readonly',
						),
					),
				)); ?>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-primary btn-lg">
					<?php echo Yii::t("language", "Export"); ?>
				</button>
			</div>
		</div>
	</fieldset>
</form>