<div class="btn-group">
	<a href="<?php echo $this->createUrl("admin/language/index") ?>" class="btn btn-primary">
		<i class="glyphicon glyphicon-arrow-left"></i> <?php echo Yii::t('language', 'Manage existing languages') ?>
	</a>
	<a href="<?php echo $this->createUrl("admin/language/category") ?>" class="btn btn-success">
		<i class="fa fa-language fw"></i> <?php echo Yii::t("language", "Manage Translations") ?>
	</a>
</div>
<br/><br/>

<form class="form-horizontal" method="post">
	<fieldset>
		<?php echo CHtml::errorSummary($model, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'language', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($model, 'language', array(
					'class' => 'form-control',
					'required' => true,
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'enabled', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeDropDownList($model, 'enabled', array(
					Language::STATUS_ENABLED => Yii::t("admin", "Yes"),
					Language::STATUS_DISABLED => Yii::t("admin", "No"),
				), array(
					'class' => 'form-control',
				)) ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'is_default', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeCheckBox($model, 'is_default', array(
					'uncheckValue' => Language::NOTDEFAULT_LANG,
					'value' => Language::DEFAULT_LANG,
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-primary"><?php echo Yii::t("misc", "Submit") ?></button>
			</div>
		</div>
	</fieldset>
</form>