<div class="btn-group">
    <a href="<?php echo $this->createUrl("admin/language/messages", array("id"=>$source['category'])) ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-arrow-left"></i> <?php echo Yii::t("language", "Manage translations in {Category} category", array(
					"{Category}"=>Helper::mb_ucfirst($source['category']),
        )) ?>
    </a>
    <a href="<?php echo $this->createUrl("admin/language/create-message", array("cat_id"=>$source['category'])) ?>" class="btn btn-success">
        <i class="fa fa-plus"></i> <?php echo Yii::t("language", "Create phrase") ?>
    </a>
    <a href="<?php echo $this->createUrl("admin/language/update-message", array("id"=>$source['id'])) ?>" class="btn btn-default">
        <i class="fa fa-pencil"></i> <?php echo Yii::t("language", "Edit phrase") ?>
    </a>
</div>
<br/><br/>

<form class="form-horizontal" method="post">
	<fieldset>
		<legend>
			<?php echo Yii::t("language", "Category name") ?> : <i><?php echo $source['category'] ?></i>
			<br/>
			<?php echo Yii::t("language", "Phrase") ?> : <i><?php echo $source['message'] ?></i>
		</legend>

		<?php echo CHtml::errorSummary($model, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'translation', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextArea($model, 'translation', array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($model, 'language', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeDropDownList($model, 'language', $languages, array(
					'class' => 'form-control',
					'options' => array(
						'-' => array(
							'disabled' => 'disabled',
						),
						'' => array(
							'readonly' => 'readonly',
						),
					),
				)); ?>
			</div>
		</div>

		<?php echo CHtml::activeHiddenField($model, 'id') ?>

		<div class="form-group">
			<div class="col-lg-10 col-lg-offset-2">
				<button type="submit" class="btn btn-primary">
					<?php echo Yii::t("language", "Translate phrase"); ?>
				</button>
			</div>
		</div>
	</fieldset>
</form>

<h3><?php echo Yii::t("language", "Existing translations") ?></h3>
<div class="table-responsive">
<table class="table table-hover">
	<thead>
		<tr>
			<th><?php echo Yii::t("language", "Language") ?></th>
			<th><?php echo Yii::t("language", "Translation") ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($langObj as $lang):?>
		<tr>
		<?php if(isset($translations[$lang->id])): ?>
			<td width="20%"><?php echo Language::formatLanguage($lang) ?></td>
			<td><?php echo $translations[$lang->id]['translation'] ?></td>
		<?php else: ?>
			<td colspan="2" class="warning">
				<?php echo Yii::t("language", "Missing translation for {Language}", array(
					"{Language}"=>"<strong>".Language::formatLanguage($lang)."</strong>",
				)) ?>
			</td>
		<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
</div>