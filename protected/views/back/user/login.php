<div class="row">
		<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
						<div class="panel-heading">
								<h3 class="panel-title"><?php echo Yii::t("user", "Please Sign In") ?></h3>
						</div>
						<div class="panel-body">
								<form role="form" method="POST">
										<fieldset>
												<?php echo CHtml::errorSummary($login_form, null, null, array(
													'class' => 'alert alert-danger',
												)); ?>
												<div class="form-group">
														<?php echo CHtml::activeTextField($login_form, 'email', array(
															'class'=> 'form-control',
															'placeholder'=>Yii::t("user", "Email"),
															'type'=>"email",
															'autofous'=>'autofocus',
														));?>
												</div>
												<div class="form-group">
														<?php echo CHtml::activePasswordField($login_form, 'password', array(
																'class' => 'form-control',
																'placeholder' => Yii::t("user", "Password"),
																'required' => true,
															));?>
												</div>

												<?php if(CCaptcha::checkRequirements() AND $scenario): ?>
												<div class="form-group">
													<?php $this->widget('CCaptcha', array(
														'buttonType' => 'button',
														'buttonOptions' => array(
																'class' => 'btn btn-success refresh-button',
														),
														'imageOptions' => array(
																'class'=>'captcha_img'
														),
													)); ?>
													<?php echo CHtml::activeTextField($login_form, 'verifyCode', array(
														'class' => 'form-control captcha',
													)); ?>
												</div>
												<?php endif; ?>

												<div class="checkbox">
														<label for="LoginForm_remember">
															<?php echo CHtml::activeCheckBox($login_form, 'remember'); ?><?php echo Yii::t("user", "Remember me"); ?>
														</label>
												</div>

												<button class="btn btn-lg btn-success btn-block" type="submit"><?php echo Yii::t("user", "Sign in") ?></button>
										</fieldset>
								</form>
						</div>
				</div>
		</div>
</div>