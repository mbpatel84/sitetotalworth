<form class="form-horizontal" method="post">
	<fieldset>
		<?php echo CHtml::errorSummary($user, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>

		<div class="form-group">
			<?php echo CHtml::activeLabel($user, 'email', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($user, 'email', array(
					'class' => 'form-control',
					'required' => true,
				)); ?>
			</div>
		</div>

		<?php if($user->isNewRecord): ?>
		<div class="form-group">
			<?php echo CHtml::activeLabel($user, 'password', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activePasswordField($user, 'password', array(
					'class' => 'form-control',
					'required' => true,
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($user, 'password2', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activePasswordField($user, 'password2', array(
					'class' => 'form-control',
					'required' => true,
				)); ?>
			</div>
		</div>
		<?php endif; ?>

		<div class="form-group">
			<?php echo CHtml::activeLabel($user, 'username', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeTextField($user, 'username', array(
					'class' => 'form-control',
					'required' => true,
				)); ?>
			</div>
		</div>

		<?php if(!$user->isSuperUser()): ?>
		<div class="form-group">
			<?php echo CHtml::activeLabel($user, 'lang_id', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeDropDownList($user, 'lang_id', $languages, array(
					'class' => 'form-control',
					'options' => array(
						'-' => array(
							'disabled' => 'disabled',
						),
						'' => array(
							'readonly' => 'readonly',
						),
					),
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($user, 'role', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeDropDownList($user, 'role', $roleList, array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($user, 'status', array('class' => 'col-lg-2 control-label')); ?>
			<div class="col-lg-10">
				<?php echo CHtml::activeDropDownList($user, 'status', User::getStatusList(), array(
					'class' => 'form-control',
				)); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-offset-2 col-sm-10">
				<div class="checkbox">
					<label>
						<?php echo CHtml::activeCheckBox($user, 'email_confirmed', array(
							'uncheckValue' => User::EMAIL_NOTCONFIRMED,
							'value' => User::EMAIL_CONFIRMED,
						)); ?>
						<?php echo Yii::t("user", "Confirmed Email") ?>
					</label>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-offset-2 col-sm-10">
				<div class="checkbox">
					<label>
						<?php echo CHtml::activeCheckBox($user, 'can_send_message', array(
							'uncheckValue' => User::DISALLOW_MESSAGE,
							'value' => User::ALLOW_MESSAGE
						)); ?>
						<?php echo Yii::t("user", "Allow send messages") ?>
					</label>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<div class="form-group">
			<div class="col-lg-offset-2 col-sm-10">
				<button class="btn btn-lg btn-primary" type="submit">
					<?php echo $user->isNewRecord ?  Yii::t("misc", "Create") : Yii::t("misc", "Update"); ?>
				</button>
			</div>
		</div>

	</fieldset>
</form>