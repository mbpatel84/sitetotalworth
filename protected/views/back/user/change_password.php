<form class="form-horizontal" method="POST">
	<fieldset>
		<?php echo CHtml::errorSummary($form, null, null, array(
			'class' => 'alert alert-danger',
		)); ?>
		<?php if($scenario): ?>
		<div class="form-group">
			<?php echo CHtml::activeLabel($form, 'oldpassword', array('class' => 'col-lg-3 control-label')); ?>
			<div class="col-lg-7">
				<?php echo CHtml::activePasswordField($form, 'oldpassword', array('class' => 'form-control')); ?>
			</div>
		</div>
		<?php endif; ?>

		<div class="form-group">
			<?php echo CHtml::activeLabel($form, 'password', array('class' => 'col-lg-3 control-label')); ?>
			<div class="col-lg-7">
				<?php echo CHtml::activePasswordField($form, 'password', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo CHtml::activeLabel($form, 'password2', array('class' => 'col-lg-3 control-label')); ?>
			<div class="col-lg-7">
				<?php echo CHtml::activePasswordField($form, 'password2', array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-7 col-lg-offset-3">
				<?php echo CHtml::submitButton(Yii::t("misc", "Submit"), array(
					'class' => 'btn btn-large btn-primary',
				)); ?>
			</div>
		</div>
	<fieldset>
</form>