-- MySQL dump 10.13  Distrib 5.5.24, for Win64 (x86)
--
-- Host: localhost    Database: sitecost_fresh
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sc_bind_website`
--

DROP TABLE IF EXISTS `sc_bind_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_bind_website` (
  `chain_id` varchar(32) NOT NULL,
  `website_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`chain_id`),
  KEY `ix_website` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_bind_website`
--

LOCK TABLES `sc_bind_website` WRITE;
/*!40000 ALTER TABLE `sc_bind_website` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_bind_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_block_sender`
--

DROP TABLE IF EXISTS `sc_block_sender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_block_sender` (
  `user_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `block_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`block_id`) USING BTREE,
  KEY `ix_user_date` (`user_id`,`block_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_block_sender`
--

LOCK TABLES `sc_block_sender` WRITE;
/*!40000 ALTER TABLE `sc_block_sender` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_block_sender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_category`
--

DROP TABLE IF EXISTS `sc_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_category` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_category`
--

LOCK TABLES `sc_category` WRITE;
/*!40000 ALTER TABLE `sc_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_category_translation`
--

DROP TABLE IF EXISTS `sc_category_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_category_translation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `lang_id` varchar(5) NOT NULL,
  `translation` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_category_lang` (`category_id`,`lang_id`),
  KEY `ix_lang` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_category_translation`
--

LOCK TABLES `sc_category_translation` WRITE;
/*!40000 ALTER TABLE `sc_category_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_category_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_language_list`
--

DROP TABLE IF EXISTS `sc_language_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_language_list` (
  `id` varchar(5) NOT NULL DEFAULT '',
  `language` varchar(50) DEFAULT NULL,
  `enabled` tinyint(3) unsigned NOT NULL,
  `is_default` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `ix_enabled` (`enabled`),
  KEY `ix_order_enabled` (`enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_language_list`
--

LOCK TABLES `sc_language_list` WRITE;
/*!40000 ALTER TABLE `sc_language_list` DISABLE KEYS */;
INSERT INTO `sc_language_list` VALUES ('de','Deutsch',1,0,'2014-09-20 13:01:58','2014-09-20 13:01:58'),('en','English',1,1,'2014-09-20 13:01:58','2014-09-20 13:01:58'),('es','Español',1,0,'2014-11-07 08:35:32','2014-11-07 08:35:32'),('nl','Nederlands',1,0,'2014-11-07 08:35:20','2014-11-07 08:35:20'),('ru','Русский',1,0,'2014-09-20 13:01:58','2014-09-20 13:01:58');
/*!40000 ALTER TABLE `sc_language_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_onsale_checker`
--

DROP TABLE IF EXISTS `sc_onsale_checker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_onsale_checker` (
  `website_id` int(10) unsigned NOT NULL,
  `attempts` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_onsale_checker`
--

LOCK TABLES `sc_onsale_checker` WRITE;
/*!40000 ALTER TABLE `sc_onsale_checker` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_onsale_checker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_post_folder_0`
--

DROP TABLE IF EXISTS `sc_post_folder_0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_post_folder_0` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `header_id` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `appeared_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_owner_header_type` (`owner_id`,`header_id`,`type`),
  KEY `ix_owner_type_state_appeared` (`owner_id`,`type`,`state`,`appeared_date`),
  KEY `ix_type_state_appeared` (`type`,`state`,`appeared_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_post_folder_0`
--

LOCK TABLES `sc_post_folder_0` WRITE;
/*!40000 ALTER TABLE `sc_post_folder_0` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_post_folder_0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_post_folder_1`
--

DROP TABLE IF EXISTS `sc_post_folder_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_post_folder_1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `header_id` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `state` tinyint(3) unsigned NOT NULL,
  `appeared_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_owner_header_type` (`owner_id`,`header_id`,`type`),
  KEY `ix_owner_type_state_appeared` (`owner_id`,`type`,`state`,`appeared_date`) USING BTREE,
  KEY `ix_type_state_appeared` (`type`,`state`,`appeared_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_post_folder_1`
--

LOCK TABLES `sc_post_folder_1` WRITE;
/*!40000 ALTER TABLE `sc_post_folder_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_post_folder_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_post_folder_2`
--

DROP TABLE IF EXISTS `sc_post_folder_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_post_folder_2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `header_id` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `state` tinyint(3) unsigned NOT NULL,
  `appeared_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_owner_header_type` (`owner_id`,`header_id`,`type`),
  KEY `ix_owner_type_state_appeared` (`owner_id`,`type`,`state`,`appeared_date`),
  KEY `ix_type_state_appeared` (`type`,`state`,`appeared_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_post_folder_2`
--

LOCK TABLES `sc_post_folder_2` WRITE;
/*!40000 ALTER TABLE `sc_post_folder_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_post_folder_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_post_header_0`
--

DROP TABLE IF EXISTS `sc_post_header_0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_post_header_0` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `companion_id` int(10) unsigned NOT NULL,
  `chain_id` varchar(32) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ix_owner_sender` (`owner_id`,`companion_id`),
  KEY `ix_owner_status_sent` (`owner_id`,`status`,`sent_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_post_header_0`
--

LOCK TABLES `sc_post_header_0` WRITE;
/*!40000 ALTER TABLE `sc_post_header_0` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_post_header_0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_post_header_1`
--

DROP TABLE IF EXISTS `sc_post_header_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_post_header_1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `companion_id` int(10) unsigned NOT NULL,
  `chain_id` varchar(32) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ix_owner_sender` (`owner_id`,`companion_id`),
  KEY `ix_owner_status_sent` (`owner_id`,`status`,`sent_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_post_header_1`
--

LOCK TABLES `sc_post_header_1` WRITE;
/*!40000 ALTER TABLE `sc_post_header_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_post_header_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_post_header_2`
--

DROP TABLE IF EXISTS `sc_post_header_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_post_header_2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `companion_id` int(10) unsigned NOT NULL,
  `chain_id` varchar(32) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ix_owner_sender` (`owner_id`,`companion_id`),
  KEY `ix_owner_status_sent` (`owner_id`,`status`,`sent_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_post_header_2`
--

LOCK TABLES `sc_post_header_2` WRITE;
/*!40000 ALTER TABLE `sc_post_header_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_post_header_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_post_message_0`
--

DROP TABLE IF EXISTS `sc_post_message_0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_post_message_0` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `header_id` int(10) unsigned NOT NULL,
  `from` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ix_owner_header_sent` (`owner_id`,`header_id`,`sent_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_post_message_0`
--

LOCK TABLES `sc_post_message_0` WRITE;
/*!40000 ALTER TABLE `sc_post_message_0` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_post_message_0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_post_message_1`
--

DROP TABLE IF EXISTS `sc_post_message_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_post_message_1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `header_id` int(10) unsigned NOT NULL,
  `from` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ix_owner_header_sent` (`owner_id`,`header_id`,`sent_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_post_message_1`
--

LOCK TABLES `sc_post_message_1` WRITE;
/*!40000 ALTER TABLE `sc_post_message_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_post_message_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_post_message_2`
--

DROP TABLE IF EXISTS `sc_post_message_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_post_message_2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `header_id` int(10) unsigned NOT NULL,
  `from` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ix_owner_header_sent` (`owner_id`,`header_id`,`sent_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_post_message_2`
--

LOCK TABLES `sc_post_message_2` WRITE;
/*!40000 ALTER TABLE `sc_post_message_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_post_message_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_scam_report`
--

DROP TABLE IF EXISTS `sc_scam_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_scam_report` (
  `sender_id` int(10) unsigned NOT NULL,
  `scammer_id` int(10) unsigned NOT NULL,
  `chain_id` varchar(32) NOT NULL,
  `complain_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`sender_id`,`scammer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_scam_report`
--

LOCK TABLES `sc_scam_report` WRITE;
/*!40000 ALTER TABLE `sc_scam_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_scam_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_trans_message`
--

DROP TABLE IF EXISTS `sc_trans_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_trans_message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(5) CHARACTER SET latin1 NOT NULL,
  `translation` text,
  PRIMARY KEY (`id`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_trans_message`
--

LOCK TABLES `sc_trans_message` WRITE;
/*!40000 ALTER TABLE `sc_trans_message` DISABLE KEYS */;
INSERT INTO `sc_trans_message` VALUES (1,'de','Geschätzter Webseitenwert aller Domains weltweit'),(1,'en','Estimated website cost of any domain.'),(1,'es','Estimar costo de cualquier Sitio Web y Dominio.'),(1,'nl','Geschatte website waarde van elk domein'),(1,'ru','Ориентировочная стоимость любого домена.'),(2,'de','Kalkulierter Preis von insgesamt {NumOfWebsites} überprüften Webseiten.'),(2,'en','{NumOfWebsites} total website price calculated.'),(2,'es','{NumOfWebsites} Total de Sitios Web que se ha calculado su precio.'),(2,'nl','{NumOfWebsites} totaal websites prijs berekend.'),(2,'ru','Оценено {NumOfWebsites} веб-сайта(-ов).'),(3,'de','Berechnen'),(3,'en','Calculate'),(3,'es','Calcular'),(3,'nl','Bereken'),(3,'ru','Вычислить'),(4,'de','Raster'),(4,'en','Grid'),(4,'es','Rejilla'),(4,'nl','Grid'),(4,'ru','Сетка'),(5,'de','Liste'),(5,'en','List'),(5,'es','Lista'),(5,'nl','Lijst'),(5,'ru','Список'),(6,'de','Alexa Rang'),(6,'en','Alexa Rank'),(6,'es','Alexa Rank'),(6,'nl','Alexa Rank'),(6,'ru','Alexa Рейтинг'),(7,'de','PageRank'),(7,'en','PageRank'),(7,'es','PageRank'),(7,'nl','Pagina Rank'),(7,'ru','PageRank'),(8,'de','Gefällt mir'),(8,'en','Likes'),(8,'es','Me gusta'),(8,'nl','Likes'),(8,'ru','Лайки'),(9,'de','Geschätzter Preis'),(9,'en','Estimate Price'),(9,'es','Precio Estimado'),(9,'nl','Geschatte prijs'),(9,'ru','Оценочная Стоимость'),(10,'de','Mehr erfahren'),(10,'en','Explore more'),(10,'es','Explorar más'),(10,'nl','Ontdek meer'),(10,'ru','Узнать больше'),(11,'de','Sprache'),(11,'en','Language'),(11,'es','Idioma'),(11,'nl','Taal'),(11,'ru','Язык'),(12,'de','Name'),(12,'en','Name'),(12,'es','Nombre'),(12,'nl','Naam'),(12,'ru','Имя'),(13,'de','Email'),(13,'en','Email'),(13,'es','Email'),(13,'nl','Email'),(13,'ru','Email'),(14,'de','Betreff'),(14,'en','Subject'),(14,'es','Asunto'),(14,'nl','Onderwerp'),(14,'ru','Тема'),(15,'de','Nachricht'),(15,'en','Body'),(15,'es','Cuerpo'),(15,'nl','Body'),(15,'ru','Тело письма'),(16,'de','Verifizierungs-Code'),(16,'en','Verification code'),(16,'es','Código de verificación'),(16,'nl','Verificatiecode'),(16,'ru','Проверочный код'),(17,'de','Senden'),(17,'en','Submit'),(17,'es','Enviar'),(17,'nl','Indienen'),(17,'ru','Отправить'),(18,'de','Wir stehen für alle Anfragen und Probleme zur Verfügung. Bitte füllen Sie das nachfolgende Formular aus um uns via Email zu kontaktieren.'),(18,'en','We are ready to assist you with any questions or problems you may have. Please fill out following form to contact us by e-mail.'),(18,'es','Estamos listos para ayudarle con cualquier duda o problema que pueda tener. Por favor complete siguiente formulario para ponerse en contacto con nosotros por e-mail.'),(18,'nl','Wij staan ​​klaar om u te helpen met eventuele vragen of problemen. Vul onderstaand formulier in om contact met ons op te nemen via e-mail.'),(18,'ru','Мы готовы помочь Вам с любыми вопросами или проблемами с которыми вы столкнулись. Пожалуйста, заполните следующую форму, чтобы связаться с нами по электронной почте.'),(19,'de','Erfahren Sie den Webseitenwert online'),(19,'en','Get website cost online'),(19,'es','Obtener costo en línea del Sitio Web'),(19,'nl','Krijg website waarde online'),(19,'ru','Получить стоимость веб-сайта он-лайн'),(20,'de','webseite kaufen, domain sch&auml;tzen, domain bewertung, domain wert, domain wert kalkulator, domain wert sch&uuml;tzer, wieviel ist meine webseite wert, webseitenpreis, webseite verkaufen, seitenpreis, seitenwert, seitenwert kalkulator, wert meiner webteite, wert einer webseite, web wert, webseitenkalkulator, webseitenbewertung, webseiten sch&auml;tzen, webseiten wert &uuml;berpr&uuml;fen, webseiten wert ermittler, webseiten wert kalkulator, wieviel ist meine webseite wert, wieviel ist meine domain wert, wert des webs, wert einer webseite'),(20,'en','buy website,domain estimator,domain valuation,domain value,domain value calculator,domain value estimator,domain worth,how much is my website worth,price website,sell website,site price,site value,site value calculator,site worth,value my website,value of a website,web worth,website calculator,website price,website price calculator,website valuation,website value,website value calculator,website value checker,website value estimator,website worth,website worth calculator,website worth checker,what is my domain worth,what is my website worth,worth of web,worth of website'),(20,'es','comprar sitio web, calculador de dominio, valorador de dominio, valor de dominio, calculadora de dominios, valor estimado de dominio, cuanto vale dominio, cuánto cuesta mi sitio web, precio costo de sitio web, vender sitio web, precio sitio, valor del sitio, calculadora valor del sitio, vale sitio, valor mi sitio Web, el valor de un sitio web, valor web, calculadora web, precio sitio web, sitio web calculadora de precios, valoración de sitio web, valor sitio web, valor calculado de página web, valor corrector de mi sitio web, valor estimador de web, página web cuanto vale, calculadora evalua sitio web, valor correcto de sitio web, ¿cuál es valor de mi dominio?, ¿cuál es el valor de mi sitio web?, valor de web, valor de sitio web.'),(20,'nl','website, domein schatter, domein waardering, domeinwaarde, de calculator van de waarde van domein, domein waarde schatter, domein waard, hoeveel is mijn website waard, prijs website kopen, verkopen website site prijs, site waarde, site'),(20,'ru','купить веб-сайт, оценочная стоимость домена, оценка домена, стоимость домена, калькулятор расчета стоимости веб-сайта, стоимость веб-сайта, сколько стоит мой веб-сайт, цена веб-сайта, цена домена, цена сайта, стоимость сайта, оценочная цена сайта, калькулятор стоимости сайта, калькулятор стоимости домена, сколько стоит мой домен'),(21,'de','Wieviel kostet eine Webseite? {Portal} ist ein kostenloser Rechner für den Wert einer Webseite und Wertschätzer für Domains. Sie können Webseiten und Domains kaufen und verkaufen ohne zusätzliche Kosten.'),(21,'en','How much is a website price? {Portal} is a free website worth calculator and domain value estimator. You Can Sell and Buy Websites and Domain names with no cost.'),(21,'es','¿Cuál es el precio de un sitio web? {Portal} es un sitio web gratuito que calcula el valor estimado de un Dominio y Sitio Web. Usted puede vender y comprar sitios web y nombres de dominio sin ningún costo.'),(21,'nl','Hoeveel is een  website waard? {Portal} is een gratis website en domeinnaam waarde rekenmachine. Je kunt  Websites en Domeinnamen verkopen en kopen zonder enige kosten.'),(21,'ru','Сколько стоит веб-сайт? {Portal} - это бесплатный сервис для расчета примерной стоимости веб-сайта. Также вы можете покупать и выставлять веб-сайты на продажу бесплатно.'),(22,'de','Kontaktieren Sie uns'),(22,'en','Contact us'),(22,'es','contáctenos'),(22,'nl','Neem contact met ons op'),(22,'ru','Контакты'),(23,'de','{Portal} Kontaktformular, Kontakt {Portal}'),(23,'en','{Portal} contact form, contact {Portal}'),(23,'es','{Portal} formulario de contacto, el contacto {Portal}'),(23,'nl','{Portal} Contactformulier, Contact {Portal}'),(23,'ru','{Portal} контактная форма, контакты {Portal}'),(24,'de','Verkaufe Sie Ihre Webseiten oder Ihre Domains'),(24,'en','Sell Websites - Sell Domains'),(24,'es','Vender Sitios Web - Vender Dominios'),(24,'nl','Verkoop Websites - verkoop Domeinen'),(24,'ru','Продать Веб-сайт - Продать Домен'),(25,'de','Wollen Sie Ihre Webseite oder Ihren Domainnamen verkaufen?'),(25,'en','Do you want to sell your website, sell your domain name?'),(25,'es','¿Quiere vender su Sitio Web y/o su nombre de dominio?'),(25,'nl','Wilt u uw website of uw domeinnaam verkopen?'),(25,'ru','Хотите продать свой веб-сайт или домен?'),(26,'de','{Portal} erlaubt Ihnen Ihre Webseite/Domain zu verkaufen und Kaufinteressenten mit Ihnen in Kontakt zu treten.'),(26,'en','{Portal} allows people you to sell your website or domain name and allow people contact with you who may interest with your website/domain.'),(26,'es','{Portal} permite a las personas vender su Sitio Web o nombre de Dominio y permiten que las personas interesadas en su SitioWeb/Dominio lo contactacten.'),(26,'nl','{Portal} stelt mensen in de gelegenheid om uw website of de domeinnaam te verkopen en mensen, die interesse hebben in uw website of domeinnaam, in staat stellen contact met u op te nemen.'),(26,'ru','{Portal} позволяет людям выставлять на продажу их сайты или домены, а также дает возможность потенциальным покупателям контактировать с продавцами.'),(27,'de','Um den Verkauf Ihrer Webseite oder Domain voran zu treiben, haben wir für Sie ein paar Instruktionen parat.'),(27,'en','To tell people that you may sell your website or sell domain name with a good offer follow this easy instructions.'),(27,'es','Para decirle a la gente que usted desea vender su Sitio Web o Dominio con una buena oferta, debe seguir estas fáciles instrucciones.'),(27,'nl','Om de mensen te vertellen dat u uw website wilt verkopen of de domeinnaam wilt verkopen met een goede aanbieding volg deze eenvoudige instructies .'),(27,'ru','Чтобы рассказать людям, что вы продаете веб-сайт или домен по хорошей цене - следуйте изложенными ниже инструкциями.'),(28,'de','Registrieren/Anmelden auf {Portal}.'),(28,'en','Register/Login to {Portal}.'),(28,'es','Registrarse/Entrar a {Portal}.'),(28,'nl','Registreer / Meld je aan {Portal}.'),(28,'ru','Зарегистрируйтесь или авторизуйтесь на {Portal}.'),(29,'de','Überprüfen Sie Ihre Webseite/Domain auf {Portal} (öffnen Sie unsere {Portal} Webseite ,tragen Sie Ihren Webseiten-/Domainnamen ein und drücken auf \"Kalkulieren\").'),(29,'en','Check your website/domain on {Portal} (Open {Portal} home page and write your website/domain name then click \"Calculate\").'),(29,'es','Compruebe su sitio web / dominio en {Portal} (Abierto {Portal} página de inicio y escribir su página web / nombre de dominio a continuación, haga clic en \"Calcular\").'),(29,'nl','Controleer uw website / domein {Portal} (Open {Portal} home page en schrijf uw website / domeinnaam en klik op \"Bereken\").'),(29,'ru','Проверьте ваш веб-сайт/домен на {Portal} (Откройте главную страницу на {Portal} и введите адрес вашего веб-сайта или домена и нажмите \"Рассчитать\").'),(30,'de','Nach der Analyse und Kalkulation Ihrer Webseite, klicken Sie den\"\"Meine Webseite/Domain verkaufen\" Link unterhalb des Widget Bereiches oder wechseln Sie zum Dashboard über das Menü'),(30,'en','After analysis and calculation of your website price done, click \"Sell my website/domain\" link under Widgets section. Or click Dashboard link on the menu.'),(30,'es','Despues de haber realizado el análisis y el cálculo del precio de su Sitio Web, haga clic en la liga \"Vender mi Sitio Web/Dominio\" en la sección Widgets. O haga clic en la liga de la Portada en el menú.'),(30,'nl','Na analyse en berekening van de waarde van uw website, klikt u op \"Verkoop mijn website / domein\" link onder Widgets sectie. Of klik Dashboard koppeling in het menu.'),(30,'ru','После анализа и расчета Вашего веб-сайта, нажмите кнопку \"Продать сайт/доман\" под виджетом.'),(31,'de','Seitenüberprüfungsseite öffnet sich. Geben Sie den HTML Code auf Ihrer Webseite (Index - root) ein und beantworten Sie die gefragten Informationen. Und klicken Sie auf den \"Überprüfen\" Knopf. Sobald die Überprüfung erfolgreich abgeschlossen ist, füllen Sie bitte das Formular aus und Ihre Webseite/Domain wird auf {Portal} verkauft.'),(31,'en','Site Verification page opens. Put the HTML code on your website main(root) page and enter the information that asked. And click \"Verify\" button. If verification done succesfully then fill out the form and your website/domain started to sell on {Portal}.'),(31,'es','Verificación de pagína abierta del sitio. Ponga el código HTML en la pagina principal (root) de su sitio web e introduzca la información que solicitó. Y haga clic en el botón \"Verificar\". Si la verificación se ha realizado con éxito llene el siguiente formulario y su Sitio Web/dominio comenzará a venderse en {Portal}.'),(31,'nl','Site Verificatie wordt geopend. Zet de HTML-code op uw website main (root) en vul de informatie in die gevraagd wordt. En klik op \"Controleer\" knop. Als de verificatie met succes is gedaan dan het formulier invullen en uw website / domeinnaam begint te verkopen op {Portal}.'),(31,'ru','Откроется страница верификации. Вставьте предлагаемый HTML код на главную страницу вашего веб-сайта и введите информацию которую Вас попросят. Нажмите кнопку \"Проверить\". Если верификация пройдет успешно и форма заполнена верно, то ваш веб-сайт/домен начнет продаваться на {Portal}.'),(32,'de','berprüfen Sie Ihre Webseite/Domain welche unter {Buy Websites} Seite gelistet ist.'),(32,'en','Check your domain/website listed on {Buy Websites} page.'),(32,'es','Compruebe su dominio/Sitio Web que aparece listado en la página {Buy Websites}'),(32,'nl','Controleer uw domein / website genoteerd op {Buy Websites} pagina website.'),(32,'ru','Проверьте Ваш веб-сайт или домен на странице {Buy Websites}.'),(33,'de','Webseiten kaufen'),(33,'en','Buy Websites'),(33,'es','Comprar Sitio Web'),(33,'nl','Websites Kopen'),(33,'ru','Купить веб-сайты'),(34,'de','Warten Sie auf Anfragen. Wenn jemand Interesse an Ihrer Webseite/Domain zeigt, werden Sie eine Email mit den nötigen Kontaktdaten des Interessenten erhalten.'),(34,'en','And wait for people contact with you. If anybody interesting with your website/domain name, you will receive an e-mail from your potential buyer including his/her contact information.'),(34,'es','Y esperar a que la gente entre en contacto con usted. Si alguien se interesa por su sitio web/dominio, recibirá un e-mail de su potencial comprador incluyendo su información de contacto.'),(34,'nl','En wacht tot de mensen contact met u opnemen. Als iemand interessant in uw website / domeinnaam heeft, ontvangt u een e-mail van uw potentiële koper inclusief zijn / haar contactgegevens.'),(34,'ru','Ждите пока с вами свяжутся. Если кто-нибудь заинтересуется вашим веб-сайтом или доменом, то вы получите уведомление на почту с данными о потенциальном покупателе.'),(35,'de','berprüfen Sie Ihre zum Verkauf stehendeWebseite und Domain'),(35,'en','Check websites and domains on sale'),(35,'es','Visite los Sitios Web y dominios en venta'),(35,'nl','Controleer websites en domeinen te koop'),(35,'ru','Веб-сайты и домены на продажу'),(36,'de','verkaufe Domain, verkaufe Webseite, verkaufe Webseite für umsonst, verkaufe Domain für umsonst, verkaufe meine Domain, verkaufe meine Webseite'),(36,'en','sell domain,sell website,sell website for free,sell domain for free,sell my domain,sell my website'),(36,'es','venta dominios, venta Sitios Web, venta dominio gratis, venta Sitios Web gratis, vender mi dominio, vender mi sitio web'),(36,'nl','domein  verkopen, website verkopen ,gratis, verkopen domeinnaam gratis, mijn domein te verkopen, mijn website verkopen'),(36,'ru','продать домен, продать веб-сайт, продать веб-сайт бесплатно, продать домен бесплатно, продать мой веб-сайт, продать мой домен'),(37,'de','Wie kann ich meine Webseite oder Domain auf {Portal} Verkaufen'),(37,'en','How to sell my website or domain on {Portal}'),(37,'es','Cómo vender mi Sitio Web o Dominio en {Portal}'),(37,'nl','Hoe moet ik mijn website of domein verkopen op {Portal}'),(37,'ru','Как продать мой веб-сайт или домен на {Portal}'),(38,'de','Liste der gerade abgefragten Webseiten. Seite {PageNr}'),(38,'en','List of upcoming websites. Page {PageNr}'),(38,'es','Lista de los próximos sitios web. Página {PageNr}'),(38,'nl','Lijst van stijgende websites. Pagina {PageNr}'),(38,'ru','Список новых веб-сайтов. Страница {PageNr}'),(39,'de','Top Webseiten nach Kosten. Seite {PageNr}'),(39,'en','Top websites by cost. Page {PageNr}'),(39,'es','Mejores Sitios Web por costo. Página {PageNr}'),(39,'nl','Top websites waarde. Pagina {PageNr}'),(39,'ru','Лучшие по цене. Страница {PageNr}'),(40,'de','Top Webseiten nach PageRank. Diagramm'),(40,'en','Top websites by PageRank. Diagram'),(40,'es','Mejores Sitios Web de PageRank. Diagrama'),(40,'nl','Top websites PageRank. Diagram'),(40,'ru','Лучшие по PageRank. Диаграмма'),(41,'de','Anzahl der Webseiten'),(41,'en','Number of websites'),(41,'es','Número de Páginas Web'),(41,'nl','Aantal websites'),(41,'ru','Количество веб-сайтов'),(42,'de','Land'),(42,'en','Country'),(42,'es','País'),(42,'nl','Land'),(42,'ru','Страна'),(43,'de','Top Webseiten nach Ländern. Diagramm. Seite {PageNr}'),(43,'en','Top websites by countries. Diagram. Page {PageNr}'),(43,'es','Mejores Sitios Web por países. Diagrama. Página {PageNr}'),(43,'nl','Top websites landen. Diagram. Pagina {PageNr}'),(43,'ru','Лучшие по странам. Диаграмма. Страница {PageNr}'),(44,'de','Geschätzter Wert'),(44,'en','Estimated worth'),(44,'es','Valor estimado'),(44,'nl','Geschatte waarde'),(44,'ru','Примерная стоимость'),(45,'de','TOP im Kostenbereich'),(45,'en','TOP by Cost'),(45,'es','Mejores por Costo'),(45,'nl','TOP Waarde'),(45,'ru','Лучшие по цене'),(46,'de','TOP PageRank'),(46,'en','TOP by PageRank'),(46,'es','Mejores por PageRank'),(46,'nl','TOP PageRank'),(46,'ru','Лучшие по PageRank'),(47,'de','TOP Länder'),(47,'en','TOP by Countries'),(47,'es','Mejores por Países'),(47,'nl','TOP Landen'),(47,'ru','Лучшие по странам'),(48,'de','Top Webseiten in {Country}. Seite {PageNr}'),(48,'en','Top websites in {Country}. Page {PageNr}'),(48,'es','Mejores sitios web en {Country}. Página {PageNr}'),(48,'nl','Top websites {Country}. Pagina {PageNr}'),(48,'ru','Лучшие веб-сайты в стране {Country}.  Страница {PageNr}'),(49,'de','Top Webseiten welche einen {PageRank} PageRank haben. Seite {PageNr}'),(49,'en','Top websites having {PageRank} PageRank. Page {PageNr}'),(49,'es','Mejores Sitios Web que tiene {PageRank}  PageRank. Página {PageNr}'),(49,'nl','Top websites hebben {PageRank} PageRank. Pagina {PageNr}'),(49,'ru','Лучшие веб-сайты, имеющие {PageRank} PageRank. Страница {PageNr}'),(50,'de','Welchen geschätzten Wert hat {Website}?'),(50,'en','How much {Website} is worth?'),(50,'es','¿Cuánto vale {Website}?'),(50,'nl','Hoeveel is {Website} waard?'),(50,'ru','Сколько стоит {Website}?'),(51,'de','{Website} Wert sind {Cost}'),(51,'en','{Website} worth is {Cost}'),(51,'es','El valor de {Website} es de {Coste}'),(51,'nl','{Website} waarde is {Cost}'),(51,'ru','Ориентировочная цена {Website} составляет {Cost}'),(52,'de','{Website} Preis, {Website} Wert, {Website} geschätzter Preis, {Website} geschätzte Kosten, {Website} geschätzter Wert'),(52,'en','{Website} price, {Website} worth, {Website} estimate price, {Website} estimate cost, {Website} estimate worth'),(52,'es','precio de {Website}, valor de {Website}, precio estimado de {Website}, costo estimado de {Website}, valor estimado de {Website}'),(52,'nl','{Website} prijs, {Website} waarde, {Website} geschatte waarde, {Website} geschatte prijs, {Website} geschatte waarde'),(52,'ru','цена {Website}, стоимость {Website}, ориентировочная цена {Website}, ориентировочная стоимость {Website}'),(53,'de','Webseitenkosten'),(53,'en','Website cost'),(53,'es','Costo del  Sitio Web'),(53,'nl','Website waarde'),(53,'ru','Стоимость веб-сайта'),(54,'de','Die Seite die Sie suchen, existiert nicht'),(54,'en','The page you are looking for doesn\'t exists'),(54,'es','La página que está buscando no existe'),(54,'nl','De pagina die u zoekt bestaat niet'),(54,'ru','Страница, которую вы ищете, не существует'),(55,'de','Hat einen geschätzten Wert von'),(55,'en','Has Estimated Worth of'),(55,'es','Tiene un valor estimado de'),(55,'nl','Heeft een geschatte waarde van'),(55,'ru','Имеет ориентировачную стоимость'),(56,'de','Seiten Preis wurde kalkuliert am: {Time}'),(56,'en','Site Price calculated at: {Time}'),(56,'es','Precio calculado del Sitio en: {Time}'),(56,'nl','Site prijs berekend op: {Time}'),(56,'ru','Примерная стоимость веб-сайта была рассчитана: {Time}'),(57,'de','Weitere Möglichkeiten'),(57,'en','More actions'),(57,'es','Más acciones'),(57,'nl','Meer akties'),(57,'ru','Действия'),(58,'de','SEO Analyse Ihrer Webseite'),(58,'en','Get website review'),(58,'es','Obtener Revisión del Sitio Web'),(58,'nl','Krijg website beoordeling'),(58,'ru','Обзор веб-сайта'),(59,'de','Webmaster Information'),(59,'en','Webmaster info'),(59,'es','Información del Webmaster'),(59,'nl','Webmaster info'),(59,'ru','Информация для веб-мастеров'),(60,'de','Kostenlos in den Webseitenkatalog eintragen'),(60,'en','Add to catalog. Free'),(60,'es','Agregar a catálogo. Gratis'),(60,'nl','Voeg gratis toe aan de catalogus.'),(60,'ru','Добавить в каталог. Бесплатно'),(61,'de','Holen Sie sich das Widget / Verkaufen Sie Ihre Webseite'),(61,'en','Get widget / Sale website'),(61,'es','Obtener  widget/venta de Sitio Web'),(61,'nl','Verkrijg Verkoop website widget'),(61,'ru','Получить виджет / продать веб-сайт'),(62,'de','Schätzung einer weiteren Webseite'),(62,'en','Estimate other website'),(62,'es','Estimación otro sitio web'),(62,'nl','Schatting andere website'),(62,'ru','Оценить другой веб-сайт'),(63,'de','Dies Webseite steht zum Verkauf'),(63,'en','This website is on sale'),(63,'es','Este Sitio Web está en venta'),(63,'nl','Deze website is te koop'),(63,'ru','Этот веб-сайт продается'),(64,'de','Verkaufsinformation'),(64,'en','Sales Information'),(64,'es','Información de ventas'),(64,'nl','Verkoop informatie'),(64,'ru','Информация о продаже'),(65,'de','Verkaufspreis'),(65,'en','Selling price'),(65,'es','Precio de venta'),(65,'nl','Verkoop prijs'),(65,'ru','Цена продажи'),(66,'de','Eindeutige monatliche Besucher'),(66,'en','Unique monthly visitors'),(66,'es','Visitantes únicos al mes'),(66,'nl','Unieke bezoekers per maand'),(66,'ru','Ежемесячное количество уникальных посетителей'),(67,'de','Monatliche Seitenaufruf'),(67,'en','Monthly page view'),(67,'es','Páginas vistas al mes'),(67,'nl','Maandelijkse pagina view'),(67,'ru','Ежемесячное количество просмотров страниц'),(68,'de','Monatliche Einnahmen'),(68,'en','Monthly revenue'),(68,'es','Ingresos mensuales'),(68,'nl','Maandelijkse inkomsten'),(68,'ru','Ежемесячная прибыль'),(69,'de','Zusätzliche Informationen'),(69,'en','Notes'),(69,'es','Notas'),(69,'nl','Aantekeningen'),(69,'ru','Заметки'),(70,'de','Sie müssen sich anmelden um den Verkäufer zu kontaktieren'),(70,'en','You should login to contact seller'),(70,'es','Usted debe iniciar sesión para contactar con el vendedor'),(70,'nl','Je moet inloggen om contact te kunnen krijgen met de verkoper'),(70,'ru','Вы должны авторизоваться чтобы связаться с продавцом'),(71,'de','Der Verkäufer hat Sie blockiert'),(71,'en','Seller has blocked you'),(71,'es','Vendedor se ha bloqueado'),(71,'nl','De verkoper heeft je geblokkeerd'),(71,'ru','Продавец заблокировал вас'),(72,'de','Sie haben diesen Nutzer geblockt. {Click here} um dies rückgängig zu machen.'),(72,'en','You have blocked this user. {Click here} to unblock'),(72,'es','Se han bloqueado este usuario. {Click here} para desbloquear'),(72,'nl','Je hebt deze gebruiker geblokkeerd. {Click here} om te deblokkeren'),(72,'ru','Вы заблокировали этого пользователя. {Click here} для разблокировки'),(73,'de','Hier klicken'),(73,'en','Click here'),(73,'es','De clic aquí'),(73,'nl','Klik hier'),(73,'ru','Нажмите сюда'),(74,'de','Kontaktieren Sie den Verkäufer'),(74,'en','Contact seller'),(74,'es','Contactar al vendedor'),(74,'nl','Contact verkoper'),(74,'ru','Связаться с продавцом'),(75,'de','Grundlegende Information'),(75,'en','Basic information'),(75,'es','Información básica'),(75,'nl','Basisinformatie'),(75,'ru','Основная информация'),(76,'de','Domain Name'),(76,'en','Domain name'),(76,'es','Nombre de dominio'),(76,'nl','Domein naam'),(76,'ru','Доменное имя'),(77,'de','Titel'),(77,'en','Title'),(77,'es','Título'),(77,'nl','Titel'),(77,'ru','Заголовок'),(78,'de','Schlüsselwörter'),(78,'en','Keywords'),(78,'es','Palabras clave'),(78,'nl','Trefwoorden'),(78,'ru','Ключевые слова'),(79,'de','Beschreibung'),(79,'en','Description'),(79,'es','Descripción'),(79,'nl','Beschrijving'),(79,'ru','Описание'),(80,'de','Suchmaschinen Übersicht'),(80,'en','Search Engine Stats'),(80,'es','Estadísticas de Busqueda'),(80,'nl','Search Engine statistieken'),(80,'ru','Поисковая Статистика'),(81,'de','Google Index'),(81,'en','Google Index'),(81,'es','Google Índice'),(81,'nl','Google Index'),(81,'ru','Google Индекс'),(82,'de','Yahoo Index'),(82,'en','Yahoo Index'),(82,'es','Índice  Yahoo'),(82,'nl','Yahoo Index'),(82,'ru','Yahoo Индекс'),(83,'de','Bing Index'),(83,'en','Bing Index'),(83,'es','Índice Bing'),(83,'nl','Bing Index'),(83,'ru','Bing Индекс'),(84,'de','Google Backlinks'),(84,'en','Google Backlinks'),(84,'es','Google Backlinks (Ligas devueltas)'),(84,'nl','Google backlinks'),(84,'ru','Количество обратный ссылок в Google'),(85,'de','Alexa Übersicht'),(85,'en','Alexa Stats'),(85,'es','Estadísticas Alexa'),(85,'nl','Alexa Stats'),(85,'ru','Alexa Статистика'),(86,'de','Globaler Rang'),(86,'en','Global Rank'),(86,'es','Posición Mundial'),(86,'nl','Wereldwijde ranking'),(86,'ru','Глобальный Ранк'),(87,'de','Verlinkungen'),(87,'en','Links in'),(87,'es','Enlaces en'),(87,'nl','Links in'),(87,'ru','Количество входящих линков'),(88,'de','Anzahl der Bewertungen'),(88,'en','Review count'),(88,'es','Conteo de los comentarios'),(88,'nl','Recensie telling'),(88,'ru','Количество отзывов'),(89,'de','Durchschnittliche Bewertung'),(89,'en','Review average'),(89,'es','Promedio de los comentarios'),(89,'nl','Gemiddelde recensie'),(89,'ru','Средняя оценка отзыв'),(90,'de','Hiesiger Rang'),(90,'en','Local Rank'),(90,'es','Rango local'),(90,'nl','Lokale rank'),(90,'ru','Локальный Ранк'),(91,'de','Ladegeschwindigkeit'),(91,'en','Load speed'),(91,'es','Velocidad de carga'),(91,'nl','Laad snelheid'),(91,'ru','Скорость загрузки'),(92,'de','{Seconds} Sekunden'),(92,'en','{Seconds} Seconds'),(92,'es','{seconds} Segundos'),(92,'nl','{Seconds} Seconden'),(92,'ru','{Seconds} секунд(-ы)'),(93,'de','{Percent} von Webseiten sind schneller'),(93,'en','{Percent} of sites are faster'),(93,'es','{Percent} de los sitios son más rápidos'),(93,'nl','{Percent} van de sites zijn sneller'),(93,'ru','{Percent} процентов веб-сайтов быстрее'),(94,'de','{Percent} von Webseiten sind langsamer'),(94,'en','{Percent} of sites are slower'),(94,'es','{Percent} de sitios son más lentos'),(94,'nl','{Percent} van de sites zijn langzamer'),(94,'ru','{Percent} веб-сайтов медленнее'),(95,'de','Täglich globaler Rang-Trend'),(95,'en','Daily Global Rank Trend'),(95,'es','Tendencia de Ranking Mundial  diarios'),(95,'nl','Dagelijkse wereldwijde ranking trend'),(95,'ru','Ежедневный Глобальный Ранг Трендов'),(96,'de','Facebook Übersicht'),(96,'en','Facebook Stats'),(96,'es','Estadísticas  Facebook'),(96,'nl','Facebook statistieken'),(96,'ru','Статистика Facebook'),(97,'de','Gefällt mir - Anzahl'),(97,'en','Like count'),(97,'es','Cuenta me gusta'),(97,'nl','Like telling'),(97,'ru','Количество Лайков'),(98,'de','Wir oft geteilt?'),(98,'en','Share count'),(98,'es','Compartir conteo'),(98,'nl','Hoeveel is de website gedeeld'),(98,'ru','Количество шарингов'),(99,'de','Kommentaranzahl'),(99,'en','Comment count'),(99,'es','Recuento de comentarios'),(99,'nl','Aantal opmerkingen'),(99,'ru','Количество комментариев'),(100,'de','Klick-Anzahl'),(100,'en','Click count'),(100,'es','Haga clic en la cuenta'),(100,'nl','Aantal kliks'),(100,'ru','Количество кликов'),(101,'de','Gesamtsumme'),(101,'en','Total count'),(101,'es','Conteo total'),(101,'nl','Totaal aantal'),(101,'ru','Общее количество'),(102,'de','Sozial Media übersicht'),(102,'en','Social Stats'),(102,'es','Estadísticas Sociales'),(102,'nl','Social Media statistieken'),(102,'ru','Социальная Статистика'),(103,'de','Twitter'),(103,'en','Twitter'),(103,'es','Twitter'),(103,'nl','Twitter'),(103,'ru','Twitter'),(104,'de','Gplus+'),(104,'en','Gplus+'),(104,'es','Gplus+'),(104,'nl','Gplus+'),(104,'ru','Gplus+'),(105,'de','Antivirus Übersicht'),(105,'en','Antivirus Stats'),(105,'es','Estadísticas Antivirus'),(105,'nl','Antivirus Stats'),(105,'ru','Статистика Антивирусов'),(106,'de','Google'),(106,'en','Google'),(106,'es','Google'),(106,'nl','Google'),(106,'ru','Google'),(107,'de','McAfee'),(107,'en','McAfee'),(107,'es','McAfee'),(107,'nl','McAfee'),(107,'ru','McAfee'),(108,'de','Katalog Übersicht'),(108,'en','Catalog Stats'),(108,'es','Catálogo de Estadísticas'),(108,'nl','Catalogus Stats'),(108,'ru','Статистика Каталогов'),(109,'de','Yahoo'),(109,'en','Yahoo'),(109,'es','Yahoo'),(109,'nl','Yahoo'),(109,'ru','Yahoo'),(110,'de','Dmoz'),(110,'en','Dmoz'),(110,'es','Dmoz'),(110,'nl','Dmoz'),(110,'ru','Dmoz'),(111,'de','Herkunft'),(111,'en','Location Stats'),(111,'es','Estadísticas  de Ubicación'),(111,'nl','Locatie statistieken'),(111,'ru','Статистика Местоположения'),(112,'de','IP Adresse'),(112,'en','IP Address'),(112,'es','Dirección IP'),(112,'nl','IP Adres'),(112,'ru','IP Адрес'),(113,'de','Region'),(113,'en','Region'),(113,'es','Región'),(113,'nl','Regio'),(113,'ru','Регион'),(114,'de','Stadt'),(114,'en','City'),(114,'es','Ciudad'),(114,'nl','Stad'),(114,'ru','Город'),(115,'de','WHOIS'),(115,'en','WHOIS'),(115,'es','WHOIS'),(115,'nl','WHOIS'),(115,'ru','WHOIS'),(116,'de','Ziegen Sie Ihren Seitenbesuchern den Wert Ihrer Webseite'),(116,'en','Show Your Visitors Your Website Value'),(116,'es','Mostrar a tus visitantes el Valor de tu Sitio Web'),(116,'nl','Toon uw bezoekers uw website waarde'),(116,'ru','Покажите Своим посетителям сколько стоит Ваш Веб-сайт'),(117,'de','Code zum Einfügen'),(117,'en','Get code'),(117,'es','Obtener el código'),(117,'nl','Krijg code'),(117,'ru','Получить код'),(118,'de','Webseitenbesitzer? {Sale Website}!'),(118,'en','Website owner? {Sale Website}!'),(118,'es','Dueño de un Sitio Web? {Sale Website}!'),(118,'nl','Website eigenaar? {Sale Website}!'),(118,'ru','Владелец веб-сайта? {Sale Website}!'),(119,'de','Webseite jetzt verkaufen'),(119,'en','Sale Website'),(119,'es','Sitio Web en Venta'),(119,'nl','Verkoop website'),(119,'ru','Продать Веб-сайт'),(120,'de','Coins'),(120,'en','Coins'),(120,'es','Monedas'),(120,'nl','Munten'),(120,'ru','Монеты'),(121,'de','Erfolgreich'),(121,'en','Success'),(121,'es','Éxito'),(121,'nl','Succes'),(121,'ru','Положительно'),(122,'de','Fehlgeschlagen'),(122,'en','Failed'),(122,'es','Error'),(122,'nl','Mislukt'),(122,'ru','Отрицательно'),(123,'de','Email'),(123,'en','Email'),(123,'es','Correo electrónico'),(123,'nl','Email'),(123,'ru','Эл. почта'),(124,'de','Passwort'),(124,'en','Password'),(124,'es','Contraseña'),(124,'nl','Wachtwoord'),(124,'ru','Пароль'),(125,'de','berprüfungs Code'),(125,'en','Verification code'),(125,'es','Código de verificación'),(125,'nl','Verificatiecode'),(125,'ru','Верификационный код'),(126,'de','An mich erinnern'),(126,'en','Remember me'),(126,'es','Recuerdame'),(126,'nl','Onthoud mij'),(126,'ru','Запомнить меня'),(127,'de','Anmeldeformular'),(127,'en','Login Form'),(127,'es','Formulario de acceso'),(127,'nl','Login formulier'),(127,'ru','Форма логина'),(128,'de','Anmelden'),(128,'en','Sign in'),(128,'es','Registrarse'),(128,'nl','Aanmelden'),(128,'ru','Войти'),(129,'de','Passwort vergessen?'),(129,'en','Forgot your password?'),(129,'es','¿Olvidó su contraseña?'),(129,'nl','Wachtwoord vergeten ?'),(129,'ru','Забыли свой пароль?'),(130,'de','Noch kein Konto? {Sign up now}!'),(130,'en','Don\'t have an account yet? {Sign up now}!'),(130,'es','¿No tienes una cuenta todavía? {Sign up now}!'),(130,'nl','Heeft u geen account nog? {Sign up now}!'),(130,'ru','Еще нету аккаунта? {Sign up now}!'),(131,'de','Jetzt registrieren'),(131,'en','Sign up now'),(131,'es','Inscríbete ahora'),(131,'nl','Meld je nu aan'),(131,'ru','Зарегистрироваться сейчас'),(132,'de','Benötigen Sie ein Konto?'),(132,'en','Need an account?'),(132,'es','¿Necesitas una cuenta?'),(132,'nl','Een account nodig?'),(132,'ru','Нужен аккаунт?'),(133,'de','Erstellen Sie ein Nutzerkonto'),(133,'en','Create an account'),(133,'es','Crear una cuenta'),(133,'nl','Maak een account aan'),(133,'ru','Создать аккаунт'),(134,'de','Erstellen eines {InstalledUrl} Kontos geht schnell, einfach und ist kostenlos'),(134,'en','Creating a {InstalledUrl} account is fast, easy, and free'),(134,'es','Creación de una {InstalledUrl} cuenta, es rápido, fácil y gratis'),(134,'nl','Het creëren van een {InstalledUrl} account is snel, gemakkelijk en gratis'),(134,'ru','Создание аккаунта на {InstalledUrl} быстро, просто и безопасно'),(136,'de','Sie haben Ihr Passwort vergessen? Kein Problem. Geben Sie uns Ihre Email-Adresse und wir senden Ihnen eine Nachricht, mit den nötigen Informationen das Passwort neu zu erstellen. Klicken Sie bitte einfach auf den Link in unserer Nachricht um Ihr Passwort zu zurück zu setzen.'),(136,'en','You forgot your password? No big deal. Give us your email address and we will send you you a message with some simple instructions to reset it. You will have to click the link in the email we send in order to reset your password.'),(136,'es','¿Olvidó su contraseña? No se preocupe, a todos nos pasa. Denos su Correo electrónico y le enviaremos un mensaje con unas sencillas instrucciones para restablecerla. Tendrá que hacer clic en el enlace del Correo electrónico que le enviaremos con el fin de restablecer la contraseña.'),(136,'nl','Je wachtwoord vergeten? Geen probleem. Geef ons uw e-mailadres en met enkele eenvoudige instructies zullen wij u een bericht te sturen om het te resetten. U hoeft enkel op de link in de mail die we u sturen te klikken'),(136,'ru','Забыли пароль? Ничего страшного. Введите адрес своей эл. почты и мы отправим вам письмо с дальнейшими инструкциями, как сбросить пароль. Вы должны будете нажать на линк в письме чтобы перейти к сбрасыванию пароля.'),(137,'de','Wir haben solche Email-Adressen nicht gefunden'),(137,'en','We didn\'t find such email address'),(137,'es','No se encontró esta dirección de correo electrónico'),(137,'nl','We hebben geen dergelijke e-mail adres gevonden'),(137,'ru','Мы не нашли такого адреса электронной почты'),(138,'de','warte, ich erinnere mich!'),(138,'en','or wait, I remember!'),(138,'es','o esperar, lo recuerdo!'),(138,'nl','of wacht, ik herinner het me!'),(138,'ru','или подождите, я вспомнил!'),(139,'de','Server ist im Moment nicht erreichbar. Später nochmal probieren.'),(139,'en','Server temporarily unavailable. Try again later.'),(139,'es','Servidor no disponible temporalmente. Inténtalo de nuevo más tarde.'),(139,'nl','Server tijdelijk niet beschikbaar. Probeer het later opnieuw.'),(139,'ru','Сервер временно недоступен. Повторите попытку позже.'),(140,'de','Passwort Rückholung'),(140,'en','Password recovery'),(140,'es','Recuperar contraseña'),(140,'nl','Wachtwoord herstel'),(140,'ru','Востоновление пароля'),(141,'de','Bitte benutzen Sie ein Email-Programm, welches HTML unterstützt'),(141,'en','Please, use mail client which support HTML markup'),(141,'es','Por favor, utilice el cliente de correo que soportan el formato HTML'),(141,'nl','U dient gebruik te maken van een e-mail client die HTML-opmaak ondersteunt'),(141,'ru','Пожалуйста, используйте почтовый клиент, который поддерживает HTML-разметку'),(142,'de','Wir haben Ihnen eine Email mit Informationen zu unserem Service gesendet. Bitte überprüfen Sie Ihren Emaileingang'),(142,'en','We\'ve sent you email with instructions. Please check your inbox'),(142,'es','Hemos le hemos enviado un correo electrónico con instrucciones. Por favor revisa tu correo'),(142,'nl','We hebben je een email gestuurd met instructies. Kijk in je inbox'),(142,'ru','Мы отправили вам письмо с инструкциями по электронной почте. Пожалуйста, проверьте свой почтовый ящик'),(143,'de','Beim Senden der Email ist ein Fehler aufgetreten'),(143,'en','An error occurred while sending email'),(143,'es','Ha ocurrido un error al enviar el email'),(143,'nl','Er is een fout opgetreden bij het versturen van de e-mail'),(143,'ru','Произошла ошибка при отправке электронной почты'),(144,'de','Neues Konto registrieren'),(144,'en','Register new account'),(144,'es','Registrar una nueva cuenta'),(144,'nl','Registreer nieuw account'),(144,'ru','Зарегистрировать новый аккаунт'),(145,'de','Registrierungsformular'),(145,'en','Register Form'),(145,'es','Formulario de Registro'),(145,'nl','Registratie formulier'),(145,'ru','Регистрационная форма'),(146,'de','{Click here} wenn Sie schon ein Konto bei uns haben und Sich anmelden möchten.'),(146,'en','{Click here} if you already have an account and just need to login.'),(146,'es','{Click here} si ya tiene una cuenta y sólo tiene que iniciar sesión.'),(146,'nl','{Click here} als je al een account hebt je hoeft dan alleen maar in te loggen.'),(146,'ru','{Click here} если у вас уже есть аккаунт или вам нужно залогиниться.'),(147,'de','Nutzername'),(147,'en','Username'),(147,'es','Nombre de usuario'),(147,'nl','Gebruikersnaam'),(147,'ru','Имя пользователя'),(148,'de','Email Bestätigung'),(148,'en','Email confirmation'),(148,'es','Confirmación por Correo electrónico'),(148,'nl','Email bevestiging'),(148,'ru','Подтверждение эл. почты'),(149,'de','Nutzerrolle'),(149,'en','User role'),(149,'es','Rol de usuario'),(149,'nl','Gebruikers role'),(149,'ru','Роль пользователя'),(150,'de','Kann Nachrichten versenden'),(150,'en','Can send message'),(150,'es','Puede enviar mensaje'),(150,'nl','Verzend de bericht'),(150,'ru','Может отправлять сообщения'),(151,'de','Nutzer Status'),(151,'en','User status'),(151,'es','Estatus de usuario'),(151,'nl','Gebruikers status'),(151,'ru','Статус пользователя'),(152,'de','Passwort nochmal eingeben'),(152,'en','Re-Password'),(152,'es','Re-Contraseña'),(152,'nl','Wachtwoord herhalen'),(152,'ru','Повтор Пароля'),(153,'de','Registriert am'),(153,'en','Registered at'),(153,'es','Registrado en'),(153,'nl','Geregistreerd bij'),(153,'ru','Дата регистрации'),(154,'de','Bearbeitet am'),(154,'en','Modified at'),(154,'es','Modificado en'),(154,'nl','Gewijzigd op'),(154,'ru','Дата редактирования'),(155,'de','Zuletzt angemeldet'),(155,'en','Last login at'),(155,'es','Última entrada en'),(155,'nl','Laatst ingelogd op'),(155,'ru','Дата последнего логина'),(156,'de','Letzte angemeldete IP-Adresse'),(156,'en','Last login IP Address'),(156,'es','Última entrada de direcciones IP'),(156,'nl','Laatste login IP-adres'),(156,'ru','IP последнего логина'),(157,'de','Passwörter sind nicht gleich'),(157,'en','Passwords do not match'),(157,'es','Las contraseñas no coinciden'),(157,'nl','Wachtwoorden komen niet overeen'),(157,'ru','Пароли не совпадают'),(158,'de','Aktiv'),(158,'en','Active'),(158,'es','Activo'),(158,'nl','Actief'),(158,'ru','Активный'),(159,'de','Geblockt'),(159,'en','Blocked'),(159,'es','Bloqueado'),(159,'nl','Geblokkeerd'),(159,'ru','Заблокирован'),(160,'de','Gelöscht'),(160,'en','Deleted'),(160,'es','Eliminar'),(160,'nl','Vewijder'),(160,'ru','Удален'),(161,'de','Nutzer'),(161,'en','User'),(161,'es','Usuario'),(161,'nl','Gebruiker'),(161,'ru','Пользователь'),(162,'de','Administrator'),(162,'en','Administrator'),(162,'es','Administrador'),(162,'nl','Beheerder'),(162,'ru','Администратор'),(163,'de','Unterverzeichnis'),(163,'en','Root'),(163,'es','Root'),(163,'nl','Root'),(163,'ru','Супер администратор'),(164,'de','Unbekannt'),(164,'en','Unknown'),(164,'es','Desconocido'),(164,'nl','Onbekend'),(164,'ru','Неизвестно'),(165,'de','Registrieren'),(165,'en','Sign up'),(165,'es','Contratar'),(165,'nl','Registreren'),(165,'ru','Зарегистрироваться'),(166,'de','Registriert über {InstalledUrl}'),(166,'en','Registration at {InstalledUrl}'),(166,'es','Registro en {InstalledUrl}'),(166,'nl','Registratie bij {InstalledUrl}'),(166,'ru','Регистрация на {InstalledUrl}'),(167,'de','Die Sprache - {Language} - existiert noch nicht'),(167,'en','The language {Language} doesn\'t exists in the system'),(167,'es','El idioma {Language} no existe en el sistema'),(167,'nl','De taal {Language} bestaat niet in het systeem'),(167,'ru','Язык {Language} не существует в системе'),(168,'de','Vielen Dank'),(168,'en','Regards'),(168,'es','Saludos'),(168,'nl','Vriendelijke groet'),(168,'ru','С уважением'),(169,'de','{Brandname} Administration'),(169,'en','{Brandname} administration'),(169,'es','{Brandname} administratición'),(169,'nl','{Brandname} Administratie'),(169,'ru','Администрация {Brandname}'),(170,'de','Bitte antworten Sie nicht auf diese Nachricht'),(170,'en','Please, do not reply to this message'),(170,'es','Por favor, no responda a este mensaje'),(170,'nl','Je kunt niet antwoorden op dit bericht'),(170,'ru','Пожалуйста, не отвечайте на это сообщение'),(171,'de','Danke für Ihre Registrierung'),(171,'en','Thank you for registration'),(171,'es','Gracias por registrarse'),(171,'nl','Bedankt voor je registratie'),(171,'ru','Спасибо за регистрацию'),(172,'de','Hallo {Name}'),(172,'en','Dear {Name}'),(172,'es','Estimado {Name}'),(172,'nl','Beste {Name}'),(172,'ru','Здравствуйте, {Имя}'),(173,'de','Danke für Ihre Registrierung auf {BranUrl}. Um Ihr Benutzerkonto zu aktivieren, folgen Sie bitte diesem Link: {VerifyUrl}'),(173,'en','Thank your for your registration at {BranUrl}. To activate your account please follow this link: {VerifyUrl}'),(173,'es','Gracias por su inscripción en {BranUrl}. Para activar su cuenta, por favor, siga este enlace: {VerifyUrl}'),(173,'nl','Bedankt voor je registratie bij {BranUrl}. Om je accout te aktiveren gebruik de volgende link: {VerifyUrl}'),(173,'ru','Благодарим Вас за регистрацию на {BranUrl}. Чтобы активировать свой аккаунт перейдите по ссылке: {VerifyUrl}'),(174,'de','Passwort vergessen?'),(174,'en','Forgot password?'),(174,'es','¿Has olvidado tu contraseña?'),(174,'nl','Paswoord vergeten?'),(174,'ru','Забыли пароль?'),(175,'de','Wir erhielten eine Benachrichtigung zum Zurücksetzen Ihres Passwortes. Sollte dies nicht der Fall sein, entschuldigen wir uns und bitten Sie diese Nachricht zu ignorieren.'),(175,'en','We\'ve received a request from you to reset a password. If you didn\'t do it, then just ignore this message.'),(175,'es','Hemos recibido una solicitud de usted para restablecer una contraseña. Si usted no lo solicitó, entonces ignore este mensaje.'),(175,'nl','We hebben een verzoek voor een nieuw wachtwoord. Als je het niet gedaan hebt, dan gewoon dit bericht negeren.'),(175,'ru','Мы получили запрос от Вас, по поводу сброса пароля. Если Вы этого не делали, то просто проигнорируйте это сообщение.'),(176,'de','Um das Passwort zurück zu setzen, folgen Sie bitte diesem Link: {RecoveryLink} und geben Sie ein neues Passwort ein.'),(176,'en','To reset password open following link: {RecoveryLink} and enter new password.'),(176,'es','Para restablecer la contraseña abra el siguiente enlace: {RecoveryLink} e introduzca la nueva contraseña.'),(176,'nl','Om je wachtwoord te resetten gebruik de link: {RecoveryLink} en voer een nieuw wachtwoord in.'),(176,'ru','Для сброса пароля перейдите по ссылке: {RecoveryLink} и введите новый пароль.'),(177,'de','Sie haben eine neue Nachricht!'),(177,'en','You got new message!'),(177,'es','¡Tienes un nuevo mensaje!'),(177,'nl','Je hebt een nieuw bericht'),(177,'ru','Вы получили новое сообщение!'),(178,'de','Sie haben eine neue Nachricht erhalten von {Name}. Öffnen Sie den nachfolgenden Link um die Nachricht zu lesen: {messageLink}'),(178,'en','You\'ve received new message from {Name}. Open following link to see a message: {messageLink}'),(178,'es','Has recibido un nuevo mensaje de {Name}. Abra el enlace siguiente para ver un mensaje: {messageLink}'),(178,'nl','Je hebt een nieuw bericht van {Name}. Gebruik de volgende link om het bericht te openen: {messageLink}'),(178,'ru','Вы получили новое сообщение от {Name}. Пройдите по ссылке чтобы увидеть сообщение: {messageLink}'),(179,'de','Schauen Sie jetzt in Ihr Email-Postfach (Keine da? Bitte Spamordner überprüfen)'),(179,'en','Go check your email and confirm RIGHT NOW'),(179,'es','Revise su correo electrónico y confirme AHORA!'),(179,'nl','Controleer U email en bevestig'),(179,'ru','Проверьте свою почту и подтвердите ПРЯМО СЕЙЧАС'),(180,'de','Eine Bestätigungs-Email wurde gesendet an {ConfirmationEmail}. Bitte überprüfen Sie Ihren Posteingang, öffnen diese Email und bestätigen den Aktivierungslink.'),(180,'en','A confirmation email has been sent on {ConfirmationEmail}. Please go to your inbox now, open the email and click on activation link.'),(180,'es','Un correo electrónico de confirmación ha sido enviado a {ConfirmationEmail}. Por favor, revise su bandeja de entrada ahora, abrir el correo electrónico y haga clic en el enlace de activación.'),(180,'nl','Er is een bevestigingsmail verstuurd naar {ConfirmationEmail}. Ga naar je inbox nu, open de e-mail en klik op de activatie link.'),(180,'ru','Сообщение о подтверждении было отправлено на {ConfirmationEmail}. Пожалуйста, зайдите в свой почтовый ящик и пройдите по ссылке активации.'),(181,'de','Der Überprüfungs-Token ist abgelaufen oder nicht valide. Bitte probieren Sie eine neues Konto zu erstellen'),(181,'en','The token is expired or does not valid. Please, try to register new account'),(181,'es','El token ha caducado o no es válido. Por favor, intente dar de alta una nueva cuenta'),(181,'nl','Het token is verlopen of is niet geldig. Alsjeblieft, probeer een nieuwe account te registreren'),(181,'ru','Токен истек или не действует. Пожалуйста, попробуйте зарегистрировать новую учетную запись'),(182,'de','Server ist im Moment nicht erreichbar. Bitte probieren Sie es später noch mal sich zu registrieren'),(182,'en','Server temporarily unavailable. Please, try to register a little bit later'),(182,'es','Servidor no disponible temporalmente. Por favor, intente registrar un poco más tarde'),(182,'nl','Server tijdelijk niet beschikbaar. Probeer alsjeblieft later te registreren'),(182,'ru','Сервер временно недоступен. Пожалуйста, попробуйте зарегистрироваться немного позже'),(183,'de','Gratulation!'),(183,'en','Congratulations!'),(183,'es','¡En hora buena!'),(183,'nl','Gefeliciteerd!'),(183,'ru','Поздравляем!'),(184,'de','Sie haben Ihre Email-Adresse erfolgreich bestätigt.'),(184,'en','You have successfully confirmed your email address.'),(184,'es','Usted ha confirmado con éxito su dirección de correo electrónico.'),(184,'nl','Je hebt met succes uw e-mailadres bevestigd.'),(184,'ru','Вы успешно подтвердили ​адрес электронной почты.'),(185,'de','Beim Überprüfen Ihrer Email ist ein Fehler aufgetreten. Bitte probieren Sie es nochmals.'),(185,'en','An error has occurred while trying to confirm your email. Please try again later.'),(185,'es','Se ha producido un error al tratar de confirmar su correo electrónico. Por favor, inténtelo de nuevo más tarde.'),(185,'nl','Er is een fout opgetreden bij uw e-mail te bevestigen. Probeer het later opnieuw.'),(185,'ru','Произошла ошибка при попытке подтвердить адрес электронной почты. Пожалуйста, повторите попытку позже.'),(186,'de','Achtung!'),(186,'en','Attention!'),(186,'es','¡Atención!'),(186,'nl','Aandacht!'),(186,'ru','Внимание!'),(187,'de','Webseiten in der Kategorie {Category}. Seite {PageNr}'),(187,'en','Websites in the category {Category}. Page {PageNr}'),(187,'es','Sitios Web en la categoría {Category}. Página {PageNr}'),(187,'nl','Websites in de categorie {Category}. Pagina {PageNr}'),(187,'ru','Веб сайты в категории {Category}. Страница {PageNr}'),(188,'de','Webseite die zum Verkauf stehen. Seite {PageNr}'),(188,'en','Websites on sale. Page {PageNr}'),(188,'es','Sitios web en venta. Página {PageNr}'),(188,'nl','Websites te koop. Pagina {PageNr}'),(188,'ru','Продающиеся веб сайты. Страница {PageNr}'),(189,'de','Passwort setzen'),(189,'en','Set new password'),(189,'es','Establecer nueva contraseña'),(189,'nl','Stel een nieuw wachtwoord in'),(189,'ru','Установить новый пароль'),(190,'de','Das Passwort wurde geändert. Bitte neues Passwort zum Anmelden verwenden'),(190,'en','The password has been changed. Use new password to login'),(190,'es','La contraseña ha sido cambiada. Utilice la nueva contraseña para iniciar sesión'),(190,'nl','Het wachtwoord is gewijzigd. Gebruik het nieuwe wachtwoord om in te loggen'),(190,'ru','Пароль был изменен. Используйте новый пароль для входа в систему'),(191,'de','Nichts gefunden'),(191,'en','Nothing found'),(191,'es','No se ha encontrado nada'),(191,'nl','Niets gevonden'),(191,'ru','Ничего не найдено'),(192,'de','Anzeige von {start}-{end} über {count} ergebnisse.'),(192,'en','Displaying {start}-{end} of {count} results.'),(192,'es','Mostrando {start} - {end} de {count} resultados.'),(192,'nl','Getoond {start}-{einde} van {count} resultaten.'),(192,'ru','Показано {start}-{end} из {count} результатов.'),(193,'de','Sortiert nach'),(193,'en','Sort by'),(193,'es','Ordenar por'),(193,'nl','Sorteer op'),(193,'ru','Сортировать'),(194,'de','Hinzugefügt zu'),(194,'en','Added at'),(194,'es','Alta en'),(194,'nl','Toegevoegd op'),(194,'ru','Добавлен'),(195,'de','Aufsteigende Sortierung'),(195,'en','Ascending order'),(195,'es','Orden Ascendente'),(195,'nl','Aflopende volgorde'),(195,'ru','По возрастанию'),(196,'de','Absteigende Sortierung'),(196,'en','Descending order'),(196,'es','Órden descendente'),(196,'nl','Oplopende volgorde'),(196,'ru','По убыванию'),(197,'de','Verkaufsdatum'),(197,'en','Selling date'),(197,'es','Fecha de Venta'),(197,'nl','Verkoop datum'),(197,'ru','Дата начала продаж'),(198,'de','Ist verkauft seit'),(198,'en','Is sold since'),(198,'es','Se vende desde'),(198,'nl','Is verkocht sinds'),(198,'ru','В продаже с'),(201,'de','Nichts wurde verkauft'),(201,'en','Nothing is sold'),(201,'es','Nada está en venta'),(201,'nl','Niets is verkocht'),(201,'ru','Ничего не выставлено на продажу'),(202,'de','ID'),(202,'en','ID'),(202,'es','ID'),(202,'nl','ID'),(202,'ru','ID'),(203,'de','Domain/Webseite'),(203,'en','Domain/Website'),(203,'es','Dominio/Sitio Web'),(203,'nl','Domain/Website'),(203,'ru','Домен/Веб-сайт'),(204,'de','Kategorie'),(204,'en','Category'),(204,'es','Categoría'),(204,'nl','Categorie'),(204,'ru','Категория'),(205,'de','Ansehen'),(205,'en','View'),(205,'es','Ver'),(205,'nl','Bekijk'),(205,'ru','Просмотр'),(206,'de','Bearbeiten'),(206,'en','Edit'),(206,'es','Editar'),(206,'nl','Bewerken'),(206,'ru','Редактировать'),(207,'de','Vom Verkauf entfernen'),(207,'en','Remove from sale'),(207,'es','Eliminar de la venta'),(207,'nl','Verwijderen uit verkoop'),(207,'ru','Убрать с продажи'),(208,'de','Sind Sie sicher, dass Sie dies löschen möchten?'),(208,'en','Are you sure you want to delete this item?'),(208,'es','¿Seguro que quieres borrar este elemento?'),(208,'nl',''),(208,'ru','Вы уверены, что хотите удалить этот элемент?'),(209,'de','Webseite besuchen'),(209,'en','Visit website'),(209,'es','Visite el Sitio Web'),(209,'nl','Bezoek website'),(209,'ru','Посетить веб-сайт'),(210,'de','Meine zum Verkauf stehenden Webseiten/Domains'),(210,'en','My Websites/Domains on Sale'),(210,'es','Mi Sitio Web/Dominios en Venta'),(210,'nl','Mijn Websites / domeinen in de aanbieding'),(210,'ru','Мои Веб-сайты/Домены на продаже'),(211,'de','Feld'),(211,'en','Field'),(211,'es','campo'),(211,'nl','Veld'),(211,'ru','Поле'),(212,'de','Wert'),(212,'en','Value'),(212,'es','valor'),(212,'nl','Waarde'),(212,'ru','Значение'),(213,'de','Information wurde aktualisiert'),(213,'en','Information has been updated'),(213,'es','La información ha sido actualizada'),(213,'nl','Informatie is bijgewerkt'),(213,'ru','Информация была обновлена'),(214,'de','Die Webseite steht bereits zum Verkauf'),(214,'en','The website is already on sale'),(214,'es','El Sitio Web ya está en venta'),(214,'nl','De website is al te koop'),(214,'ru','Веб-сайт уже продается'),(215,'de','Email wurde gesendet'),(215,'en','Email has been sent'),(215,'es','El correo electrónico ha sido enviado'),(215,'nl','E-mail is verzonden'),(215,'ru','Эл. сообщение было отправлено'),(216,'de','Bitte eine Kategorie wählen'),(216,'en','Please choose category'),(216,'es','Por favor, seleccione la categoría'),(216,'nl','Kies categorie'),(216,'ru','Пожалуйста выберите категорию'),(217,'de','Aktualisierung'),(217,'en','Update'),(217,'es','Actualización'),(217,'nl','Updaten'),(217,'ru','Обновить'),(218,'de','Zum Verkaufen einstellen'),(218,'en','Put up for sale'),(218,'es','Colocado para venta'),(218,'nl','Te koop aangeboden'),(218,'ru','Выставить на продажу'),(219,'de','Das Widget wurde gefunden'),(219,'en','The widget has been found'),(219,'es','El widget ha sido encontrado'),(219,'nl','De widget is gevonden'),(219,'ru','Виджет был найден'),(220,'de','Das Widget wurde nicht gefunden'),(220,'en','The widget has not been found'),(220,'es','No se ha encontrado el widget'),(220,'nl','De widget is niet gevonden'),(220,'ru','Виджет не был найден'),(221,'de','Webseite {Domain} wurde vom Verkauf entfernt'),(221,'en','Website {Domain} has been removed from sale'),(221,'es','Sitio web {Domain} ha sido retirado de la venta'),(221,'nl','Website {Domain} is verwijderd uit de verkoop'),(221,'ru','Веб-сайт {Domain} был убран с продаж'),(222,'de','Widget in Ihre Hauptseite einfügen und klicken Sie den \"Verifizieren\" Knopf. Sie können HTML/CSS überarbeiten, aber lassen Sie den - do follow - Link bestehen.'),(222,'en','Insert widget on your main page and click \"verify\" button. Feel free to modify HTML/CSS, but leave do follow link.'),(222,'es','Inserte el widget en su página principal y haga clic en el botón \"verificar\". Siente la confianza de modificar HTML/CSS, pero no deje de seguir el enlace.'),(222,'nl','Zet de widget op je hoofdpagina en klik op \"verifiëren\" knop. Voel je vrij om HTML / CSS te wijzigen, maar laat de volg link intact.'),(222,'ru','Вставьте код виджета на главную страницу вашего веб-сайта. Вы можете модифицировать HTML/CSS как вам угодно, только оставьте do follow линк, иначе бот не сможет определить ваш веб-сайт.'),(223,'de','Überprüfen'),(223,'en','Verify'),(223,'es','Compruebe'),(223,'nl','Verifiëren'),(223,'ru','Проверить'),(224,'de','Füllen Sie das Formular aus'),(224,'en','Fill out the form'),(224,'es','Rellene el formulario'),(224,'nl','Vul het formulier in'),(224,'ru','Заполните форму'),(225,'de','Webseite/Domain verkaufen'),(225,'en','Sell Website/Domain'),(225,'es','Vende Sitio Web/Dominio'),(225,'nl','Verkoop Website / Domein'),(225,'ru','Продать веб-сайт/домен'),(226,'de','Gratulation. Ihre Webseite/Domain steht zum Verkauf'),(226,'en','Congratulations. Your website/domain is on sale'),(226,'es','¡Felicitaciones!. Su sitio web/dominio está en venta'),(226,'nl','Gefeliciteerd. Uw website / domeinnaam staat te koop'),(226,'ru','Поздравляем. Ваш веб-сайт/домен выставлен на продажу'),(227,'de','Posteingang'),(227,'en','Inbox'),(227,'es','Bandeja de entrada'),(227,'nl','Inbox'),(227,'ru','Входящие'),(228,'de','Senden'),(228,'en','Sent Items'),(228,'es','Elementos enviados'),(228,'nl','Verzonden Items'),(228,'ru','Отправленные'),(229,'de','Favoriten'),(229,'en','Starred'),(229,'es','Destacado'),(229,'nl','Met ster'),(229,'ru','Избранные'),(230,'de','Wichtig'),(230,'en','Important'),(230,'es','Importante'),(230,'nl','Belangrijk'),(230,'ru','Важные'),(231,'de','Abfall'),(231,'en','Trash'),(231,'es','Papelera'),(231,'nl','Trash'),(231,'ru','Удаленные'),(232,'de','Blockierte Nutzer'),(232,'en','Blocked users'),(232,'es','Usuarios bloqueados'),(232,'nl','Geblokkeerde gebruikers'),(232,'ru','Заблокированные пользователи'),(233,'de','Aktion'),(233,'en','Action'),(233,'es','Acción'),(233,'nl','Aktie'),(233,'ru','Действие'),(234,'de','Aktualisieren'),(234,'en','Refresh'),(234,'es','Actualizar'),(234,'nl','Vernieuwen'),(234,'ru','Обновить'),(235,'de','Neu'),(235,'en','New'),(235,'es','Nuevo'),(235,'nl','Nieuw'),(235,'ru','Новое'),(236,'de','Als gelesen markieren'),(236,'en','Mark as Read'),(236,'es','Marcar como leído'),(236,'nl','Markeren als gelezen'),(236,'ru','Пометить как прочитанное'),(237,'de','Als Favorit markieren'),(237,'en','Mark as Starred'),(237,'es','Marcar como Destacado'),(237,'nl','Markeren met ster'),(237,'ru','Пометить как избранное'),(238,'de','Von den wichtigen Nachrichten entfernen'),(238,'en','Remove from Important folder'),(238,'es','Eliminar de la carpeta Importante'),(238,'nl','Verwijderen uit map Belangrijk'),(238,'ru','Удалить из папки Важные'),(239,'de','In den Papierkorb'),(239,'en','Move to Trash'),(239,'es','Mover a la papelera'),(239,'nl','Naar de prullenbak verplaatsen'),(239,'ru','Переместить в удаленные'),(240,'de','Als wichtig markieren'),(240,'en','Mark as Important'),(240,'es','Marcar como importante'),(240,'nl','Markeren als belangrijk'),(240,'ru','Пометить как важное'),(241,'de','Von den Favoriten entfernen'),(241,'en','Remove from Starred folder'),(241,'es','Eliminar de la carpeta Destacados'),(241,'nl','Verwijderen uit Ster map'),(241,'ru','Удалить из папки Избранные'),(242,'de','Komplett entfernen'),(242,'en','Completely remove'),(242,'es','Retire completamente'),(242,'nl','Volledig verwijderen'),(242,'ru','Удалить на всегда'),(243,'de','Gelöschte Nachrichten wieder herstellen'),(243,'en','Restore from Trash folder'),(243,'es','Restaurar desde la Papelera'),(243,'nl','Herstellen vanaf Prullenbak'),(243,'ru','Восстановить из папки Удаленные'),(244,'de','Meine Nachrichten'),(244,'en','My messages'),(244,'es','Mis mensajes'),(244,'nl','Mijn berichten'),(244,'ru','Мои сообщения'),(245,'de','Nachricht wurde gesendet'),(245,'en','Message has been sent'),(245,'es','El mensaje ha sido enviado'),(245,'nl','Bericht is verzonden'),(245,'ru','Сообщение было отправлено'),(246,'de','Neue Nachricht'),(246,'en','New message'),(246,'es','Nuevo mensaje'),(246,'nl','Nieuw bericht'),(246,'ru','Новое сообщение'),(247,'de','Ein interner Fehler ist aufgetreten. Bitte probieren Sie es nochmals.'),(247,'en','An internal error occurred. Please try again later'),(247,'es','Se ha producido un error interno. Por favor, inténtelo de nuevo más tarde'),(247,'nl','Er is een interne fout opgetreden. Probeer het later opnieuw'),(247,'ru','Произошла внутренняя ошибка. Пожалуйста, повторите попытку позже'),(248,'de','Nachricht'),(248,'en','Message'),(248,'es','Mensaje'),(248,'nl','Bericht'),(248,'ru','Сообщение'),(249,'de','Sie haben nicht die nötigen Befugnisse um Nachrichten zu versenden'),(249,'en','Unauthorized users can not send messages'),(249,'es','Los usuarios no autorizados no pueden enviar mensajes'),(249,'nl','Onbevoegde gebruikers kunnen geen berichten verzenden'),(249,'ru','Не авторизованные пользователи не могут отправлять сообщения'),(250,'de','Sie können keine Nachrichten an sich selbst versenden'),(250,'en','You can\'t send message to yourself'),(250,'es','No puedes enviar el mensaje a ti mismo'),(250,'nl','Je mag geen bericht naar jezelf sturen'),(250,'ru','Вы не можете себе отправить сообщение'),(251,'de','Es ist Ihnen nicht gestattet Nachrichten zu versenden'),(251,'en','You are forbidden to send messages'),(251,'es','Está prohibido el envío de mensajes'),(251,'nl','U mag geen berichten te verzenden'),(251,'ru','Вам запрещено отправлять сообщения'),(252,'de','Dieser Nutzer hat Sie blockiert'),(252,'en','This user has blocked you'),(252,'es','Este usuario se ha bloqueado'),(252,'nl','Deze gebruiker u heeft geblokkeerd'),(252,'ru','Этот пользователь вас заблокировал'),(253,'de','Nachricht senden'),(253,'en','Send message'),(253,'es','Enviar mensaje'),(253,'nl','Verstuur bericht'),(253,'ru','Отправить сообщение'),(254,'de','Aus dem Ordner der Verkauftenbenachrichtigungen entfernen'),(254,'en','Removed from sale'),(254,'es','Eliminado de venta'),(254,'nl','Verwijderd uit de verkoop'),(254,'ru','Снят с продажи'),(255,'de','Die Webseite wurde verkauft oder vom Verkauf entfernt'),(255,'en','The website has been sold or removed from sales.'),(255,'es','La página web se ha vendido o retirado de la venta.'),(255,'nl','De website is verkocht of verwijderd uit de verkoop.'),(255,'ru','Веб-сайт был продан или удален из продажи.'),(256,'de','Ich bin'),(256,'en','I\'m'),(256,'es','Estoy'),(256,'nl','Ik'),(256,'ru','Я'),(257,'de','Chatten mit {Username}'),(257,'en','Chat with {Username}'),(257,'es','Chatea con {Username}'),(257,'nl','Chat met {Username}'),(257,'ru','Чат с {Username}'),(258,'de','Zurück zum Posteingang'),(258,'en','Back to inbox'),(258,'es','Volver a la bandeja de entrada'),(258,'nl','Terug naar inbox'),(258,'ru','Вернуться в почтовый ящик'),(259,'de','Laden...'),(259,'en','Loading...'),(259,'es','Leyendo, espere ...'),(259,'nl','Laaden...'),(259,'ru','Загрузка...'),(260,'de','Absender blockieren'),(260,'en','Block Sender'),(260,'es','Bloquear remitente'),(260,'nl','Blokkeer afzender'),(260,'ru','Блокировать отправителя'),(261,'de','Betrüger!'),(261,'en','Scammer!'),(261,'es','Estafador!'),(261,'nl','Scammer!'),(261,'ru','Мошенник!'),(262,'de','Chat'),(262,'en','Chat'),(262,'es','Chat'),(262,'nl','Chat'),(262,'ru','Чат'),(263,'de','Sie haben diesen Nutzer blockiert. {Click here} um die Blockade auf zu heben'),(263,'en','You have blocked this user. {Click here} to unblock'),(263,'es','Se ha bloqueado este usuario. {Click here} para desbloquear'),(263,'nl','Je hebt deze gebruiker geblokkeerd. {Klik hier} te deblokkeren'),(263,'ru','Вы заблокировали этого пользователя. {Click here} чтобы разблокировать'),(264,'de','Geben Sie Ihre Nachricht hier ein...'),(264,'en','Type your message here...'),(264,'es','Escriba su mensaje aquí ...'),(264,'nl','Typ hier je bericht ...'),(264,'ru','Введите Ваше сообщение здесь...'),(265,'de','Jetzt'),(265,'en','Just now'),(265,'es','En este momento'),(265,'nl','Juist nu'),(265,'ru','Только что'),(266,'de','Vor einem Jahr'),(266,'en','{One} year ago'),(266,'es','Hace {One} año'),(266,'nl','{One} jaar geleden'),(266,'ru','{One} год назад'),(267,'de','Vor {Many} Jahren'),(267,'en','{Many} years ago'),(267,'es','Hace {Many} años'),(267,'nl','{Many} jaren geleden'),(267,'ru','{Many} лет назад'),(268,'de','Vor einem Monat'),(268,'en','{One} month ago'),(268,'es','Hace {One} mes'),(268,'nl','{One} maand geleden'),(268,'ru','{One} месяц назад'),(269,'de','Vor {Many} Monaten'),(269,'en','{Many} months ago'),(269,'es','Hace {Many} meses'),(269,'nl','{Many} maanden geleden'),(269,'ru','{Many} месяца(-ев) назад'),(270,'de','Vor einem Tag'),(270,'en','{One} day ago'),(270,'es','Hace {One} day'),(270,'nl','{One} dag geleden'),(270,'ru','{One} день назад'),(271,'de','Vor {Many} Tagen'),(271,'en','{Many} days ago'),(271,'es','Hace {Many} dias'),(271,'nl','{Many} dagen geleden'),(271,'ru','{Many} дня(-ей) назад'),(272,'de','Vor einer Stunde'),(272,'en','{One} hour ago'),(272,'es','Hace {One} hora'),(272,'nl','{One} uur geleden'),(272,'ru','{One} час назад'),(273,'de','Vor {Many} Stunden'),(273,'en','{Many} hours ago'),(273,'es','Hace {Many} horas'),(273,'nl','{Many} rren geleden'),(273,'ru','{Many} часа(-ов) назад'),(274,'de','Vor einer Minute'),(274,'en','{One} minute ago'),(274,'es','Hace {One} minuto'),(274,'nl','{One} minuut geleden'),(274,'ru','{One} минуту назад'),(275,'de','Vor {Many} Minuten'),(275,'en','{Many} minutes ago'),(275,'es','Hace {Many} minutos'),(275,'nl','{Many} minuten geleden'),(275,'ru','{Many} минут(-ы) назад'),(276,'de','Blockiert am'),(276,'en','Blocked at'),(276,'es','Bloqueado en'),(276,'nl','Geblokkeerde bij'),(276,'ru','Дата блокировки'),(277,'de','Blockierung aufheben'),(277,'en','Unblock'),(277,'es','Desbloquear'),(277,'nl','Deblokkeren'),(277,'ru','Разблокировать'),(278,'de','Nutzer wurde Blockiert'),(278,'en','User has been blocked'),(278,'es','El usuario ha sido bloqueado'),(278,'nl','Gebruiker werd geblokkeerd'),(278,'ru','Пользователь был заблокирован'),(279,'de','Nutzerblockade wurde aufgehoben'),(279,'en','User has been unblocked'),(279,'es','El usuario ha sido desbloqueado'),(279,'nl','Gebruiker is gedeblokkeerd'),(279,'ru','Пользователь был разблокирован'),(280,'de','Nutzer wurde Blockiert und als Betrüger markiert'),(280,'en','User has been blocked and marked as a spammer'),(280,'es','El usuario ha sido bloqueado y marcado como un spammer'),(280,'nl','Gebruiker werd geblokkeerd en gemarkeerd als een spammer'),(280,'ru','Пользователь был помечен как мошенник'),(281,'de','Nachrichten wurden wieder hergestellt'),(281,'en','Messages have been restored'),(281,'es','Los mensajes han sido restaurados'),(281,'nl','Berichten zijn terug gezet'),(281,'ru','Сообщения были востановлены'),(282,'de','Nachrichten wurden komplett entfernt'),(282,'en','Messages have been completely removed'),(282,'es','Los mensajes se han eliminado'),(282,'nl','Berichten zijn volledig verwijderd'),(282,'ru','Сообщения были полностью удалены'),(283,'de','Ausgewählte Nachrichten wurden in den Ordner für gelöschte Nachrichten verschoben'),(283,'en','Selected messages have been moved to trash folder'),(283,'es','Los mensajes seleccionados han sido movidos a la Papelera'),(283,'nl','Geselecteerde berichten zijn verplaatst naar Prullenbak'),(283,'ru','Выбранные сообщения были перемещены в папку Удаленные'),(284,'de','Ausgewählte Nachrichten wurden als gelesen markiert'),(284,'en','Selected messages have been marked as read'),(284,'es','Los mensajes seleccionados se han marcado como leído'),(284,'nl','Geselecteerde berichten zijn gemarkeerd als gelezen'),(284,'ru','Выбранные сообщения были помечены как Прочитанные'),(285,'de','Ausgewählte Nachrichten wurden nach Favoriten verschoben'),(285,'en','Selected messages have been moved to Starred folder'),(285,'es','Los mensajes seleccionados se han movido a la carpeta Destacados'),(285,'nl','Geselecteerde berichten zijn verplaatst naar de map met ster'),(285,'ru','Выбранные сообщения были перемещены в папку Избранные'),(286,'de','Ausgewählte Nachrichten wurden aus dem Favoritenordner entfernt'),(286,'en','Selected messages have been removed from Starred folder'),(286,'es','Los mensajes seleccionados se han eliminado de la carpeta Destacados'),(286,'nl','Geselecteerde berichten zijn verwijderd uit de map met ster'),(286,'ru','Выбранные сообщения были удалены из папки Избронные'),(287,'de','Ausgewählte Nachrichten wurden aus dem für wichtige Nachrichtenordner entfernt'),(287,'en','Selected messages have been removed from Important folder'),(287,'es','Los mensajes seleccionados se han eliminado de la carpeta Importante'),(287,'nl',''),(287,'ru','Выбранные сообщения были удалены из папки Важные'),(288,'de','Ausgewählte Nachrichten wurden als wichtig markiert'),(288,'en','Selected messages has been marked as Important'),(288,'es','Los mensajes seleccionados se han marcado como Importante'),(288,'nl','Geselecteerde berichten is gemarkeerd als belangrijk'),(288,'ru','Выбранные сообщения были помечены как Важные'),(289,'de','Passwort wurde geändert'),(289,'en','Password has been changed'),(289,'es','La contraseña ha sido cambiada'),(289,'nl','Wachtwoord is gewijzigd'),(289,'ru','Пароль был изменен'),(290,'de','Profileinstellungen'),(290,'en','Profile Settings'),(290,'es','Configuración del perfil'),(290,'nl','Profiel instellingen'),(290,'ru','Настройки профайла'),(291,'de','nderen Sie das Passwort Format'),(291,'en','Change Password Form'),(291,'es','Formulario de Cambio de Contraseña'),(291,'nl','Verander password formulier'),(291,'ru','Форма для смены пароля'),(292,'de','Altes Passwort'),(292,'en','Old password'),(292,'es','Contraseña anterior'),(292,'nl','Oud wachtwoord'),(292,'ru','Старый пароль'),(293,'de','Altes Passwort falsch'),(293,'en','Incorrect old password'),(293,'es','Contraseña antigua incorrecta'),(293,'nl','Onjuist oud wachtwoord'),(293,'ru','Неверный старый пароль'),(294,'de','Nutzer kann in Datenbank nicht gefunden werden'),(294,'en','Unable to find user in database'),(294,'es','No se ha podido encontrar el usuario en la base de datos'),(294,'nl','Gebruiker is niet te vinden in database'),(294,'ru','Невозможно найти пользователя в базе данных'),(295,'de','Sie nutzen Administratorenrechte oberhalb {Username}'),(295,'en','You are using administration rights over {Username}'),(295,'es','Está utilizando derechos administrativos sobre {username}'),(295,'nl','U maakt gebruik van administratie rechten op {Username}'),(295,'ru','Вы управляете данными пользователя {Username}'),(296,'de','Start'),(296,'en','Home'),(296,'es','Inicio'),(296,'nl','Start'),(296,'ru','Главная'),(297,'de','Top Seiten'),(297,'en','Top'),(297,'es','Arriba'),(297,'nl','Top'),(297,'ru','Лучшие'),(298,'de','Zuletzt geschätzt'),(298,'en','Upcoming'),(298,'es','Próxima'),(298,'nl','Aankomende'),(298,'ru','Новые'),(299,'de','Webseiten verkaufen'),(299,'en','Sell Websites'),(299,'es','Venta de Proyectos'),(299,'nl','Verkoop Websites'),(299,'ru','Продать веб-сайты'),(300,'de','Anmelden'),(300,'en','Sign in'),(300,'es','Registrarse'),(300,'nl','Aanmelden'),(300,'ru','Войти'),(301,'de','Posteingang'),(301,'en','Inbox'),(301,'es','Bandeja de entrada'),(301,'nl','Inbox'),(301,'ru','Входящие'),(302,'de','{NumberOfMsg} Nachrichten'),(302,'en','{NumberOfMsg} Messages'),(302,'es','{NumberOfMsg} Mensajes'),(302,'nl','{NumberOfMsg} Berichten'),(302,'ru','{NumberOfMsg} сообщение(-ий)'),(303,'de','Alle Nachrichten anzeigen'),(303,'en','See all messages'),(303,'es','Ver todos los mensajes'),(303,'nl','Bekijk alle berichten'),(303,'ru','Все сообщения'),(304,'de','Zum Verkauf'),(304,'en','On sale'),(304,'es','En venta'),(304,'nl','In de verkoop'),(304,'ru','Продается'),(305,'de','Hallo, {Username}'),(305,'en','Hello, {Username}'),(305,'es','Hola, {Username}'),(305,'nl','Hallo, {Username}'),(305,'ru','Привет, {Username}'),(306,'de','Einstellungen'),(306,'en','Settings'),(306,'es','Ajustes'),(306,'nl','Instellingen'),(306,'ru','Настройки'),(307,'de','Abmelden'),(307,'en','Logout'),(307,'es','Salir'),(307,'nl','Afmelden'),(307,'ru','Выход'),(308,'de','Erstellt von {Developer}'),(308,'en','Developed by {Developer}'),(308,'es','Desarrollado por {Developer}'),(308,'nl','Ontwikkeld door {Developer}'),(308,'ru','Разработал {Developer}'),(309,'de','Sprache erstellen'),(309,'en','Create Language'),(309,'es','Crear Idioma'),(309,'nl','Maak een taal'),(309,'ru','Создать язык'),(310,'de','Übersetzungen verwalten'),(310,'en','Manage Translations'),(310,'es','Gestionar Traducciones'),(310,'nl','Beheer vertalingen'),(310,'ru','Управление переводами'),(311,'de','Sprache exportieren'),(311,'en','Export Translation'),(311,'es','Exportar Traducción'),(311,'nl','Exporteer vertaling'),(311,'ru','Экспортировать перевод'),(312,'de','Übersetzung importieren'),(312,'en','Import Translation'),(312,'es','Importar Traucción'),(312,'nl','Importeer vertaling'),(312,'ru','Импотировать перевод'),(313,'de','Strach-ID'),(313,'en','Language ID'),(313,'es','Idioma ID'),(313,'nl','Taal ID'),(313,'ru','Идентификатор языка'),(314,'de','Sprache'),(314,'en','Language'),(314,'es','Idioma'),(314,'nl','Taal'),(314,'ru','Язык'),(315,'de','Ist aktiviert'),(315,'en','Is Enabled'),(315,'es','Está habilitado'),(315,'nl','Is ingeschakeld'),(315,'ru','Включен'),(316,'de','Ist voreingestellt'),(316,'en','Is Default'),(316,'es','Es por Default'),(316,'nl','Is standaard'),(316,'ru','По умолчанию'),(317,'de','Erstellt am'),(317,'en','Created at'),(317,'es','Creado el'),(317,'nl','Gemaakt op'),(317,'ru','Создан'),(318,'de','Ja'),(318,'en','Yes'),(318,'es','Sí'),(318,'nl','Ja'),(318,'ru','Да'),(319,'de','Nein'),(319,'en','No'),(319,'es','No'),(319,'nl','Nee'),(319,'ru','Нет'),(320,'de','Löschen'),(320,'en','Delete'),(320,'es','Eliminar'),(320,'nl','Delete'),(320,'ru','Удалить'),(321,'de','Neue Sprache erstellen'),(321,'en','Create new Language'),(321,'es','Crear nuevo Idioma'),(321,'nl','Maak een nieuwe taal'),(321,'ru','Создать новый язык'),(322,'de','Übersetzungen nicht kopieren'),(322,'en','Do not copy translations'),(322,'es','No copie traducciones'),(322,'nl','Kopiër de vertalingen niet'),(322,'ru','Не копировать переводы'),(323,'de','Neue Sprache - {Language} - wurde erstellt'),(323,'en','New language {Language} has been created'),(323,'es','Nuevo idioma {Language} se ha creado'),(323,'nl','Nieuwe taal {Language} is gemaakt'),(323,'ru','Был создан новый язык {Language}'),(324,'de','Kopie erstellen'),(324,'en','Make copy'),(324,'es','Hacer copia'),(324,'nl','Maak kopie'),(324,'ru','Сделать копию'),(325,'de','Falsches Sprach-ID Format'),(325,'en','Incorrect Language ID format'),(325,'es','Formato ID de Idioma Incorrecto'),(325,'nl','Onjuiste taal ID formaat'),(325,'ru','Неверно задан идентификатор языка'),(326,'de','Die Sprache existiert noch nicht'),(326,'en','The language doesn\'t exists'),(326,'es','El idioma no existe'),(326,'nl','Deze taal bestaat niet'),(326,'ru','Язык не существует'),(327,'de','Voreingestellte Sprache kann nicht deaktiviert werden'),(327,'en','Default language can\'t be disabled'),(327,'es','Idioma por Default no puede desactivarse'),(327,'nl','Standaardtaal kan niet worden uitgeschakeld'),(327,'ru','Язык, выбранный по умолчанию, не может быть отключен'),(328,'de','Voreingestellte Sprache muss spezifiziert werden'),(328,'en','Default language must be specified'),(328,'es','Idioma por Default se debe especificar'),(328,'nl','Standaard taal moet worden opgegeven'),(328,'ru','Язык по умолчанию должен быть определен'),(329,'de','{Language} bearbeiten'),(329,'en','Edit {Language} language'),(329,'es','Editar Idioma {Language}'),(329,'nl','Bewerk {Language} taal'),(329,'ru','Редактировать {Language} язык'),(330,'de','Eintrag wurde erfolgreich überarbeitet'),(330,'en','Record has been successfully modified'),(330,'es','El registro ha sido modificado con éxito'),(330,'nl','Record is met succes gewijzigd'),(330,'ru','Запись была успешно изменина'),(331,'de','Voreingestellte Sprache kann nicht gelöscht werden'),(331,'en','Default language can\'t be deleted'),(331,'es','Idioma por Default no se puede eliminar'),(331,'nl','Standaard taal kan niet worden verwijderd'),(331,'ru','Язык, выбранный по умолчанию, не может быть удален'),(332,'de','Eintrag wurde gelöscht'),(332,'en','Record has been deleted'),(332,'es','El registro ha sido borrado'),(332,'nl','Record is verwijderd'),(332,'ru','Запись была удалена'),(333,'de','Suche Phrase/Übersetzung'),(333,'en','Find phrase/translation'),(333,'es','Encontrar frase/traducción'),(333,'nl','Zoek de zin/vertaling'),(333,'ru','Найти фразу/перевод'),(334,'de','Fehlende Übersetzungen'),(334,'en','Missing translations'),(334,'es','Traducciones faltantes'),(334,'nl','Ontbrekende vertalingen'),(334,'ru','Ненайденные переводы'),(335,'de','Phrase erstellen'),(335,'en','Create phrase'),(335,'es','Crear frase'),(335,'nl','Maak zin'),(335,'ru','Создать фразу'),(336,'de','Kategoriename'),(336,'en','Category name'),(336,'es','Nombre de Categoría'),(336,'nl','Categorie naam'),(336,'ru','Имя категории'),(337,'de','Ungültiger Kategoriename'),(337,'en','Invalid category name'),(337,'es','Nombre de la categoría inválido'),(337,'nl','Ongeldige categorie naam'),(337,'ru','Неверно задано имя категории'),(338,'de','Die Kategorie {Category} ist bereits vorhanden'),(338,'en','The category {Category} has already been taken'),(338,'es','La categoría {Category} ya se ha tomado'),(338,'nl',''),(338,'ru','Категория с именем {Category} уже существует'),(339,'de','Sprachen in Kategorie {Category} verwalten'),(339,'en','Manage translations in {Category} category'),(339,'es','Gestionar traducciones en categoría {Category}'),(339,'nl','Beheer vertalingen in {Category} categorie'),(339,'ru','Управление переводами в {Category} категории'),(340,'de','Phrase'),(340,'en','Phrase'),(340,'es','Frase'),(340,'nl','Zin'),(340,'ru','Фраза'),(341,'de','Neue Phrase erstellen'),(341,'en','Create new phrase'),(341,'es','Crear nueva frase'),(341,'nl','Maak een nieuwe zin'),(341,'ru','Создать новую фразу'),(342,'de','Eine beliebige Kategorie wählen'),(342,'en','Choose any category'),(342,'es','Seleccione cualquier Categoría'),(342,'nl','Kies een categorie'),(342,'ru','Выберите любую категорию'),(343,'de','Neue Phrase wurde erstellt'),(343,'en','New phrase has been created'),(343,'es','Nueva frase ha sido creado'),(343,'nl','Nieuwe zin is gemaakt'),(343,'ru','Была создана новая фраза'),(344,'de','Diese Phrase ist bereits in Kategorie {Category} vorhanden'),(344,'en','This phrase already exists in {Category} category'),(344,'es','Esta frase ya existe en categoría {Category}'),(344,'nl','Deze zin bestaat al in {categorie} categorie'),(344,'ru','Эта фраза уже существует в категории {Category}'),(345,'de','Erstellen'),(345,'en','Create'),(345,'es','Crear'),(345,'nl','Maak'),(345,'ru','Создать'),(346,'de','ODER'),(346,'en','OR'),(346,'es','O'),(346,'nl','OF'),(346,'ru','ИЛИ'),(347,'de','Neue Kategorie'),(347,'en','New category'),(347,'es','Nueva categoría'),(347,'nl','Nieuwe categorie'),(347,'ru','Новая категория'),(350,'de','Phrase übersetzen'),(350,'en','Translate phrase'),(350,'es','Traducir frase'),(350,'nl','Vertaal zin'),(350,'ru','Перевести фразу'),(351,'de','Phrase bearbeiten'),(351,'en','Edit phrase'),(351,'es','Editar frase'),(351,'nl','Zin bewerken'),(351,'ru','Редактировать фразу'),(352,'de','Phrase wurde übersetzt'),(352,'en','Phrase has been translated'),(352,'es','Frase ha sido traducido'),(352,'nl','Zin is vertaald'),(352,'ru','Фраза была переведена'),(353,'de','Eine beliebige Sprache wählen'),(353,'en','Choose any language'),(353,'es','Seleccione cualquier Idioma'),(353,'nl','Kies een taal'),(353,'ru','Выберите любой язык'),(354,'de','Die Phrase mit der ID {ID} existiert noch nicht'),(354,'en','The phrase with ID {ID} doesn\'t exists'),(354,'es','La frase con ID {ID} no existe'),(354,'nl','De zin met id {ID} bestaat niet'),(354,'ru','Фраза с идентификатором {ID} не существует'),(355,'de','Übersetzung'),(355,'en','Translation'),(355,'es','Traducción'),(355,'nl','Vertaling'),(355,'ru','Перевод'),(356,'de','Vorhandene Übersetzungen'),(356,'en','Existing translations'),(356,'es','Traducciones existentes'),(356,'nl','Bestaande vertalingen'),(356,'ru','Существующие переводы'),(357,'de','Fehlende übersetzung für {Langage}'),(357,'en','Missing translation for {Language}'),(357,'es','Trandución faltante para {Language}'),(357,'nl','Ontbrekende vertaling voor {Language}'),(357,'ru','Ненайден перевод для {Language}'),(358,'de','Suche'),(358,'en','Search'),(358,'es','Búsqueda'),(358,'nl','Zoek'),(358,'ru','Поиск'),(359,'de','Sprachen verwalten'),(359,'en','Manage Languages'),(359,'es','Gestionar Idiomas'),(359,'nl','Beheer talen'),(359,'ru','Управление языками'),(360,'de','Alle Einträge entfernen'),(360,'en','Remove all records'),(360,'es','Retire todos los registros'),(360,'nl','Verwijder alle records'),(360,'ru','Удалить все записи'),(361,'de','Alle Einträge wurden entfernt'),(361,'en','All records have been removed'),(361,'es','Todos los registros se han eliminado'),(361,'nl','Alle records zijn verwijderd'),(361,'ru','Все записи были удалены'),(362,'de','Sie müssen ein .zip Archiv installieren'),(362,'en','You need to install ZipArchive package'),(362,'es','Necesita instalar el paquete del archivo ZIP'),(362,'nl','U moet ZipArchive pakket installeren'),(362,'ru','Вам нужно установить ZipArchive пакет'),(363,'de','Temporäre Datei kann nicht erstellt werden'),(363,'en','Can\'t create temporary file'),(363,'es','No se puede crear el archivo temporal'),(363,'nl','Kan tijdelijk bestand niet maken'),(363,'ru','Не могу создать временный файл'),(364,'de','Für diese Sprache sollte zumindest eine Kategorie vorhanden sein'),(364,'en','The language must contain at least one category'),(364,'es','El idioma debe contener al menos una categoría'),(364,'nl','De taal moet minstens een categorie bevatten'),(364,'ru','Язык должен содержать хотя бы одну категорию'),(365,'de','Temporäre Datei kann nicht geschlossen werden'),(365,'en','Can\'t close temporary file'),(365,'es','No se puede cerrar el archivo temporal'),(365,'nl','Kan tijdelijk bestand niet sluiten'),(365,'ru','Не могу закрыть временный файл'),(366,'de','Bitte wählen Sie eine Sprache zum Exportieren aus'),(366,'en','Please choose language you want to export'),(366,'es','Por favor, seleccione el idioma que desea exportar'),(366,'nl','Kies een taal die u wilt exporteren'),(366,'ru','Выберите язык который хотите экспортировать'),(367,'de','Exportieren'),(367,'en','Export'),(367,'es','Exportar'),(367,'nl','Exporteer'),(367,'ru','Экспортировать'),(368,'de','Importieren'),(368,'en','Import'),(368,'es','Importar'),(368,'nl','Importeer'),(368,'ru','Импотировать'),(369,'de','Beliebige Sprache'),(369,'en','Any language'),(369,'es','Cualquier idioma'),(369,'nl','Willekeurige taal'),(369,'ru','Любой язык'),(370,'de','Beliebige Kategorie'),(370,'en','Any category'),(370,'es','Cualquier categoría'),(370,'nl','Willekeurige categorie'),(370,'ru','Любая категория'),(371,'de','Kann .zip Archiv nicht öffnen'),(371,'en','Couldn\'t open zip archive'),(371,'es','No se pudo abrir archivo ZIP'),(371,'nl','Kon zip bestand niet openen'),(371,'ru','Не могу открыть ZIP архив'),(372,'de','Datei {File} wurde nicht angenommen, da keine .csv Datei'),(372,'en','Ignoring file {File} because it\'s not a csv file'),(372,'es','Archivo {file} Ignorado, porque no es un archivo CSV'),(372,'nl','Negeer het bestand {File} want het is geen csv-bestand'),(372,'ru','Игнорирую файл {File}, потому что это не csv'),(373,'de','Keine Verbindung zur Datei {File} möglich'),(373,'en','Unable to create stream from {File} file'),(373,'es','No se puede crear la corriente de  archivo {File}'),(373,'nl','Niet in staat om bestand {File} uit te pakken'),(373,'ru','Не возможно создать потом файла {File}'),(374,'de','Ungültiges Format für {Row} Reihe in {Category} Kategorie'),(374,'en','Invalid format of {Row} row in {Category} category'),(374,'es','Formato inválido de {Row} fila en categoría {Category}'),(374,'nl','Ongeldig formaat van {Row} rij {Category} categorie'),(374,'ru','Неверный формат строки {Row} в категории {Category}'),(375,'de','Übersetzung für die Sprache - {Language} - wurden importiert'),(375,'en','Translations for {Language} language have been imported'),(375,'es','Traducciones del Idioma {Language} se han importado'),(375,'nl','Vertalingen voor {Language} taal zijn geimporteert'),(375,'ru','Перевод языка {Language} был импортирован'),(376,'de','Sie können nur .zip Dateien hochladen'),(376,'en','You are able upload only ZIP file'),(376,'es','Usted está habilitando  sólo la subida de archivos ZIP'),(376,'nl','Je kan alleen ZIP bestanden uploaden'),(376,'ru','Во можете загружать только ZIP файлы'),(377,'de','Zugewiesener Import'),(377,'en','Forced import'),(377,'es','Forzar importar'),(377,'nl','Geforceerde import'),(377,'ru','Принудительное импортирование'),(378,'de','.zip Datei (Übersetzungen)'),(378,'en','Zip file (translations)'),(378,'es','Archivo ZIP (traducciones)'),(378,'nl','Zip file (vertaling)'),(378,'ru','Zip file (translations)'),(379,'de','Dies bedeutet, dass wenn eine Phrase übersetzt ist, wird diese ersetzt'),(379,'en','This means that if there is a phrase translation it will be replaced'),(379,'es','Esto significa que si hay una traducción de la frase será reemplazado'),(379,'nl','Dit betekent dat als er een zin vertaling bestaat dan zal deze wordenvervangen'),(379,'ru','Означает что если фраза уже переведена для данного языка то она будет заменена, а не проигнорирована'),(380,'de','Nutzer verwalten'),(380,'en','Manage Users'),(380,'es','Administrar usuarios'),(380,'nl','Gebruikers beheren'),(380,'ru','Управление пользователями'),(381,'de','Nutzer Information'),(381,'en','User info'),(381,'es','Información del usuario'),(381,'nl','Gebruikers info'),(381,'ru','Информация пользователя'),(382,'de','Bearbeite Nutzerdaten'),(382,'en','Edit user data'),(382,'es','Editar datos de usuario'),(382,'nl','Gebruikersgegevens bewerken'),(382,'ru','Редактировать данные пользователя'),(383,'de','Lösche Nutzer'),(383,'en','Delete user'),(383,'es','Eliminar  usuario'),(383,'nl','Wis gebruiker'),(383,'ru','Удалить пользователя'),(384,'de','Erstelle Nutzer'),(384,'en','Create User'),(384,'es','Crear usuario'),(384,'nl','Gebruiker aanmaken'),(384,'ru','Создать пользователя'),(385,'de','Bestätigte Email'),(385,'en','Confirmed Email'),(385,'es','Correo electrónico confirmado'),(385,'nl','Bevestigde E-mail'),(385,'ru','Email Подтвержден'),(386,'de','Erlaube das Senden von Nachrichten'),(386,'en','Allow send messages'),(386,'es','Permitir enviar mensajes'),(386,'nl','Toe staan om berichten sturen'),(386,'ru','Разрешить отправлять сообщения'),(387,'de','Nutzer wurde erstellt'),(387,'en','User has been created'),(387,'es','El usuario ha sido creado'),(387,'nl','Gebruiker is aangemaakt'),(387,'ru','Пользователь был создан'),(388,'de','Passwort zurücksetzen'),(388,'en','Reset password'),(388,'es','Restablecer contraseña'),(388,'nl','Reset wachtwoord'),(388,'ru','Сбросить пароль'),(389,'de','Nutzer Posteingang'),(389,'en','User inbox'),(389,'es','Bandeja de entrada del usuario'),(389,'nl','Gebruikers inbox'),(389,'ru','Почта пользователя'),(390,'de','Nutzer hat {CountOnSale} Webseit/en zum Verkauf'),(390,'en','User has {CountOnSale} website(-s) on sale'),(390,'es','El usuario tiene {CountOnSale}  Sitio(s) Web en venta'),(390,'nl','Gebruiker heeft {CountOnSale} website (-s) in de verkoop'),(390,'ru','Пользователь имеет {CountOnSale} веб-сайта(-ов) на продаже'),(391,'de','Feld'),(391,'en','Field'),(391,'es','Campo'),(391,'nl','Veld'),(391,'ru','Поле'),(392,'de','Wert'),(392,'en','Value'),(392,'es','Valor'),(392,'nl','Waarde'),(392,'ru','Значение'),(393,'de','Registriert mit IP'),(393,'en','Registered IP'),(393,'es','IP registrada'),(393,'nl','Geregistreerde met IP'),(393,'ru','IP регистрации'),(394,'de','Nutzer wurde gelöscht'),(394,'en','User has been deleted'),(394,'es','El usuario ha sido borrado'),(394,'nl','Gebruiker is aangemaakt'),(394,'ru','Пользователь был удален'),(395,'de','Sie haben die Anmeldezeit überschritten und wurden automatisch vom System getrennt.'),(395,'en','You have been disconnected from the session'),(395,'es','Usted ha sido desconectado de la sesión'),(395,'nl','Je contact met de sessie vebroken'),(395,'ru','Вы были отключены от сесии'),(396,'de','Bitte anmelden'),(396,'en','Please Sign In'),(396,'es','Por favor Entre'),(396,'nl','U kunt hier inloggen'),(396,'ru','Пожалуйста авторизуйтесь'),(397,'de','Kategorien verwalten'),(397,'en','Manage Categories'),(397,'es','Administrar Categorías'),(397,'nl','Manage Categories'),(397,'ru','Управление категориями'),(398,'de','Kategorie erstellen'),(398,'en','Create Category'),(398,'es','Crear Categoría'),(398,'nl','Categorie aanmaken'),(398,'ru','Создать категорию'),(399,'de','Kategorie'),(399,'en','Category'),(399,'es','Categoría'),(399,'nl','Categorie'),(399,'ru','Категория'),(400,'de','Slug'),(400,'en','Slug'),(400,'es','Slug'),(400,'nl','Slug'),(400,'ru','URL линк'),(401,'de','Kategorie wurde erstellt'),(401,'en','Category has been created'),(401,'es','La Categoría ha sido creada'),(401,'nl','Categorie is aangemaakt'),(401,'ru','Категория была создана'),(402,'de','Kategorie bearbeiten'),(402,'en','Edit category'),(402,'es','Editar Categoría'),(402,'nl','Categorie bewerken'),(402,'ru','Редактировать категорию'),(403,'de','Erstelle Übersetzung'),(403,'en','Create translation'),(403,'es','Crear traducción'),(403,'nl','Maak vertaling'),(403,'ru','Создать перевод'),(404,'de','Kategorieübersetzung'),(404,'en','Category Translation'),(404,'es','Traducción de Categoría'),(404,'nl','Categorie vertaling'),(404,'ru','Перевести категорию'),(405,'de','Übersetzung wurde erstellt'),(405,'en','Translation has been created'),(405,'es','La traducción ha sido creada'),(405,'nl','Vertaling is gemaakt'),(405,'ru','Перевод был создан'),(406,'de','Aktualisiere Übersetzung'),(406,'en','Update translation'),(406,'es','Actualizar  traducción'),(406,'nl','Update vertaling'),(406,'ru','Обновить перевод'),(407,'de','Übersetzung für diese Sprache existiert bereits. {Update translation}'),(407,'en','Translation for this language already exists. {Update translation}'),(407,'es','La traducción para este idioma ya existe. {Update translation}'),(407,'nl','Vertaling voor deze taal bestaat reeds. {Update vertaling}'),(407,'ru','Перевод для этого языка уже существует. {Update translation}'),(408,'de','Übersetzung wurde aktualisiert'),(408,'en','Translation has been updated'),(408,'es','La traducción ha sido actualizada'),(408,'nl','Vertaling is bijgewerkt'),(408,'ru','Перевод был обнавлен'),(409,'de','Übersetzung wurde gelöscht'),(409,'en','Translation has been deleted'),(409,'es','La traducción ha sido borrada'),(409,'nl','Vertaling is verwijderd'),(409,'ru','Перевод был удален'),(410,'de','Betrug Reporte'),(410,'en','Scam reports'),(410,'es','Reporte de estafa'),(410,'nl','Scam rapporten'),(410,'ru','Список скамеров'),(411,'de','Versender'),(411,'en','Sender'),(411,'es','Remitente'),(411,'nl','Afzender'),(411,'ru','Отправитель'),(412,'de','Betrüger'),(412,'en','Scammer'),(412,'es','Estafador'),(412,'nl','Scammer'),(412,'ru','Мошенник'),(413,'de','Datum der Beschwerde'),(413,'en','Complain date'),(413,'es','Fecha de queja'),(413,'nl','Datum klacht'),(413,'ru','Дата отправки'),(414,'de','Unterhaltung ansehen'),(414,'en','View dialog'),(414,'es','Ver conversación'),(414,'nl','Bekijk gesprek'),(414,'ru','Просмотреть диалог'),(416,'de','Der Dialog zwischen dem {Sender} und dem {Scammer}'),(416,'en','The dialog between {Sender} and {Scammer}'),(416,'es','La conversación entre el {Sender} y {Scammer}'),(416,'nl','Gesprek tussen {Sender} en {Scammer}'),(416,'ru','Диалог между {Sender} и {Scammer}'),(417,'de','Verhindere  {Scammer} vom Senden von Nachrichten'),(417,'en','Prevent {Scammer} from sending letters'),(417,'es','Prevenir {Scammer} de envío de cartas'),(417,'nl','Voorkom dat {Scammer} brieven kan verzenden'),(417,'ru','Запретить {Scammer} отправлять письма'),(418,'de','Legende'),(418,'en','Legend'),(418,'es','Leyenda'),(418,'nl','Log'),(418,'ru','Легенда'),(419,'de','Gesendet an'),(419,'en','Sent at'),(419,'es','Enviado a'),(419,'nl','Verzonden op'),(419,'ru','Дата отправки'),(420,'de','Nutzer wurde einggeschränkt'),(420,'en','User has been restricted'),(420,'es','El usuario ha sido restringido'),(420,'nl','Gebruiker is beperkt'),(420,'ru','Права пользователя были ограничены'),(421,'de','Webseiten Verwalten'),(421,'en','Manage Websites'),(421,'es','Gestionar Sitios Web'),(421,'nl','Beheer websites'),(421,'ru','Управление Веб-сайтами'),(422,'de','Steht zum Verkauf'),(422,'en','Is On Sale'),(422,'es','Está en venta'),(422,'nl','Is in de uitverkoop'),(422,'ru','Продается'),(423,'de','Nicht zum Verkauf'),(423,'en','Not on sale'),(423,'es','No a la venta'),(423,'nl','Niet te koop'),(423,'ru','Не продается'),(424,'de','Alles'),(424,'en','All'),(424,'es','Todos'),(424,'nl','Alle'),(424,'ru','Все'),(425,'de','Eingabe leer lassen'),(425,'en','Leave empty input'),(425,'es','Dejar entrada vacía'),(425,'nl','Laat invoer leeg'),(425,'ru','Оставьте поле пустым'),(426,'de','Webseite'),(426,'en','Website'),(426,'es','Sitio Web'),(426,'nl','Website'),(426,'ru','Веб-сайт'),(427,'de','Domain'),(427,'en','Domain'),(427,'es','Dominio'),(427,'nl','Domein'),(427,'ru','Домен'),(428,'de','Längengrad'),(428,'en','Longitude'),(428,'es','Longitud'),(428,'nl','Lengte'),(428,'ru','Долгота'),(429,'de','Breitengrad'),(429,'en','Latitude'),(429,'es','Latitud'),(429,'nl','Breedte'),(429,'ru','Широта'),(430,'de','Neukalkulation des geschätzten Preises'),(430,'en','Re-Calculate estimate price'),(430,'es','Re-calcular precio estimado'),(430,'nl','Herbereken geschatte prijs'),(430,'ru','Пересчитать оценочную стоимость'),(431,'de','Webseite vom Verkauf zurückziehen'),(431,'en','Remove website from sale'),(431,'es','Eliminar Sitio Web en venta'),(431,'nl','Haal website uit de verkoop'),(431,'ru','Убрать веб-сайт с продажи'),(432,'de','Webseiteninformation anzeigen'),(432,'en','View website info'),(432,'es','Ver información del Sitio Web'),(432,'nl','Bekijk de website info'),(432,'ru','Информация о веб-сайте'),(433,'de','Webseite entfernen'),(433,'en','Remove website'),(433,'es','Eliminar Sitio Web'),(433,'nl','Verwijder website'),(433,'ru','Удалить веб-сайт'),(434,'de','Der geschätzte Wert der Webseite wurde neu kalkluiert. {Click here} to see a result'),(434,'en','The website\'s estimated price has been recalculated. {Click here} to see a result'),(434,'es','El precio estimado de la Página Web se ha recalculado. {Click here} para ver el resultado'),(434,'nl','De geschatte prijs is herberekend van de  website . {Click here} om het resultaat te zien'),(434,'ru','Ориентировочная цена веб-сайта была пересчитана. {Click here} чтобы увидеть результат.'),(435,'de','Webseite {Website} wurde vom Verkauf entfernt'),(435,'en','Website {Website} has been removed from sale'),(435,'es','Sitio Web {Website} ha sido retirado de la venta'),(435,'nl','Website {Website} is verwijderd uit de verkoop'),(435,'ru','Веб-сайт {Website} был снят с продаж'),(436,'de','Webseite wurde gelöscht'),(436,'en','Website has been deleted'),(436,'es','El Sitio Web ha sido borrado'),(436,'nl','Website is verwijderd'),(436,'ru','Веб-сайт был удален'),(437,'de','Administration'),(437,'en','Admin panel'),(437,'es','Panel de Admnistrador'),(437,'nl','Admin panel'),(437,'ru','Панель администратора'),(438,'de','Willkommen zurück, {Username}'),(438,'en','Welcome back, {Username}'),(438,'es','Bienvenido de nuevo, {Username}'),(438,'nl','Welkom terug, {Username}'),(438,'ru','Добро пожаловать, {Username}'),(439,'de','Es ist/sind {NumOfScam} Bescherde/n wegen Betrug eingegangen. {Click here} um eine Aktion zu starten.'),(439,'en','There are {NumOfScam} complaints of fraud. {Click here} to take an action.'),(439,'es','Hay {NumOfScam}  denuncias de fraude. {Click here} para tomar una acción.'),(439,'nl','Er zijn {NumOfScam} klachten van fraude. {Klik hier} om een ​​actie te ondernemen.'),(439,'ru','Появилось {NumOfScam} жалоб(-ы) на мошенничество. {Click here} чтобы предпринять соответствующие действия.'),(440,'de','Wir haben {NumOfMissingTrans} fehlende Übersetzungen. {Click here} um mehr zu erfahren.'),(440,'en','We have {NumOfMissingTrans} missing translations. {Click here} to find out more.'),(440,'es','Nosotros tenemos {NumOfMissingTrans} traducciones faltantes. {Click here} para obtener más información.'),(440,'nl','We hebben {NumOfMissingTrans} ontbrekende vertalingen. {Klik hier} om meer te weten te komen.'),(440,'ru','Было найдено {NumOfMissingTrans} недостающих перевода. {Click here} чтобы узнать больше.'),(441,'de','Navigation verbergen'),(441,'en','Toggle navigation'),(441,'es','Navegación alternativa'),(441,'nl','Navigatie'),(441,'ru','Переключить навигацию'),(442,'de','Dashboard'),(442,'en','Dashboard'),(442,'es','Portada'),(442,'nl','Dashboard'),(442,'ru','Главная'),(443,'de','Lokalisierung'),(443,'en','Localization'),(443,'es','Localización'),(443,'nl','Lokalisatie'),(443,'ru','Локализация'),(444,'de','Werkzeuge'),(444,'en','Tools'),(444,'es','Herramientas'),(444,'nl','Gereedschap'),(444,'ru','Инструменты'),(445,'de','Sitemap erstellen'),(445,'en','Sitemap generation'),(445,'es','Generador Sitemap'),(445,'nl','Genereer sitemap'),(445,'ru','Генерация карты сайта'),(446,'de','Cache leeren'),(446,'en','Flush cache'),(446,'es','Vaciar caché'),(446,'nl','Flush cache'),(446,'ru','Очистить кэш'),(447,'de','Widgetüberprüfung starten'),(447,'en','Run widget checker'),(447,'es','Ejecutar comprobador de widget'),(447,'nl','Draai widget checker'),(447,'ru','Запустить проверку виджетов'),(448,'de','Müllsammler starten'),(448,'en','Run garbage collector'),(448,'es','Ejecutar recolector de basura'),(448,'nl','Draai troep verzamelaar'),(448,'ru','Запустить сборщика мусора'),(449,'de','Seite kann nicht erreicht werden: {Host}'),(449,'en','Could not reach host: {Host}'),(449,'es','No se pudo alcanzar el host: {Host}'),(449,'nl','Kon host niet bereiken: {Host}'),(449,'ru','Не удалось достичь хост: {Host}'),(450,'de','HTML konnte nicht von Webseite übernommen werden'),(450,'en','Couldn\'t grab html from website'),(450,'es','No se pudo tomar HTML de la página web'),(450,'nl','Kon de html code niet van de website pakken'),(450,'ru','Не получилось скачать HTML с веб-сайта'),(451,'de','Webseite beinhaltet unlesbare Wörter'),(451,'en','Website contains badwords'),(451,'es','Sitio web contiene malas palabras'),(451,'nl','Website bevat foute woorden'),(451,'ru','Веб-сайт содержит запрещенные слова'),(452,'de','Fehler beim Einlesen der Daten'),(452,'en','Error while inserting data'),(452,'es','Error al insertar datos'),(452,'nl','Error tijdens data invoer'),(452,'ru','Произошла ошибка во время вставки данных'),(453,'de','Fehler beim Aktualisieren der Daten'),(453,'en','Error while updating data'),(453,'es','Error al actualizar los datos'),(453,'nl','Error terwijl de data werd geupdate'),(453,'ru','Произошла ошибка во время обновления данных'),(454,'de','Sitemap wurde erstellt'),(454,'en','Sitemap has been generated'),(454,'es','Mapa del sitio ha sido generado'),(454,'nl','Sitgemap is gemaakt'),(454,'ru','Карта сайта была сгенерирована'),(455,'de','Stellen Sie sicher, dass \"sitemap\" Pfad und die \"sitemap.xml\" Datei Schreibrechte besitzen.'),(455,'en','Make sure \"sitemap\" directory and \"sitemap.xml\" file has writable permissions'),(455,'es','Asegúrese de que el directorio \"mapa del sitio\" y el archivo \"sitemap.xml\" tiene permisos de escritura'),(455,'nl','Wees er zeker van dat de directory \"sitemap\" en het bestand sitemap.xml scrijfbaar zijn'),(455,'ru','Убедитесь что директория \"sitemap\" и файл \"sitemap.xml\" доступны для записи'),(456,'de','Cache wurde bereinigt'),(456,'en','Cache has been cleared'),(456,'es','La Caché se ha borrado'),(456,'nl','Cache is gewist'),(456,'ru','Кэш был очистен'),(457,'de','Alle Webseiten die zum Verkauf stehen wurden überprüft'),(457,'en','All websites that are on sale have been checked'),(457,'es','Todos los Sitios Web que están a la venta han sido verificados'),(457,'nl','Alle websites die te koop zijn gecontroleerd'),(457,'ru','Все веб-сайты, которые находятся на продаже были проверены'),(458,'de','Abgelaufene Daten wurden bereinigt'),(458,'en','Expired data has been cleared'),(458,'es','Datos caducados se han borrado'),(458,'nl','Verlopen gegevens zijn gewist'),(458,'ru','Истекшие данные были удалены'),(459,'de','Falsche Email oder Passwort'),(459,'en','Invalid email or password'),(459,'es','Correo electrónico o contraseña no válidos'),(459,'nl','Ongeldig email of wachtwoord'),(459,'ru','Неправильный эл. адрес или пароль'),(460,'de','Bestätigungsemail wurde versendet an Sie versendet. Bitte bestätigen Sie Ihre Anmeldung'),(460,'en','We have sent you confirmation email. Please confirm it'),(460,'es','Hemos enviado una confirmación de correo electrónico. Por favor, confirme ésta'),(460,'nl','Wij hebben u ​​een bevestiging e-mail gestuurd. Bevestig het a.u.b'),(460,'ru','Мы отправили Вам письмо с подтверждением. Пожалуйста проверьте почту'),(461,'de','Nutzer wurde blockiert oder gelöscht'),(461,'en','User has been blocked or deleted'),(461,'es','El usuario ha sido bloqueado o eliminado'),(461,'nl','Gebruiker werd geblokkeerd of verwijderd'),(461,'ru','Пользователь был заблокирован'),(462,'de','Seite wurde nicht gefunden'),(462,'en','Page not found'),(462,'es','Página no encontrada'),(462,'nl','Pagina niet gevonden'),(462,'ru','Страница не найдена'),(463,'de','Interner Serverfehler'),(463,'en','Internal server error'),(463,'es','Error Interno del Servidor'),(463,'nl','Interne server error'),(463,'ru','Внутренняя ошибка сервера'),(464,'de','Zugang nicht erlaubt'),(464,'en','Access denied'),(464,'es','Acceso denegado'),(464,'nl','Toegang geweigerd'),(464,'ru','Доступ запрещен'),(465,'de','Aktion \"{action}\" existiert nicht in \"{controller}\".'),(465,'en','Action \"{action}\" does not exist in \"{controller}\".'),(465,'es','Acción \"{ation}\" no existe en \"{controller}\".'),(465,'nl','Actie \"{action}\" bestaat niet in \"{controller}\".'),(465,'ru','\"{action}\" действие не найдено в контроллере  \"{controller}\".'),(466,'de','Tägliche Werbeeinnahmen'),(466,'en','Daily Ads Revenue'),(466,'es','Ingresos diarios de Anuncios'),(466,'nl','Dagelijkse advertentie verdiensten'),(466,'ru','Ежедневный доход с рекламы'),(467,'de','Tägliche Seitenansichten'),(467,'en','Daily Pageviews'),(467,'es','Vista de páginas diarias'),(467,'nl','Dagelijkse pagina bezoeken'),(467,'ru','Ежедневный просмотр страниц'),(468,'de','Tägliche eindeutige Besucher'),(468,'en','Daily Unique Visitors'),(468,'es','Visitantes únicos al día'),(468,'nl','Dagelijks unieke bezoekers'),(468,'ru','Ежедневные уникальные посетители'),(469,'de','Geschätzte tägliche Statistik'),(469,'en','Estimated Daily Stats'),(469,'es','Estadísticas Diarias estimadas'),(469,'nl','Geschatte dagelijkse statistieken'),(469,'ru','Предполагаемая дневная статистика'),(470,'de','Geschätzte tägliche Analyse'),(470,'en','Estimated Data Analytics'),(470,'es','Estimación analítica de datos'),(470,'nl','Geschatte data gegevens'),(470,'ru','Предполагаемые аналитические данные'),(471,'de','Geschätzte monatliche Statistik'),(471,'en','Estimated Monthly Stats'),(471,'es','Estimación de estadísticas mensuales'),(471,'nl','Geschatte maand statistieken'),(471,'ru','Предполагаемая месячная статистика'),(472,'de','Geschätzte jährliche Statistik'),(472,'en','Estimated Yearly Stats'),(472,'es','Estadísticas anuales estimadas'),(472,'nl','Geschatte jaarlijkse statistieken'),(472,'ru','Предполагаемая годовая статистика'),(473,'de','Monatliche Werbeeinnahmen'),(473,'en','Monthly Ads Revenue'),(473,'es','Ingresos de Anuncios Mensuales'),(473,'nl','Maandelijkse advertenties inkomsten'),(473,'ru','Ежемесячный доход с рекламы'),(474,'de','Monatliche Seitenansichten'),(474,'en','Monthly Pageviews'),(474,'es','Páginas vistas mensuales'),(474,'nl','Maandelijkse pagina views'),(474,'ru','Ежемесячный просмотр страниц'),(475,'de','Monatliche eindeutige Besucher'),(475,'en','Monthly Unique Visitors'),(475,'es','Visitantes únicos mensuales'),(475,'nl','Unieke bezoekers per maand'),(475,'ru','Ежемесячные уникальные посетители'),(476,'de','Norton'),(476,'en','Norton'),(476,'es','Norton'),(476,'nl','Norton'),(476,'ru','Norton'),(477,'de','Jährliche Werbeeinnahmen'),(477,'en','Yearly Ads Revenue'),(477,'es','Renta de Anuncios anuales'),(477,'nl','Jaarlijkse advertenties inkomsten'),(477,'ru','Ежегодный доход с рекламы'),(478,'de','Jährliche Seitenansichten'),(478,'en','Yearly Pageviews'),(478,'es','Páginas vistas anuales'),(478,'nl','Jaarlijkse pagina views'),(478,'ru','Ежегодный просмотр страниц'),(479,'de','Jährliche eindeutige Besucher'),(479,'en','Yearly Unique Visitors'),(479,'es','Visitantes Únicos anuales'),(479,'nl','Jaarlijkse unieke bezoekers'),(479,'ru','Ежегодные уникальные посетители'),(480,'de','Tägliche Reichweite (Prozent)'),(480,'en','Daily Reach (Percent)'),(480,'es','Alcance diario (Porcentaje)'),(480,'nl','Dagelijks bereik (Procent)'),(480,'ru','Ежедневная аудитория (в процентах)'),(481,'de','Fakultativ'),(481,'en','Optional'),(481,'es','Opcional'),(481,'nl','Optioneel'),(481,'ru','Не обязателньо'),(482,'de','AVG'),(482,'en','AVG'),(482,'es','AVG'),(482,'nl','AVG'),(482,'ru','AVG'),(483,'de','RSS'),(483,'en','RSS'),(483,'es','RSS'),(483,'nl','RSS'),(483,'ru','RSS'),(484,'de','Atom'),(484,'en','Atom'),(484,'es','Atom'),(484,'nl','Atom'),(484,'ru','Atom'),(485,'de','{BrandName} | Atom-Feed'),(485,'en','{BrandName} | Atom feed'),(485,'es','{BrandName} | Atom feed'),(485,'nl','{BrandName} | Atom feed'),(485,'ru','{BrandName} | Atom feed'),(486,'de','{BrandName} | RSS feed'),(486,'en','{BrandName} | RSS feed'),(486,'es','{BrandName} | RSS feed'),(486,'nl','{BrandName} | RSS feed'),(486,'ru','{BrandName} | RSS feed');
/*!40000 ALTER TABLE `sc_trans_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_trans_missing`
--

DROP TABLE IF EXISTS `sc_trans_missing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_trans_missing` (
  `category` varchar(32) NOT NULL,
  `key` varchar(255) NOT NULL,
  `lang_id` varchar(5) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inserted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`category`,`key`,`lang_id`),
  UNIQUE KEY `ix_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_trans_missing`
--

LOCK TABLES `sc_trans_missing` WRITE;
/*!40000 ALTER TABLE `sc_trans_missing` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_trans_missing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_trans_source_message`
--

DROP TABLE IF EXISTS `sc_trans_source_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_trans_source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_category_message` (`category`,`message`)
) ENGINE=InnoDB AUTO_INCREMENT=487 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_trans_source_message`
--

LOCK TABLES `sc_trans_source_message` WRITE;
/*!40000 ALTER TABLE `sc_trans_source_message` DISABLE KEYS */;
INSERT INTO `sc_trans_source_message` VALUES (437,'admin','Admin panel'),(442,'admin','Dashboard'),(446,'admin','Flush cache'),(443,'admin','Localization'),(319,'admin','No'),(448,'admin','Run garbage collector'),(447,'admin','Run widget checker'),(445,'admin','Sitemap generation'),(439,'admin','There are {NumOfScam} complaints of fraud. {Click here} to take an action.'),(444,'admin','Tools'),(440,'admin','We have {NumOfMissingTrans} missing translations. {Click here} to find out more.'),(438,'admin','Welcome back, {Username}'),(318,'admin','Yes'),(295,'admin','You are using administration rights over {Username}'),(195,'category','Ascending order'),(399,'category','Category'),(401,'category','Category has been created'),(404,'category','Category Translation'),(398,'category','Create Category'),(403,'category','Create translation'),(196,'category','Descending order'),(402,'category','Edit category'),(198,'category','Is sold since'),(397,'category','Manage Categories'),(197,'category','Selling date'),(400,'category','Slug'),(193,'category','Sort by'),(407,'category','Translation for this language already exists. {Update translation}'),(405,'category','Translation has been created'),(409,'category','Translation has been deleted'),(408,'category','Translation has been updated'),(406,'category','Update translation'),(187,'category','Websites in the category {Category}. Page {PageNr}'),(188,'category','Websites on sale. Page {PageNr}'),(15,'contact','Body'),(18,'contact','Contact page description'),(23,'contact','Contact page keywords'),(22,'contact','Contact us'),(13,'contact','Email'),(12,'contact','Name'),(14,'contact','Subject'),(450,'error_code','Calculation Error Code 102'),(451,'error_code','Calculation Error Code 103'),(452,'error_code','Calculation Error Code 201'),(453,'error_code','Calculation Error Code 202'),(455,'error_code','Sitemap error code 101'),(370,'language','Any category'),(369,'language','Any language'),(365,'language','Can\'t close temporary file'),(363,'language','Can\'t create temporary file'),(336,'language','Category name'),(342,'language','Choose any category'),(353,'language','Choose any language'),(371,'language','Couldn\'t open zip archive'),(309,'language','Create Language'),(321,'language','Create new Language'),(341,'language','Create new phrase'),(335,'language','Create phrase'),(317,'language','Created at'),(331,'language','Default language can\'t be deleted'),(327,'language','Default language can\'t be disabled'),(328,'language','Default language must be specified'),(322,'language','Do not copy translations'),(351,'language','Edit phrase'),(329,'language','Edit {Language} language'),(356,'language','Existing translations'),(367,'language','Export'),(311,'language','Export Translation'),(333,'language','Find phrase/translation'),(377,'language','Forced import'),(372,'language','Ignoring file {File} because it\'s not a csv file'),(368,'language','Import'),(312,'language','Import Translation'),(325,'language','Incorrect Language ID format'),(337,'language','Invalid category name'),(374,'language','Invalid format of {Row} row in {Category} category'),(316,'language','Is Default'),(315,'language','Is Enabled'),(314,'language','Language'),(313,'language','Language ID'),(324,'language','Make copy'),(359,'language','Manage existing languages'),(310,'language','Manage Translations'),(339,'language','Manage translations in {Category} category'),(357,'language','Missing translation for {Language}'),(334,'language','Missing translations'),(347,'language','New category'),(323,'language','New language {Language} has been created'),(343,'language','New phrase has been created'),(340,'language','Phrase'),(352,'language','Phrase has been translated'),(366,'language','Please choose language you want to export'),(358,'language','Search'),(338,'language','The category {Category} has already been taken'),(326,'language','The language doesn\'t exists'),(364,'language','The language must contain at least one category'),(167,'language','The language {Language} doesn\'t exists in the system'),(354,'language','The phrase with ID {ID} doesn\'t exists'),(379,'language','This means that if there is a phrase translation it will be replaced'),(344,'language','This phrase already exists in {Category} category'),(350,'language','Translate phrase'),(355,'language','Translation'),(375,'language','Translations for {Language} language have been imported'),(373,'language','Unable to create stream from {File} file'),(376,'language','You are able upload only ZIP file'),(362,'language','You need to install ZipArchive package'),(378,'language','Zip file (translations)'),(172,'mail','Dear {Name}'),(174,'mail','Forgot password?'),(170,'mail','Please, do not reply to this message'),(168,'mail','Regards'),(171,'mail','Thank you for registration'),(173,'mail','Thank you for registration at {BranUrl}. Click here: {VerifyUrl}'),(176,'mail','To reset password open following link: {RecoveryLink} and enter new password.'),(175,'mail','We\'ve received a request from you to reset a password. If you didn\'t do it, then just ignore this message.'),(177,'mail','You got new message!'),(178,'mail','You\'ve received new message from {Name}. Open following link to see a message: {messageLink}'),(169,'mail','{Brandname} administration'),(194,'misc','Added at'),(424,'misc','All'),(208,'misc','Are you sure you want to delete this item?'),(484,'misc','Atom'),(33,'misc','Buy Websites'),(73,'misc','Click here'),(345,'misc','Create'),(320,'misc','Delete'),(308,'misc','Developed by {Developer}'),(192,'misc','Displaying {start}-{end} of {count} results.'),(206,'misc','Edit'),(305,'misc','Hello, {Username}'),(296,'misc','Home'),(202,'misc','ID'),(301,'misc','Inbox'),(11,'misc','Language'),(307,'misc','Logout'),(423,'misc','Not on sale'),(191,'misc','Nothing found'),(304,'misc','On sale'),(481,'misc','Optional'),(346,'misc','OR'),(216,'misc','Please choose category'),(360,'misc','Remove all records'),(483,'misc','RSS'),(303,'misc','See all messages'),(299,'misc','Sell Websites'),(306,'misc','Settings'),(300,'misc','Sign in'),(17,'misc','Submit'),(441,'misc','Toggle navigation'),(297,'misc','Top'),(298,'misc','Upcoming'),(217,'misc','Update'),(16,'misc','Verification code'),(205,'misc','View'),(485,'misc','{BrandName} | Atom feed'),(486,'misc','{BrandName} | RSS feed'),(302,'misc','{NumberOfMsg} Messages'),(465,'notification','Action \"{action}\" does not exist in \"{controller}\".'),(361,'notification','All records have been removed'),(185,'notification','An error has occurred while trying to confirm your email. Please try again later.'),(143,'notification','An error occurred while sending email'),(247,'notification','An internal error occurred. Please try again later'),(186,'notification','Attention!'),(215,'notification','Email has been sent'),(141,'notification','Please, use mail client which support HTML markup'),(332,'notification','Record has been deleted'),(330,'notification','Record has been successfully modified'),(182,'notification','Server temporarily unavailable. Please, try to register a little bit later'),(139,'notification','Server temporarily unavailable. Try again later.'),(54,'notification','The page you are looking for doesn\'t exists'),(181,'notification','The token is expired or does not valid. Please, try to register new account'),(294,'notification','Unable to find user in database'),(142,'notification','We\'ve sent you email with instructions. Please check your inbox'),(233,'post','Action'),(258,'post','Back to Inbox'),(260,'post','Block Sender'),(276,'post','Blocked at'),(232,'post','Blocked users'),(262,'post','Chat'),(257,'post','Chat with {Username}'),(242,'post','Completely remove'),(230,'post','folder_important'),(227,'post','folder_inbox'),(228,'post','folder_sent'),(229,'post','folder_starred'),(231,'post','folder_trash'),(256,'post','I\'m'),(265,'post','Just now'),(259,'post','Loading...'),(240,'post','Mark as Important'),(236,'post','Mark as Read'),(237,'post','Mark as Starred'),(248,'post','Message'),(245,'post','Message has been sent'),(282,'post','Messages have been completely removed'),(281,'post','Messages have been restored'),(239,'post','Move to Trash'),(244,'post','My messages'),(235,'post','New'),(246,'post','New message'),(234,'post','Refresh'),(238,'post','Remove from Important folder'),(241,'post','Remove from Starred folder'),(254,'post','Removed from sale'),(243,'post','Restore from Trash folder'),(261,'post','Scammer!'),(288,'post','Selected messages has been marked as Important'),(284,'post','Selected messages have been marked as read'),(285,'post','Selected messages have been moved to Starred folder'),(283,'post','Selected messages have been moved to trash folder'),(287,'post','Selected messages have been removed from Important folder'),(286,'post','Selected messages have been removed from Starred folder'),(253,'post','Send message'),(255,'post','The website has been sold or removed from sales.'),(252,'post','This user has blocked you'),(264,'post','Type your message here...'),(249,'post','Unauthorized users can not send messages'),(277,'post','Unblock'),(278,'post','User has been blocked'),(280,'post','User has been blocked and marked as a spammer'),(279,'post','User has been unblocked'),(251,'post','You are forbidden to send messages'),(250,'post','You can\'t send message to yourself'),(263,'post','You have blocked this user. {Click here} to unblock'),(271,'post','{Many} days ago'),(273,'post','{Many} hours ago'),(275,'post','{Many} minutes ago'),(269,'post','{Many} months ago'),(267,'post','{Many} years ago'),(270,'post','{One} day ago'),(272,'post','{One} hour ago'),(274,'post','{One} minute ago'),(268,'post','{One} month ago'),(266,'post','{One} year ago'),(204,'sale','Category'),(226,'sale','Congratulations. Your website/domain is on sale'),(203,'sale','Domain/Website'),(211,'sale','Field'),(224,'sale','Fill out the form'),(213,'sale','Information has been updated'),(222,'sale','Insert widget on your main page and click \"verify\" button. Feel free to modify HTML/CSS, but leave do follow link.'),(210,'sale','My Websites/Domains on Sale'),(201,'sale','Nothing is sold'),(218,'sale','Put up for sale'),(207,'sale','Remove from sale'),(225,'sale','Sell Website/Domain'),(214,'sale','The website is already on sale'),(219,'sale','The widget has been found'),(220,'sale','The widget has not been found'),(212,'sale','Value'),(223,'sale','Verify'),(209,'sale','Visit website'),(221,'sale','Website {Domain} has been removed from sale'),(30,'sale_instruction','After analysis and calculation of your website price done, click \"Sell my website/domain\"'),(34,'sale_instruction','And wait for people contact with you.'),(35,'sale_instruction','Check websites and domains on sale'),(32,'sale_instruction','Check your domain/website listed on {Buy Websites} page'),(29,'sale_instruction','Check your website/domain on {Portal}'),(25,'sale_instruction','Do you want to sell your website, sell your domain name?'),(28,'sale_instruction','Register/Login to {Portal}'),(37,'sale_instruction','Sale instruction description'),(36,'sale_instruction','Sale instruction page keywords'),(24,'sale_instruction','Sell Websites - Sell Domains'),(31,'sale_instruction','Site Verification page opens'),(27,'sale_instruction','To tell people that you may sell your website or sell domain'),(26,'sale_instruction','{Portal} allows people you to sell your website.'),(413,'scam','Complain date'),(418,'scam','Legend'),(417,'scam','Prevent {Scammer} from sending letters'),(410,'scam','Scam reports'),(412,'scam','Scammer'),(411,'scam','Sender'),(419,'scam','Sent at'),(416,'scam','The dialog between {Sender} and {Scammer}'),(415,'scam','The dialog has been completely removed'),(420,'scam','User has been restricted'),(414,'scam','View dialog'),(464,'site','Access denied'),(21,'site','Index page description'),(20,'site','Index page keywords'),(19,'site','Index page title'),(463,'site','Internal server error'),(462,'site','Page not found'),(457,'tools','All websites that are on sale have been checked'),(456,'tools','Cache has been cleared'),(458,'tools','Expired data has been cleared'),(454,'tools','Sitemap has been generated'),(180,'user','A confirmation email has been sent on {ConfirmationEmail}'),(158,'user','Active'),(162,'user','Administrator'),(386,'user','Allow send messages'),(159,'user','Blocked'),(150,'user','Can send message'),(291,'user','Change Password Form'),(385,'user','Confirmed Email'),(183,'user','Congratulations!'),(133,'user','Create an account'),(384,'user','Create User'),(134,'user','Creating a {InstalledUrl} account is fast, easy, and free'),(383,'user','Delete user'),(160,'user','Deleted'),(130,'user','Don\'t have an account yet? {Sign up now}!'),(382,'user','Edit user data'),(123,'user','Email'),(148,'user','Email confirmation'),(391,'user','Field'),(136,'user','Forgot password instruction'),(129,'user','Forgot your password?'),(179,'user','Go check your email and confirm RIGHT NOW'),(293,'user','Incorrect old password'),(459,'user','Invalid email or password'),(155,'user','Last login at'),(156,'user','Last login IP Address'),(127,'user','Login Form'),(380,'user','Manage Users'),(154,'user','Modified at'),(132,'user','Need an account?'),(292,'user','Old password'),(138,'user','or wait, I remember!'),(124,'user','Password'),(289,'user','Password has been changed'),(140,'user','Password recovery'),(157,'user','Passwords do not match'),(396,'user','Please Sign In'),(290,'user','Profile Settings'),(152,'user','Re-Password'),(145,'user','Register Form'),(144,'user','Register new account'),(153,'user','Registered at'),(393,'user','Registered IP'),(166,'user','Registration at {InstalledUrl}'),(126,'user','Remember me'),(388,'user','Reset password'),(163,'user','Root'),(189,'user','Set new password'),(128,'user','Sign in'),(165,'user','Sign up'),(131,'user','Sign up now'),(190,'user','The password has been changed. Use new password to login'),(164,'user','Unknown'),(161,'user','User'),(461,'user','User has been blocked or deleted'),(387,'user','User has been created'),(394,'user','User has been deleted'),(390,'user','User has {CountOnSale} website(-s) on sale'),(389,'user','User Inbox'),(381,'user','User info'),(149,'user','User role'),(151,'user','User status'),(147,'user','Username'),(392,'user','Value'),(125,'user','Verification code'),(137,'user','We didn\'t find such email address'),(460,'user','We have sent you confirmation email. Please confirm it'),(395,'user','You have been disconnected from the session'),(184,'user','You have successfully confirmed your email address.'),(146,'user','{Click here} if you already have an account and just need to login.'),(60,'website','Add to catalog. Free'),(6,'website','Alexa Rank'),(85,'website','Alexa Stats'),(105,'website','Antivirus Stats'),(482,'website','AVG'),(75,'website','Basic information'),(83,'website','Bing Index'),(3,'website','Calculate'),(108,'website','Catalog Stats'),(114,'website','City'),(100,'website','Click count'),(120,'website','Coins'),(99,'website','Comment count'),(74,'website','Contact seller'),(449,'website','Could not reach host: {Host}'),(42,'website','Country'),(466,'website','Daily Ads Revenue'),(95,'website','Daily Global Rank Trend'),(467,'website','Daily Pageviews'),(480,'website','Daily Reach (Percent)'),(468,'website','Daily Unique Visitors'),(79,'website','Description'),(110,'website','Dmoz'),(427,'website','Domain'),(76,'website','Domain name'),(62,'website','Estimate other website'),(9,'website','Estimate Price'),(469,'website','Estimated Daily Stats'),(470,'website','Estimated Data Analytics'),(471,'website','Estimated Monthly Stats'),(1,'website','Estimated website cost of any domain'),(44,'website','Estimated worth'),(472,'website','Estimated Yearly Stats'),(10,'website','Explore more'),(96,'website','Facebook Stats'),(122,'website','Failed'),(117,'website','Get code'),(58,'website','Get website review'),(61,'website','Get widget / Sale website'),(86,'website','Global Rank'),(106,'website','Google'),(84,'website','Google Backlinks'),(81,'website','Google Index'),(104,'website','Gplus+'),(4,'website','Grid'),(55,'website','Has Estimated Worth of'),(50,'website','How much {Website} is worth?'),(112,'website','IP Address'),(422,'website','Is On Sale'),(78,'website','Keywords'),(429,'website','Latitude'),(425,'website','Leave empty input'),(97,'website','Like count'),(8,'website','Likes'),(87,'website','Links in'),(5,'website','List'),(38,'website','List of upcoming websites. Page {PageNr}'),(91,'website','Load speed'),(90,'website','Local Rank'),(111,'website','Location Stats'),(428,'website','Longitude'),(421,'website','Manage Websites'),(107,'website','McAfee'),(473,'website','Monthly Ads Revenue'),(67,'website','Monthly page view'),(474,'website','Monthly Pageviews'),(68,'website','Monthly revenue'),(475,'website','Monthly Unique Visitors'),(57,'website','More actions'),(476,'website','Norton'),(69,'website','Notes'),(41,'website','Number of websites'),(7,'website','Page Rank'),(430,'website','Re-Calculate estimate price'),(113,'website','Region'),(433,'website','Remove website'),(431,'website','Remove website from sale'),(89,'website','Review average'),(88,'website','Review count'),(119,'website','Sale Website'),(64,'website','Sales Information'),(80,'website','Search Engine Stats'),(71,'website','Seller has blocked you'),(65,'website','Selling price'),(98,'website','Share count'),(116,'website','Show Your Visitors Your Website Value'),(56,'website','Site Price calculated at: {Time}'),(102,'website','Social Stats'),(121,'website','Success'),(434,'website','The website\'s estimated price has been recalculated. {Click here} to see a result'),(63,'website','This website is on sale'),(77,'website','Title'),(45,'website','TOP by Cost'),(47,'website','TOP by Countries'),(46,'website','TOP by PageRank'),(43,'website','Top websites by countries. Diagram. Page {PageNr}'),(40,'website','Top websites by PageRank. Diagram'),(39,'website','Top websites by price. Page {PageNr}'),(49,'website','Top websites having {PageRank} PageRank. Page {PageNr}'),(48,'website','Top websites in {Country}. Page {PageNr}'),(101,'website','Total count'),(103,'website','Twitter'),(66,'website','Unique monthly visitors'),(432,'website','View website info'),(59,'website','Webmaster info'),(426,'website','Website'),(53,'website','Website cost'),(436,'website','Website has been deleted'),(52,'website','website keywords'),(118,'website','Website owner? {Sale Website}!'),(435,'website','Website {Website} has been removed from sale'),(115,'website','WHOIS'),(109,'website','Yahoo'),(82,'website','Yahoo Index'),(477,'website','Yearly Ads Revenue'),(478,'website','Yearly Pageviews'),(479,'website','Yearly Unique Visitors'),(72,'website','You have blocked this user. {Click here} to unblock'),(70,'website','You should login to contact seller'),(2,'website','{NumOfWebsites} total website price calculated'),(93,'website','{Percent} of sites are faster'),(94,'website','{Percent} of sites are slower'),(92,'website','{Seconds} Seconds'),(51,'website','{Website} worth is {Cost}');
/*!40000 ALTER TABLE `sc_trans_source_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_user`
--

DROP TABLE IF EXISTS `sc_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `password` varchar(32) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `email` varchar(80) NOT NULL,
  `lang_id` varchar(5) NOT NULL,
  `role` enum('user','administrator','root') NOT NULL DEFAULT 'user',
  `can_send_message` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `username` varchar(30) NOT NULL,
  `email_confirmed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `post_server_id` smallint(5) unsigned NOT NULL,
  `ip` varchar(32) NOT NULL DEFAULT '127.0.0.1',
  `last_ip_login` varchar(32) NOT NULL DEFAULT '127.0.0.1',
  `registered_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_username` (`username`),
  KEY `ix_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_user`
--

LOCK TABLES `sc_user` WRITE;
/*!40000 ALTER TABLE `sc_user` DISABLE KEYS */;
INSERT INTO `sc_user` VALUES (1,'4dec0c215ba6008a2da7072c1d5a34c0','a3f61195aac72cba69f20b471b8fa319','admin@example.com','ru','root',1,1,'Administrator',1,0,'127.0.0.1','127.0.0.1','2014-09-20 13:02:01','2014-09-20 13:02:01','2014-11-07 08:34:33');
/*!40000 ALTER TABLE `sc_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_user_token`
--

DROP TABLE IF EXISTS `sc_user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_user_token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `token` varchar(32) NOT NULL,
  `type` smallint(5) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_token_type_status` (`token`,`type`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_user_token`
--

LOCK TABLES `sc_user_token` WRITE;
/*!40000 ALTER TABLE `sc_user_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_user_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_webdata_alexa`
--

DROP TABLE IF EXISTS `sc_webdata_alexa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_webdata_alexa` (
  `wid` int(10) unsigned NOT NULL,
  `rank` int(10) unsigned NOT NULL,
  `linksin` int(10) unsigned NOT NULL,
  `review_count` int(10) unsigned NOT NULL,
  `review_avg` float unsigned NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `country_rank` int(10) unsigned NOT NULL,
  `speed_time` int(10) unsigned NOT NULL,
  `pct` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`wid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_webdata_alexa`
--

LOCK TABLES `sc_webdata_alexa` WRITE;
/*!40000 ALTER TABLE `sc_webdata_alexa` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_webdata_alexa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_webdata_antivirus`
--

DROP TABLE IF EXISTS `sc_webdata_antivirus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_webdata_antivirus` (
  `wid` int(10) unsigned NOT NULL,
  `google` enum('safe','warning','caution','untested') NOT NULL DEFAULT 'untested',
  `avg` enum('safe','warning','caution','untested') NOT NULL DEFAULT 'untested',
  PRIMARY KEY (`wid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_webdata_antivirus`
--

LOCK TABLES `sc_webdata_antivirus` WRITE;
/*!40000 ALTER TABLE `sc_webdata_antivirus` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_webdata_antivirus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_webdata_catalog`
--

DROP TABLE IF EXISTS `sc_webdata_catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_webdata_catalog` (
  `wid` int(10) unsigned NOT NULL,
  `dmoz` tinyint(3) unsigned NOT NULL,
  `yahoo` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`wid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_webdata_catalog`
--

LOCK TABLES `sc_webdata_catalog` WRITE;
/*!40000 ALTER TABLE `sc_webdata_catalog` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_webdata_catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_webdata_location`
--

DROP TABLE IF EXISTS `sc_webdata_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_webdata_location` (
  `wid` int(10) unsigned NOT NULL,
  `city` varchar(100) NOT NULL,
  `region_name` varchar(100) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `longitude` float NOT NULL,
  `latitude` float NOT NULL,
  `country_code` varchar(2) NOT NULL,
  PRIMARY KEY (`wid`),
  KEY `ix_country_code` (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_webdata_location`
--

LOCK TABLES `sc_webdata_location` WRITE;
/*!40000 ALTER TABLE `sc_webdata_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_webdata_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_webdata_main`
--

DROP TABLE IF EXISTS `sc_webdata_main`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_webdata_main` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(150) NOT NULL,
  `idn` varchar(150) NOT NULL,
  `md5domain` varchar(32) NOT NULL,
  `added_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `price` decimal(30,2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_md5domain` (`md5domain`),
  KEY `ix_added` (`added_at`),
  KEY `ix_price` (`price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_webdata_main`
--

LOCK TABLES `sc_webdata_main` WRITE;
/*!40000 ALTER TABLE `sc_webdata_main` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_webdata_main` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_webdata_metatags`
--

DROP TABLE IF EXISTS `sc_webdata_metatags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_webdata_metatags` (
  `wid` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`wid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_webdata_metatags`
--

LOCK TABLES `sc_webdata_metatags` WRITE;
/*!40000 ALTER TABLE `sc_webdata_metatags` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_webdata_metatags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_webdata_searchengine`
--

DROP TABLE IF EXISTS `sc_webdata_searchengine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_webdata_searchengine` (
  `wid` int(10) unsigned NOT NULL,
  `google_index` bigint(20) unsigned NOT NULL,
  `bing_index` bigint(20) unsigned NOT NULL,
  `google_backlinks` bigint(20) unsigned NOT NULL,
  `page_rank` enum('n-a','0','1','2','3','4','5','6','7','8','9','10') NOT NULL DEFAULT 'n-a',
  `yahoo_index` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`wid`),
  KEY `ix_page_rank` (`page_rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_webdata_searchengine`
--

LOCK TABLES `sc_webdata_searchengine` WRITE;
/*!40000 ALTER TABLE `sc_webdata_searchengine` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_webdata_searchengine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_webdata_social`
--

DROP TABLE IF EXISTS `sc_webdata_social`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_webdata_social` (
  `wid` int(10) unsigned NOT NULL,
  `gplus` int(10) unsigned NOT NULL,
  `facebook_share_count` int(10) unsigned NOT NULL,
  `facebook_like_count` int(10) unsigned NOT NULL,
  `facebook_comment_count` int(10) unsigned NOT NULL,
  `facebook_total_count` int(10) unsigned NOT NULL,
  `facebook_click_count` int(10) unsigned NOT NULL,
  `twitter` int(10) unsigned NOT NULL,
  PRIMARY KEY (`wid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_webdata_social`
--

LOCK TABLES `sc_webdata_social` WRITE;
/*!40000 ALTER TABLE `sc_webdata_social` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_webdata_social` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_webdata_whois`
--

DROP TABLE IF EXISTS `sc_webdata_whois`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_webdata_whois` (
  `wid` int(10) unsigned NOT NULL,
  `text` text,
  PRIMARY KEY (`wid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_webdata_whois`
--

LOCK TABLES `sc_webdata_whois` WRITE;
/*!40000 ALTER TABLE `sc_webdata_whois` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_webdata_whois` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_website_sale`
--

DROP TABLE IF EXISTS `sc_website_sale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_website_sale` (
  `website_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `price` decimal(30,2) NOT NULL,
  `monthly_visitors` int(10) unsigned NOT NULL,
  `monthly_revenue` decimal(20,2) NOT NULL,
  `monthly_views` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `added_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`website_id`) USING BTREE,
  KEY `ix_cat_price` (`category_id`,`price`),
  KEY `ix_cat_added` (`category_id`,`added_at`),
  KEY `ix_user_added` (`user_id`,`added_at`) USING BTREE,
  KEY `ix_added` (`added_at`),
  KEY `ix_price` (`price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_website_sale`
--

LOCK TABLES `sc_website_sale` WRITE;
/*!40000 ALTER TABLE `sc_website_sale` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_website_sale` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-13 16:29:51
